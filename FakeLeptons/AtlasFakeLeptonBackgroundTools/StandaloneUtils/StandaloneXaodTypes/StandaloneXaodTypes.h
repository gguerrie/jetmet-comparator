/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FAKEBKGTOOLS_STANDALONEXAODTYPES_H
#define FAKEBKGTOOLS_STANDALONEXAODTYPES_H

#include <string>
#include <vector>
#include <set>
#include <map>
#include <type_traits>
#include <stdexcept>
#include <memory>
#include <iostream>

/// Emulations of xAOD types and enums for standalone usage
/// All declarations are inside a global FakeBkgTools{} namespace to avoid potential conflicts with other standalone packages

enum StatusCode
{
    SUCCESS,
    FAILURE
};
    
namespace xAOD
{
    namespace Type
    {
      enum ObjectType
      {
         Other = 0, ///< An object not falling into any of the other categories
         Jet          = 2, ///< The object is a jet
         Electron = 6, ///< The object is an electron
         Photon   = 7, ///< The object is a photon
         Muon     = 8, ///< The object is a muon
         Tau      = 9, ///< The object is a tau (jet)
         BTag   = 102, ///< The object is a b-tagging object
         Particle          = 1101, ///< Generic particle object, for analysis
         CompositeParticle = 1102  ///< Particle composed of other particles
      };
    }

    struct EventInfo
    {
        std::map<std::string, float> custom;
        template<class ReturnType> float auxdataConst(const std::string& data) const
        {
           return static_cast<ReturnType>(custom.at(data));
        }
    };
    
    template<typename Type> struct Scalar
    {
        Scalar() : m_value() {}
        Scalar(Type value) : m_value(value) {}
        Scalar& operator=(Type value) { m_value = value; return *this; }
        Type& operator()() { return m_value; }
        const Type& operator()() const { return m_value; }
        //Type& operator Type() { return m_value; }
        //const Type& operator Type() const { return m_value; }
    protected:
        Type m_value;
    };

    struct IParticle
    {
        Scalar<xAOD::Type::ObjectType> type;
        Scalar<float> eta, pt, phi;
        Scalar<int8_t> charge;
        bool tight;
        float real_eff, fake_eff;
        std::map<std::string, float> custom;
        IParticle& operator*() { return *this; }
        const IParticle& operator*() const { return *this; }
        template<class ReturnType> float auxdataConst(const std::string& data) const
        {
            if(data=="real_eff") return static_cast<ReturnType>(real_eff);
            else if(data=="fake_eff") return static_cast<ReturnType>(fake_eff);
            else return static_cast<ReturnType>(custom.at(data));
        }
    };
    using Electron = IParticle;
    using Muon = IParticle;
    
    struct IParticleContainer : public std::vector<IParticle>
    {
        using std::vector<IParticle>::vector;
        EventInfo eventInfo;
    };
}

namespace CP
{   
    enum SystematicCode
    {
        Unsupported = 0,
        Ok = 1
    };
    struct SystematicVariation : public std::string
    {
        SystematicVariation(const std::string& s, int param=0) : std::string(s), m_param(param) { if(param) append("__"+std::to_string(abs(param))+(param>0?"up":"down")); }
        const std::string& name() const { return *this; }
        std::string basename() const { return m_param? this->substr(0, this->rfind("__")) : name(); }
        int parameter() const { return m_param; }
    protected:
        short int m_param;
    };
    
    class SystematicSet : public std::set<SystematicVariation>
    {
    public:
        mutable std::size_t m_hash = 0;
        static CP::SystematicCode filterForAffectingSystematics(const SystematicSet& in, const SystematicSet&, SystematicSet& out)
        {
            out = in;
            return CP::SystematicCode::Ok;
        }
    };
    
    class ISystematicsTool
    {
    public:
        
        virtual StatusCode initialize() = 0;
        
        virtual bool isAffectedBySystematic(const CP::SystematicVariation& systematic) const = 0 ;
        virtual CP::SystematicSet affectingSystematics() const = 0;
        virtual CP::SystematicSet recommendedSystematics() const = 0;
        virtual CP::SystematicCode applySystematicVariation(const CP::SystematicSet& systConfig) = 0;
    };
    
    struct ISelectionAccessor
    {
        bool getBool(const xAOD::IParticle& p) const { return p.tight; }
    };
    inline StatusCode makeSelectionAccessor(const std::string&, std::unique_ptr<ISelectionAccessor>& uptr, bool = false)
    {
        uptr.reset(new CP::ISelectionAccessor);
        return StatusCode::SUCCESS;
    }
}

namespace MSG
{
    enum Level : int { NIL = 0, VERBOSE, DEBUG, INFO, WARNING, ERROR, FATAL, ALWAYS, NUM_LEVELS };
}
    
    /// PropertyManager: this set of classes allows to emulate the property management of the ATLAS EDM, by providing declareProperty / setProperty methods
    /// Usage: the base class 'BaseTool' must inherit from PropertyManager<BaseTool>;
    /// subsequent derived classes 'DerivedTool' must inherit from ExtraPropertyManager<DerivedTool, DirectBaseTool>, where DirectBaseTool is their direct base class.
    

namespace FakeBkgTools
{
    class IPropertyManager
    {
    protected:
        virtual void setPropertyPrivate(const std::string& name, const std::vector<std::string>& value) = 0;
        virtual void setPropertyPrivate(const std::string& name, const std::string& value) = 0;
        virtual void setPropertyPrivate(const std::string& name, int value) = 0;
        virtual void setPropertyPrivate(const std::string& name, bool value) = 0;
        template<class T, class D> friend class ExtraPropertyManager;
    };

    template<class Tool, class DirectBaseTool = void> class ExtraPropertyManager;

    template<class Tool>
    class ExtraPropertyManager<Tool, void> : public IPropertyManager
    {
    protected:
        std::map<std::string, int Tool::*> m_properties;
        IPropertyManager* m_extra = nullptr;
        void setExtraManager(IPropertyManager* extra) { m_extra = extra; };
        ExtraPropertyManager() {}
    public:
        ExtraPropertyManager(const ExtraPropertyManager& rhs) = default;
        ExtraPropertyManager(ExtraPropertyManager&& rhs) = default;
    protected:
        template<typename T> void declareProperty(const std::string& name, T Tool::* property, const std::string&)
        {
            m_properties[name] = reinterpret_cast<int Tool::*>(property); 
        }
        virtual void setPropertyPrivate(const std::string& name, const std::vector<std::string>& value) override
        {
            auto itr = m_properties.find(name);
            if(itr != m_properties.end()) static_cast<Tool*>(this)->*reinterpret_cast<std::vector<std::string> Tool::*>(itr->second) = value;
            else if(m_extra) m_extra->setPropertyPrivate(name, value);
            else throw std::out_of_range("the tool has no property named '" + name + "'");
        }
        virtual void setPropertyPrivate(const std::string& name, const std::string& value) override
        {
            auto itr = m_properties.find(name);
            if(itr != m_properties.end()) static_cast<Tool*>(this)->*reinterpret_cast<std::string Tool::*>(itr->second) = value;
            else if(m_extra) m_extra->setPropertyPrivate(name, value);
            else throw std::out_of_range("the tool has no property named '" + name + "'");
        }
        virtual void setPropertyPrivate(const std::string& name, int value) override
        {
            auto itr = m_properties.find(name);
            if(itr != m_properties.end()) static_cast<Tool*>(this)->*reinterpret_cast<int Tool::*>(itr->second) = value;
            else if(m_extra) m_extra->setPropertyPrivate(name, value);
            else throw std::out_of_range("the tool has no property named '" + name + "'");
        }
        virtual void setPropertyPrivate(const std::string& name, bool value) override
        {
            auto itr = m_properties.find(name);
            if(itr != m_properties.end()) static_cast<Tool*>(this)->*reinterpret_cast<bool Tool::*>(itr->second) = value;
            else if(m_extra) m_extra->setPropertyPrivate(name, value);
            else throw std::out_of_range("the tool has no property named '" + name + "'");
        }
        template<class T, class D> friend class ExtraPropertyManager;
    };

    template<class Tool, class DirectBaseTool> class ExtraPropertyManager : public ExtraPropertyManager<Tool, void>
    {
    public:
        ExtraPropertyManager() : ExtraPropertyManager<Tool, void>()
        {
            static_cast<ExtraPropertyManager<DirectBaseTool, void>*>(static_cast<Tool*>(this))->setExtraManager(this);
        }
        ExtraPropertyManager(const ExtraPropertyManager& rhs) : ExtraPropertyManager<Tool, void>(rhs)
        {
            static_cast<ExtraPropertyManager<DirectBaseTool, void>*>(static_cast<Tool*>(this))->setExtraManager(this);
        }
        ExtraPropertyManager(ExtraPropertyManager&& rhs) : ExtraPropertyManager<Tool, void>(rhs)
        {
            static_cast<ExtraPropertyManager<DirectBaseTool, void>*>(static_cast<Tool*>(this))->setExtraManager(this);
        }
    };

    template<class Tool> class PropertyManager : public ExtraPropertyManager<Tool, void>
    {
    public:
        PropertyManager() {}
        template<typename T> void setProperty(const std::string& name, const T& value) { this->setPropertyPrivate(name, value); }
        void setProperty(const std::string& name, const char* value) { setProperty(name, std::string(value)); }
    };
}

namespace std
{
    template<> struct hash<CP::SystematicVariation>
    {
        std::size_t operator()(const CP::SystematicVariation& sysvar) const { return std::hash<std::string>()(sysvar.name()); }
    };
    
    template<> struct hash<CP::SystematicSet>
    {
        std::size_t operator()(const CP::SystematicSet& systSet) const
        {
            if(systSet.m_hash) return systSet.m_hash;
            for(auto& sysvar : systSet) systSet.m_hash = systSet.m_hash ^ std::hash<std::string>()(sysvar.name()); 
            return systSet.m_hash;
        }
    };
}

namespace asg
{
    class AsgTool
    {
    public:
        AsgTool(const std::string&) {}
        bool msgLvl(MSG::Level lvl) const { return m_outputLevel <= lvl; }
        void setOutputLevel(MSG::Level outputLevel) { m_outputLevel = outputLevel; } 
    protected:
        MSG::Level m_outputLevel = MSG::INFO; //!
    };
}

inline std::string PathResolverFindDataFile(const std::string& path)
{
    return path;
}

inline std::string PathResolverFindCalibFile(const std::string& path)
{
    return path;
}

#define ASG_TOOL_CLASS(x, y)
#define ASG_TOOL_CLASS2(x, y, z)
#define ATH_CHECK(cmd) if(cmd != StatusCode::SUCCESS) return StatusCode::FAILURE


#define ATH_MSG_LVL(lvl, lvlTxt, x)                                        \
  do {                                                                     \
    if (this->msgLvl (lvl)) {                                              \
      std::cout << "[FakeBkgTools] " << lvlTxt << ": " << x << std::endl;  \
    }                                                                      \
  } while (0)

#define ATH_MSG_VERBOSE(x) ATH_MSG_LVL(MSG::VERBOSE, "VERBBOSE", x)
#define ATH_MSG_DEBUG(x) ATH_MSG_LVL(MSG::DEBUG, "DEBUG", x)
#define ATH_MSG_INFO(x) std::cout << "[FakeBkgTools] INFO: " << x << std::endl
#define ATH_MSG_WARNING(x) std::cout << "[FakeBkgTools] WARNING: " << x << std::endl << "\tin function " << __PRETTY_FUNCTION__ \
    << std::endl << "\tin file " << __FILE__ << " line " << __LINE__ << std::endl
#define ATH_MSG_ERROR(x) std::cerr << "[FakeBkgTools] ERROR: " << x << std::endl << "\tin function " << __PRETTY_FUNCTION__ \
    << std::endl << "\tin file " << __FILE__ << " line " << __LINE__ << std::endl
#define ATH_MSG_FATAL(x) std::cerr << "[FakeBkgTools] FATAL: " << x << std::endl << "\tin function " << __PRETTY_FUNCTION__ \
    << std::endl << "\tin file " << __FILE__ << " line " << __LINE__ << std::endl


#endif
