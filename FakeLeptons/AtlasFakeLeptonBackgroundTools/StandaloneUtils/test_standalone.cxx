/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

//----------------------------------------------------------------------
//
//  This program provides a toy MC model with a single pseudoexperiment.  It
//  includes the main features of the FakeBkgTools interface; a real analysis
//  will need to input the necessary lepton and event information from an
//  xAOD file or custom ntuple.
//
//----------------------------------------------------------------------
#include "FakeBkgTools/ApplyFakeFactor.h"
#include "FakeBkgTools/AsymptMatrixTool.h"
#include "FakeBkgTools/LhoodMM_tools.h"

#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

#include <string>
#include <fstream>
#include <iostream>
#include <random>
#include <vector>

using namespace FakeBkgTools;
using namespace std;

void initialize(CP::BaseFakeBkgTool& tool, const string& input);
void writeXML(const string& name, int type);
void writeROOT(const string& name, int type);

std::string selection = "2T";
std::string process = ">=1F[T]";



int main(int, char*[])
{
    CP::ApplyFakeFactor fkfTool("ApplyFakeFactor");
    CP::AsymptMatrixTool asmTool("AsymptMatrixTool");
    CP::LhoodMM_tools lhmTool("LhoodMM_tools");
    
    string input = "/tmp/efficiencies";
    if(true) // switch to test either XML or ROOT input
    {
        input += ".xml";
        writeXML(input, 0);
    }
    else
    {
        input += ".root";
        writeROOT(input, 0);
    }

    initialize(fkfTool, input);
    initialize(asmTool, input);
    initialize(lhmTool, input);
	
    std::default_random_engine rdm;
    std::bernoulli_distribution tightFakeElectronPdf(0.1), tightFakeMuonPdf(0.15), flavourPdf(0.5);
    float asmYield, fkfYield, lhmYield;
    float asmYieldSumw2=0, fkfYieldSumw2=0, lhmYieldStatUp, lhmYieldStatDown;
    float nTrueFakes = 0;
    for(int i=0;i<5000;++i)
    {
        xAOD::IParticleContainer leptons;
        unsigned ntight = 0;
        for(int l=0;l<2;++l)
        {
            leptons.emplace_back();
            leptons.back().charge = 1;
            leptons.back().pt = 70;
            if(flavourPdf(rdm))
            {
                leptons.back().type = xAOD::Type::Electron;
                leptons.back().tight = tightFakeElectronPdf(rdm);
            }
            else
            {
                leptons.back().type = xAOD::Type::Muon;
                leptons.back().tight = tightFakeMuonPdf(rdm);
            }
            if(leptons.back().tight) ntight++;
        }
        if(ntight==2) ++nTrueFakes;
        fkfTool.addEvent(leptons);
        asmTool.addEvent(leptons);
        lhmTool.addEvent(leptons);
        float wgt;
        if(asmTool.getEventWeight(wgt, selection, process) != StatusCode::SUCCESS) { cout << "ERROR: AsymptMatrixTool::getEventWeightAndUncertainties() failed\n"; exit(2); }
        asmYield += wgt;
        asmYieldSumw2 += wgt * wgt;
        if(fkfTool.getEventWeight(wgt, selection, process) != StatusCode::SUCCESS) { cout << "ERROR: ApplyFakeFactor::getEventWeightAndUncertainties() failed\n"; exit(2); }
        fkfYield += wgt;
        fkfYieldSumw2 += wgt * wgt;
    }
    if(lhmTool.getTotalYield(lhmYield, lhmYieldStatUp, lhmYieldStatDown) != StatusCode::SUCCESS) { cout << "ERROR: LhoodMM_tools::getTotalYieldAndUncertainties() failed\n"; exit(2); }
    
    cout <<"actual = " << nTrueFakes << ", asympt. matrix = " << asmYield << " +- " << sqrt(asmYieldSumw2)
        << ", fake factor = " << fkfYield << " +- " << sqrt(fkfYieldSumw2)
        << ", LH matrix = " << lhmYield << " +- " << lhmYieldStatUp << endl;
    
    asmTool.getTotalYield(lhmYield, lhmYieldStatUp, lhmYieldStatDown);
    cout << " accumulated yield for asympt. matrix = " << lhmYield << " +- " << lhmYieldStatUp << endl;
    
    return 0x0;
}

void initialize(CP::BaseFakeBkgTool& tool, const string& input)
{
    tool.setProperty("InputFiles", std::vector<std::string>{input});
    tool.setProperty("Selection", selection);
    tool.setProperty("Process", process);
    tool.setProperty("EnergyUnit", "GeV");
    tool.setOutputLevel(MSG::INFO);
    tool.setProperty("ConvertWhenMissing", true);
    if(tool.initialize() != StatusCode::SUCCESS)
    {
        cout << "ERROR: unable to initialize tool " << endl;;
        exit(1);
    }
}

void writeXML(const string& name, int type)
{
    ofstream f(name.c_str(), ios_base::out);
    if(type == 0)
    {
        f << "<syst affects=\"fake-efficiency\">syst</syst>"
            << "<electron type=\"fake-efficiency\" input=\"central-value\" stat=\"per-bin\"><bin> 0.10 +-0.02(stat) +-0.03(syst) </bin></electron>"
            << "<electron type=\"real-efficiency\" input=\"central-value\" stat=\"global\"><bin> 0.90+-0.01(stat) </bin></electron>"
            << "<muon type=\"fake-efficiency\" input=\"central-value\" stat=\"per-bin\"><bin> 0.15 +-0.03(stat) +-0.01(syst) </bin></muon>"
            << "<muon type=\"real-efficiency\" input=\"central-value\" stat=\"global\"><bin> 0.95+-0.01(stat) </bin></muon>";
    }
    f.close();
}

void writeROOT(const string& name, int type)
{
    TFile* file = TFile::Open(name.c_str(), "RECREATE");
    if(type == 0)
    {
        TH1D hElFake("FakeEfficiency_el_pt","FakeEfficiency", 2, 10., 100.);
        hElFake.SetBinContent(1, 0.01);
        hElFake.SetBinError(1, 0.001);
        hElFake.SetBinContent(2, 0.10);
        hElFake.SetBinError(2, 0.036);
        TH1D hMuFake("FakeEfficiency_mu_pt","FakeEfficiency", 1, 10., 100.);
        hMuFake.SetBinContent(1, 0.15);
        hMuFake.SetBinError(1, 0.032);
        TH1D hElReal("RealEfficiency_el_pt","RealEfficiency", 1, 10., 100.);
        hElReal.SetBinContent(1, 0.90);
        hElReal.SetBinError(1, 0.01);
        TH1D hMuReal("RealEfficiency_mu_pt","RealEfficiency", 1, 10., 100.);
        hMuReal.SetBinContent(1, 0.95);
        hMuReal.SetBinError(1, 0.01);
        file->cd();
        hElFake.Write();
        hElReal.Write();
        hMuFake.Write();
        hMuReal.Write();
    }
    file->Close();
    delete file;
}
