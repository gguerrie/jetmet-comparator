#ifndef FFFITTER_H_
#define FFFITTER_H_

#include "ForwardFolding/ForwardFolding/FoldingParameter.h"

#include "TH1.h"
#include "TH2D.h"
#include "TRandom3.h"

#include <memory>
#include <vector>

class FFFitter {

public:
    FFFitter();

    ~FFFitter() = default;

    void SetNormalise(const bool& flag);

    void SetParameterR(const int& steps,
                       const double& min,
                       const double& max);
    
    void SetParameterS(const int& steps,
                       const double& min,
                       const double& max);

    void SetRandomSeed(const int& seed);
    
    void SetChi2Option(const std::string& opt);

    void SetSubtractMinChi2(const bool& flag);

    void SetNneighbourIndices(const int& n);

    void FixParamS(const int& index);
    void FixParamR(const int& index);

    void FindBestTemplate(const std::vector<std::vector<TH1D> >& histos,
                          const TH1D& data);

    void RunPseudoexperiments(const std::vector<std::vector<TH1D> >& histos,
                              const TH1D& data,
                              const int& n);

    std::size_t GetBestIndexS() const;
    std::size_t GetBestIndexR() const;

    double GetBestChi2() const;

    const std::vector<std::vector<double> >& GetFullChi2() const;

    double GetBestValueS(bool* is_ok) const;
    double GetBestValueR(bool* is_ok) const;

    std::vector<double> GetBestProfileS() const;
    std::vector<double> GetBestProfileR() const;

    double GetParamSuncertaintyChi2(bool* is_ok) const;
    double GetParamRuncertaintyChi2(bool* is_ok) const;

    const std::vector<double>& GetPEvaluesS() const;
    const std::vector<double>& GetPEvaluesR() const;

    int GetSsteps() const;
    int GetRsteps() const;

    double GetDeltaS() const;
    double GetDeltaR() const;
    
    double GetMinS() const;
    double GetMinR() const;
    
    double GetMaxS() const;
    double GetMaxR() const;

    std::unique_ptr<TH2D> Create2Dchi2Histo() const;
    
    double IndexToValue(const int& index, const FoldingParameter::PARAMETER& par) const;

private:
    
    double GetFittedParam(const FoldingParameter::PARAMETER& par,
                          const bool& error,
                          bool* is_ok) const;

    void Poissonise(TH1D* h, TRandom3* rand) const;

    std::unique_ptr<FoldingParameter> m_param_r;
    std::unique_ptr<FoldingParameter> m_param_s;
    double m_delta_r;
    double m_delta_s;

    bool m_normalise;
    bool m_subtract;
    int m_seed;
    std::string m_chi2_option;
    int m_n_neighbour_indices;
    int m_fix_index_s;
    int m_fix_index_r;
    int m_best_index_s;
    int m_best_index_r;
    double m_best_chi2;
    std::vector<std::vector<double> > m_chi2_vec;

    std::vector<double> m_PE_s_values;
    std::vector<double> m_PE_r_values;

};

#endif
