#ifndef FOLDER_H_
#define FOLDER_H_

#include "ForwardFolding/ForwardFolding/FoldingParameter.h"

#include <memory>
#include <vector>

class Folder{

public:
    Folder();

    ~Folder();

    void SetParameterR(const int& steps,
                       const float& min,
                       const float& max);
    
    void SetParameterS(const int& steps,
                       const float& min,
                       const float& max);
   
    std::vector<std::vector<float> > Smear(const float& reco,
                                           const float& truth) const;

    std::pair<int,int> GetFoldingSizes() const;

private:

    float FFformula(const float& reco,
                    const float& truth,
                    const float& s,
                    const float& r) const;

    std::unique_ptr<FoldingParameter> m_param_r;
    std::unique_ptr<FoldingParameter> m_param_s;
    
    float m_delta_r;
    float m_delta_s;

};

#endif
