#ifndef FOLDINGPARAMETER_H_
#define FOLDINGPARAMETER_H_

class FoldingParameter {

public:

    enum PARAMETER {
        S = 0,
        R = 1
    };

    explicit FoldingParameter(const int& steps,
                              const float& min,
                              const float& max);

    ~FoldingParameter() = default;

    int GetSteps() const;
    float GetMin() const;
    float GetMax() const;

private:
    const int m_float_steps;
    const float m_float_min;
    const float m_float_max;

};

#endif
