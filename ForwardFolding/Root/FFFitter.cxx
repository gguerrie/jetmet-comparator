#include "ForwardFolding/ForwardFolding/FFFitter.h"

#include "TF1.h"
#include "TGraph.h"
#include "TH1.h"
#include "TH2D.h"

#include <iostream>

FFFitter::FFFitter() :
    m_param_r(nullptr),
    m_param_s(nullptr),
    m_delta_r(0),
    m_delta_s(0),
    m_normalise(false),
    m_subtract(true),
    m_seed(0),
    m_chi2_option(""),
    m_n_neighbour_indices(1),
    m_fix_index_s(-1),
    m_fix_index_r(-1),
    m_best_index_s(999),
    m_best_index_r(999),
    m_best_chi2(99999) {
}

void FFFitter::SetParameterR(const int& steps,
                             const double& min,
                             const double& max) {

    if (steps <= 1) { 
        std::cerr << "FFFitter::SetParameterR: ERROR Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "FFFitter::SetParameterR: ERROR Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_param_r = std::unique_ptr<FoldingParameter>(new FoldingParameter(steps, min, max));

    m_delta_r = static_cast<double>((max - min) / (steps-1));
}

void FFFitter::SetParameterS(const int& steps,
                             const double& min,
                             const double& max) {

    if (steps <= 1) { 
        std::cerr << "FFFitter::SetParameterS: ERROR Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "FFFitter::SetParameterS: ERROR Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_param_s = std::unique_ptr<FoldingParameter>(new FoldingParameter(steps, min, max));

    m_delta_s = static_cast<double>((max - min) / (steps-1));
}

void FFFitter::SetNormalise(const bool& flag) {
    m_normalise = flag;
}

void FFFitter::SetRandomSeed(const int& seed) {
    m_seed = seed;
}
    
void FFFitter::SetSubtractMinChi2(const bool& flag) {
    m_subtract = flag;
}

void FFFitter::SetChi2Option(const std::string& opt) {
    m_chi2_option = opt;
}

void FFFitter::FixParamS(const int& index) {
    m_fix_index_s = index;
}

void FFFitter::FixParamR(const int& index) {
    m_fix_index_r = index;
}

void FFFitter::SetNneighbourIndices(const int& n) {
    if (n < 1) {
        std::cout << "FFFitter::SetNneighbourIndices: WARNING: Requested n < 1, setting to 1\n";
        m_n_neighbour_indices = 1;
        return;
    }
    if (n > 3) {
        std::cout << "FFFitter::SetNneighbourIndices: WARNING: Requested n > 3, this may not give the best results\n";
    }
    m_n_neighbour_indices = n;
}

void FFFitter::FindBestTemplate(const std::vector<std::vector<TH1D> >& histos,
                                const TH1D& data) {

    if (histos.size() == 0) {
        std::cerr << "FFFitter::FindBestTemplate: ERROR Empty 2D vector passed!" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (histos.at(0).size() == 0) {
        std::cerr << "FFFitter::FindBestTemplate: ERROR Empty 1D vector passed!" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    m_chi2_vec.clear();
    m_chi2_vec.resize(histos.size());
    for (auto& i : m_chi2_vec) {
        i.resize(histos.at(0).size());
    }
    
    m_best_index_s = 999;
    m_best_index_r = 999;
    m_best_chi2 = 99999999;

    std::size_t start_index_s(0);
    std::size_t end_index_s(histos.size());
    if (m_fix_index_s >= 0) {
        start_index_s = m_fix_index_s;
        end_index_s = m_fix_index_s+1;
    }
    for (std::size_t is = start_index_s; is < end_index_s; ++is) {
        std::size_t start_index_r(0);
        std::size_t end_index_r(histos.at(is).size());
        if (m_fix_index_r >= 0) {
            start_index_r = m_fix_index_r;
            end_index_r = m_fix_index_r+1;
        }
        for (std::size_t ir = start_index_r; ir < end_index_r; ++ir) {

            std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(histos.at(is).at(ir).Clone()));

            if (m_normalise && (std::fabs(tmp->Integral()) > 1e-6)) {
                tmp->Scale(data.Integral()/tmp->Integral());
            }

            const double chi2 = data.Chi2Test(tmp.get(), m_chi2_option.c_str());

            m_chi2_vec[is][ir] = chi2;

            if (chi2 < m_best_chi2) {
                m_best_chi2 = chi2;
                m_best_index_s = is;
                m_best_index_r = ir;
            }
        }
    }

    if (m_subtract) {
        for (auto& i : m_chi2_vec) {
            for (auto& j : i) {
                j-= m_best_chi2;
            }
        }
    }
}

void FFFitter::RunPseudoexperiments(const std::vector<std::vector<TH1D> >& histos,
                                    const TH1D& data,
                                    const int& n) {

    if (histos.size() == 0) {
        std::cerr << "FFFitter::RunPseudoexperiments: ERROR Empty 2D vector passed!" << std::endl;
        exit(EXIT_FAILURE);
    }
    if (histos.at(0).size() == 0) {
        std::cerr << "FFFitter::RunPseudoexperiments: ERROR Empty 1D vector passed!" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_PE_s_values.clear();
    m_PE_r_values.clear();

    TRandom3 rand(m_seed);

    std::cout << "FFFitter::RunPseudoexperiments: Started running PE \n";
    /// loop on PE
    for (int ipe = 0; ipe < n; ++ipe) {
        m_chi2_vec.clear();
        m_chi2_vec.resize(histos.size());
        for (auto& i : m_chi2_vec) {
            i.resize(histos.at(0).size());
        }
        if (ipe % 100 == 0) {
            std::cout << "FFFitter::RunPseudoexperiments: Running PE: " << ipe << " out of " << n << " PEs \n";
        }
        std::unique_ptr<TH1D> data_smeared(static_cast<TH1D*>(data.Clone()));

        /// Smear data
        Poissonise(data_smeared.get(), &rand);

        m_best_chi2 = 99999;
        m_best_index_s = 999;
        m_best_index_r = 999;

        std::size_t start_index_s(0);
        std::size_t end_index_s(histos.size());
        if (m_fix_index_s >= 0) {
            start_index_s = m_fix_index_s;
            end_index_s = m_fix_index_s+1;
        }

        /// Find the best template
        for (std::size_t is = start_index_s; is < end_index_s; ++is) {
            std::size_t start_index_r(0);
            std::size_t end_index_r(histos.at(is).size());
            if (m_fix_index_r >= 0) {
                start_index_r = m_fix_index_r;
                end_index_r = m_fix_index_r+1;
            }
            for (std::size_t ir = start_index_r; ir < end_index_r; ++ir) {

                std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(histos.at(is).at(ir).Clone()));

                if (m_normalise && (std::fabs(tmp->Integral()) > 1e-6)) {
                    tmp->Scale(data_smeared->Integral()/tmp->Integral());
                }

                const double chi2 = data_smeared->Chi2Test(tmp.get(), m_chi2_option.c_str());
                m_chi2_vec[is][ir] = chi2;

                if (chi2 < m_best_chi2) {
                    m_best_chi2 = chi2;
                    m_best_index_s = is;
                    m_best_index_r = ir;
                }
            }
        }

        if (m_subtract) {
            for (auto& i : m_chi2_vec) {
                for (auto& j : i) {
                    j-= m_best_chi2;
                }
            }
        }

        bool s_is_ok(true);
        bool r_is_ok(true);
        const double best_s = GetBestValueS(&s_is_ok);
        const double best_r = GetBestValueR(&r_is_ok);

        if (s_is_ok && r_is_ok) {
            m_PE_s_values.emplace_back(best_s);
            m_PE_r_values.emplace_back(best_r);
        }
    }
}

std::size_t FFFitter::GetBestIndexS() const {
    return m_best_index_s;
}

std::size_t FFFitter::GetBestIndexR() const {
    return m_best_index_r;
}

double FFFitter::GetBestChi2() const {
    return m_best_chi2;
}

double FFFitter::GetBestValueS(bool* is_ok) const {
    return GetFittedParam(FoldingParameter::PARAMETER::S, false, is_ok);
}

double FFFitter::GetBestValueR(bool* is_ok) const {
    return GetFittedParam(FoldingParameter::PARAMETER::R, false, is_ok);
}

const std::vector<std::vector<double> >& FFFitter::GetFullChi2() const {
    return m_chi2_vec;
}

std::vector<double> FFFitter::GetBestProfileS() const {
    std::vector<double> result;

    for (const auto& is : m_chi2_vec) {
        result.emplace_back(is.at(m_best_index_r));
    }

    return result;
}

std::vector<double> FFFitter::GetBestProfileR() const {
    return m_chi2_vec.at(m_best_index_s);
}

double FFFitter::GetParamSuncertaintyChi2(bool* is_ok) const {
    return GetFittedParam(FoldingParameter::PARAMETER::S, true, is_ok);
}

double FFFitter::GetParamRuncertaintyChi2(bool* is_ok) const {
    return GetFittedParam(FoldingParameter::PARAMETER::R, true, is_ok);
}

int FFFitter::GetSsteps() const {
    return m_param_s->GetSteps();
}

int FFFitter::GetRsteps() const {
    return m_param_r->GetSteps();
}

double FFFitter::GetDeltaS() const {
    return m_delta_s;
}

double FFFitter::GetDeltaR() const {
    return m_delta_r;
}

double FFFitter::GetMinS() const {
    return m_param_s->GetMin();
}

double FFFitter::GetMinR() const {
    return m_param_r->GetMin();
}

double FFFitter::GetMaxS() const {
    return m_param_s->GetMax();
}

double FFFitter::GetMaxR() const {
    return m_param_r->GetMax();
}

const std::vector<double>& FFFitter::GetPEvaluesS() const {
    return m_PE_s_values;
}

const std::vector<double>& FFFitter::GetPEvaluesR() const {
    return m_PE_r_values;
}

std::unique_ptr<TH2D> FFFitter::Create2Dchi2Histo() const {

    std::unique_ptr<TH2D> result(new TH2D("",
                                          "",
                                          m_param_s->GetSteps(),
                                          m_param_s->GetMin() - 0.5*m_delta_s,
                                          m_param_s->GetMax() + 0.5*m_delta_s,
                                          m_param_r->GetSteps(),
                                          m_param_r->GetMin() - 0.5*m_delta_r,
                                          m_param_r->GetMax() + 0.5*m_delta_r));

    for (int is = 0; is < m_param_s->GetSteps(); ++is) {
        for (int ir = 0; ir < m_param_r->GetSteps(); ++ir) {
            result->SetBinContent(is+1, ir+1, m_chi2_vec.at(is).at(ir));
        }
    }

    result->GetXaxis()->SetTitle("param s");
    result->GetYaxis()->SetTitle("param r");
    result->GetZaxis()->SetTitle("#chi^{2} - C");

    return result;
}

double FFFitter::IndexToValue(const int& index, const FoldingParameter::PARAMETER& par) const {
    double result(9999);

    if (par == FoldingParameter::PARAMETER::S) {
        result = m_param_s->GetMin() + index*m_delta_s;
    } else if (par == FoldingParameter::PARAMETER::R) {
        result = m_param_r->GetMin() + index*m_delta_r;
    } else {
        std::cerr << "FFFitter::IndexToValue: ERROR Unknown parameter, returning -1" << std::endl;
        return -1;
    }

    return result;
}
    
double FFFitter::GetFittedParam(const FoldingParameter::PARAMETER& par,
                                const bool& error,
                                bool* is_ok) const {
    std::vector<double> input;
    std::vector<double> x;

    *is_ok = true;

    if (par == FoldingParameter::PARAMETER::S) {
        const std::vector<double> tmp = GetBestProfileS();
        if ((m_best_index_s > (m_n_neighbour_indices-1)) && (m_best_index_s < (m_param_s->GetSteps()-m_n_neighbour_indices))) { 
            for (int i = -m_n_neighbour_indices; i < (m_n_neighbour_indices+1); ++i) {
                const double index = i + m_best_index_s;
                x.emplace_back(m_param_s->GetMin() + index*m_delta_s);
                input.emplace_back(tmp.at(index));
            }
        } else if (m_best_index_s < m_n_neighbour_indices) {
            for (int i = 0; i < (2*m_n_neighbour_indices+1); ++i) {
                const double index = i + m_best_index_s;
                x.emplace_back(m_param_s->GetMin() + index*m_delta_s);
                input.emplace_back(tmp.at(index));
            }
        } else {
            for (int i = -(2*m_n_neighbour_indices); i < m_n_neighbour_indices; ++i) {
                const double index = i + m_best_index_s;
                x.emplace_back(m_param_s->GetMin() + index*m_delta_s);
                input.emplace_back(tmp.at(index));
            }
        }
    } else if (par == FoldingParameter::PARAMETER::R) {
        const std::vector<double> tmp = GetBestProfileR();
        if ((m_best_index_r > (m_n_neighbour_indices-1)) && (m_best_index_r < (m_param_r->GetSteps()-m_n_neighbour_indices))) { 
            for (int i = -m_n_neighbour_indices; i < (m_n_neighbour_indices+1); ++i) {
                const double index = i + m_best_index_r;
                x.emplace_back(m_param_r->GetMin() + index*m_delta_r);
                input.emplace_back(tmp.at(index));
            }
        } else if (m_best_index_r < 1) {
            for (int i = 0; i < (2*m_n_neighbour_indices+1); ++i) {
                const double index = i + m_best_index_r;
                x.emplace_back(m_param_r->GetMin() + index*m_delta_r);
                input.emplace_back(tmp.at(index));
            }
        } else {
            for (int i = -(2*m_n_neighbour_indices); i < m_n_neighbour_indices; ++i) {
                const double index = i + m_best_index_r;
                x.emplace_back(m_param_r->GetMin() + index*m_delta_r);
                input.emplace_back(tmp.at(index));
            }
        }
    } else {
        std::cerr << "FFFitter::GetFittedParam: ERROR Unknown parameter" << std::endl;
        return -1;
    }

    TGraph g(x.size(), &x[0], &input[0]);
    TF1 func("func","[0]*(x-[1])*(x-[1]) + [3]", x[0], x.back());
    if (par == FoldingParameter::PARAMETER::S) {
        func.SetParameter(0, 500000);
    } else if (par == FoldingParameter::PARAMETER::R) {
        func.SetParameter(0, 10000);
    }
    const int index = (x.size())/2;
    func.SetParameter(1, x[index]);
    func.SetParameter(2, 0);
    const Int_t converged = g.Fit(&func,"Q");
    if (converged != 0) {
        *is_ok = false;
    }

    if (func.GetChisquare() > 3) {
        *is_ok = false;
    }

    /// the sigma should be 1/sqrt{a} if y = a*(x-b)^2
    const double a = func.GetParameter(0);
    const double b = func.GetParameter(1);

    if ((b <= x.at(0)) || (b >= x.back())) {
        *is_ok = false;        
    }

    if ((*is_ok) == false && !error) {
        if (par == FoldingParameter::PARAMETER::S) {
            return IndexToValue(m_best_index_s, FoldingParameter::PARAMETER::S);
        } else if (par == FoldingParameter::PARAMETER::R){
            return IndexToValue(m_best_index_r, FoldingParameter::PARAMETER::R);
        }
    }

    if (error) {
        return std::sqrt(1./a);
    } else {
        return b;
    }
}

void FFFitter::Poissonise(TH1D* h, TRandom3* rnd) const {
    const int nbins = h->GetNbinsX();

    for (int ibin = 1; ibin <= nbins; ++ibin) {
        const double poissonised = rnd->Poisson(h->GetBinContent(ibin));
        h->SetBinContent(ibin, poissonised);
        h->SetBinError(ibin, std::sqrt(poissonised));
    }
}
