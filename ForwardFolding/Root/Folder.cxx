#include "ForwardFolding/ForwardFolding/Folder.h"

#include <iostream>

Folder::Folder() :
    m_param_r(nullptr),
    m_param_s(nullptr),
    m_delta_r(0),
    m_delta_s(0) {}

Folder::~Folder() {
}

void Folder::SetParameterR(const int& steps,
                           const float& min,
                           const float& max) {

    if (steps <= 1) { 
        std::cerr << "Folder::SetParameterR: Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "Folder::SetParameterR: Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_param_r = std::unique_ptr<FoldingParameter>(new FoldingParameter(steps, min, max));

    m_delta_r = static_cast<float>((max - min) / (steps-1));
}

void Folder::SetParameterS(const int& steps,
                           const float& min,
                           const float& max) {

    if (steps <= 1) { 
        std::cerr << "Folder::SetParameterS: Number of steps <= 1!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (min >= max) {
        std::cerr << "Folder::SetParameterS: Min >= max!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_param_s = std::unique_ptr<FoldingParameter>(new FoldingParameter(steps, min, max));
    m_delta_s = static_cast<float>((max - min) / (steps-1));
}

std::vector<std::vector<float> > Folder::Smear(const float& reco,
                                               const float& truth) const {

    if (!m_param_s || !m_param_r) {
        std::cerr << "Folder::Smear: The parameters are not set!" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::vector<std::vector<float> > result(m_param_s->GetSteps(),
                                            std::vector<float>(m_param_r->GetSteps()));

    for (int s_steps = 0; s_steps < m_param_s->GetSteps(); ++s_steps) {
        for (int r_steps = 0; r_steps < m_param_r->GetSteps(); ++r_steps) {
            const float actual_s = m_param_s->GetMin() + m_delta_s*s_steps;
            const float actual_r = m_param_r->GetMin() + m_delta_r*r_steps;
            result[s_steps][r_steps] = FFformula(reco, truth, actual_s, actual_r);
        }
    }

    return result;
}

std::pair<int,int> Folder::GetFoldingSizes() const {
    if (!m_param_s || !m_param_r) {
        std::cerr << "Folder::GetFoldingSizes: The parameters are not set!" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::pair<int,int> result = std::make_pair<int,int>(m_param_s->GetSteps(), m_param_r->GetSteps());

    return result;
}

float Folder::FFformula(const float& reco,
                        const float& truth,
                        const float& s,
                        const float& r) const {

    return s*reco + (reco - truth)*(r-s);
}
