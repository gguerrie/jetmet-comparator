# JetMET comparator [![build status](https://gitlab.cern.ch/tdado/wmasslocalprocessing/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/tdado/wmasslocalprocessing/commits/master)

This code is used to compare the jet response ratio plots for MC20 and MC21, in R22.
The code produces histograms and various kinds of plots.

If you have any questions, fell free to contact `tomas.dado@cern.ch`


## Table of contents
1.  [Getting the code](#getting-the-code)
2.  [Compiling the code](#compiling-the-code)
3.  [Creating file list](#creating-file-list)
4.  [Producing histograms](#producing-histograms)
5.  [Producing plots](#producing-plots)


## Getting the code

The suggested file structure is as follows:
```
mkdir WmassAnalysis
cd WmassAnalysis
mkdir OutputHistos Plots FitPlots Comparison
```
And then get the code using the following command:
```
git clone ssh://git@gitlab.cern.ch:7999/tdado/wmasslocalprocessing.git
```

## Compiling the code
Change directory to the main repository:

```
cd jetmet-comparator
```
Run the setup script:

```
source setup.sh
```

## Creating file list
Now you can produce file list (a text file with the paths to the input files) and also a sumWeights file (a text file with sumWeights, needed for normalisation).
You can do this by going to `scripts/`:
```
cd ../scripts/
```

Change the path to the folder in `makeFileList.sh`.

The folder can contain all files for signal, background and data with any kind of campaign and a mix of FullSim and FastSim samples.
The code fill search for ROOT files in the directory and read the metadata information

Produce the files:
```
./makeFileList.sh
```

This will create the two text files. You can inspect them.

## Producing histograms
Now we have everything ready to process the files in `file_list.txt`.
To produce the nominal (non-systematics) histograms do:

```
cd ../ #To get to the main folder
./build/bin/w-mass Test
```

The argument (`Test`) can be any string. The code will create a new folder in `../OutputHistos/` with the name that you pass as as argument and put all the output files in the new folder.


## Producing plots
When at least the nominal production of histograms is finished, you can produce plots comparing MC20 and MC21 by running:
```
./build/bin/JetMETplot <folder1> <folder2>
```
