#include "WmassAnalysis/Common.h"

#include "WmassAnalysis/Event.h"

#include "TF1.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TString.h"

#include <algorithm>
#include <iostream>

void Common::AddOverflow(TH1D* hist) {
    const int nbins = hist->GetNbinsX();

    hist->SetBinContent(1, hist->GetBinContent(0) + hist->GetBinContent(1) );
    hist->SetBinContent(nbins, hist->GetBinContent(nbins) + hist->GetBinContent(nbins+1) );
    hist->SetBinContent(0, 0 );
    hist->SetBinContent(nbins+1, 0 );
    
    hist->SetBinError(1, std::sqrt(hist->GetBinError(0)*hist->GetBinError(0) + hist->GetBinError(1)*hist->GetBinError(1)));
    hist->SetBinError(nbins, std::sqrt(hist->GetBinError(nbins)*hist->GetBinError(nbins) + hist->GetBinError(nbins+1)*hist->GetBinError(nbins+1)));
    hist->SetBinError(0, 0);
    hist->SetBinError(nbins+1, 0);
}

void Common::AddOverflow(TH2D *hist) {
	int nbinsX = hist->GetNbinsX();
	int nbinsY = hist->GetNbinsY();

	for (int i = 1; i < nbinsX+1; i++){
		hist->SetBinContent(i,1, hist->GetBinContent(i,0) + hist->GetBinContent(i,1) );
		hist->SetBinContent(i,nbinsY, hist->GetBinContent(i,nbinsY) + hist->GetBinContent(i,nbinsY+1) );
		hist->SetBinContent(i,0, 0 );
		hist->SetBinContent(i,nbinsY+1, 0 );
	
		hist->SetBinError(i,1, std::sqrt(hist->GetBinError(i,0)*hist->GetBinError(i,0) + hist->GetBinError(i,1)*hist->GetBinError(i,1)));
		hist->SetBinError(i,nbinsY, std::sqrt(hist->GetBinError(i,nbinsY)*hist->GetBinError(i,nbinsY) + hist->GetBinError(i,nbinsY+1)*hist->GetBinError(i,nbinsY+1)));
		hist->SetBinError(i,0, 0);
		hist->SetBinError(i,nbinsY+1, 0);
	}

	for (int i = 1; i < nbinsY+1; i++){
		hist->SetBinContent(1,i, hist->GetBinContent(0,i) + hist->GetBinContent(1,i) );
		hist->SetBinContent(nbinsX,i, hist->GetBinContent(nbinsX,i) + hist->GetBinContent(nbinsX+1,i) );
		hist->SetBinContent(0,i, 0 );
		hist->SetBinContent(nbinsX+1,i, 0 );
	
		hist->SetBinError(1,i, std::sqrt(hist->GetBinError(0,i)*hist->GetBinError(0,i) + hist->GetBinError(1,i)*hist->GetBinError(1,i)));
		hist->SetBinError(nbinsX,i, std::sqrt(hist->GetBinError(nbinsX,i)*hist->GetBinError(nbinsX,i) + hist->GetBinError(nbinsX+1,i)*hist->GetBinError(nbinsX+1,i)));
		hist->SetBinError(0,1, 0);
		hist->SetBinError(nbinsX+1,i, 0);
	}
}

void Common::NormaliseMatrix(TH2D* mat, bool byRow) {
    const int binsx = mat->GetNbinsX();
    const int binsy = mat->GetNbinsY();


    if (byRow) {
        for (int iy = 0; iy <= binsy; ++iy) {
            double sum(0);
            for (int ix = 1; ix <= binsx; ++ix) {
                sum+= mat->GetBinContent(ix, iy);
            }
            for (int ix = 1; ix <= binsx; ++ix) {
                if (std::fabs(sum) > 1e-6) mat->SetBinContent(ix, iy, (mat->GetBinContent(ix,iy)/sum));
            }
        }
    } else {
        for (int ix = 0; ix <= binsx; ++ix) {
            double sum(0);
            for (int iy = 1; iy <= binsy; ++iy) {
                sum+= mat->GetBinContent(ix, iy);
            }
            for (int iy = 1; iy <= binsy; ++iy) {
                if (std::fabs(sum) < 1e-6) mat->SetBinContent(ix, iy, (mat->GetBinContent(ix,iy)/sum));
            }
        }
    }
}

std::size_t Common::IndexFromVec(const float& value,
                                 const std::vector<float>& binning,
                                 const bool& is_abs) {

    const float value_fixed = is_abs ? std::fabs(value) : value;

    for (std::size_t i = 0; i < binning.size() - 1; ++i) {
        /// is not the last
        if (value_fixed >= binning.at(i) && value_fixed < binning.at(i+1)) {
            return i;
        }
    }

    if (is_abs) {
        return binning.size()-2;
    }

    /// is the last bin
    return binning.size()-1;
}
    
std::string Common::GetNameFromVec(const float& value,
                                   const std::vector<float>& binning,
                                   const bool& is_abs) {

    std::string result;

    const std::size_t index = Common::IndexFromVec(value, binning, is_abs);

    if (index < binning.size() -1) {
        if (index == 0) {
            result = Form("0.00-%.2f", binning.at(1));
        } else {
            result = Form("%.2f-%.2f", binning.at(index), binning.at(index+1));
        }
    } else {
        result = Form("%.2f-#infty", binning.at(index));
    }

    return result;
}
    
std::vector<std::string> Common::GetBinsNames(const std::vector<float>& binning,
                                              const bool& is_pt) {

    std::vector<std::string> result;
    const std::string prefix = is_pt ? "pt_" : "eta_";

    for (std::size_t ibin = 0; ibin < (binning.size() - 1); ++ibin) {
        const std::string name = Form("%s%.2f_%.2f", prefix.c_str(), binning.at(ibin), binning.at(ibin+1));
        result.emplace_back(name);
    }

    if (is_pt) {
        const std::string name = Form("%s%.2f_inf", prefix.c_str(), binning.at(binning.size() - 1));
        result.emplace_back(name);
    }

    return result;
}

std::string Common::RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove) {
    std::string result = str;
    result.erase(std::remove(result.begin(), result.end(), '_'), result.end());

    for (const auto& i : remove) {
        auto itr = result.find(i.c_str());       
        if (itr == std::string::npos) continue;

        const auto size = i.size();
        result.erase(itr, size);
    }

    return result;
}
    
float Common::GetMax(TH1D* h1, TH1D* h2) {
    const float max1 = h1->GetMaximum();
    const float max2 = h2->GetMaximum();

    return std::max(max1, max2);
}
    
double Common::GetMean(const std::vector<double>& v) {
    double result(0);

    for (const auto& i : v) {
        result+= i;
    }
    
    result = static_cast<double>(result/v.size());

    return result;
}

double Common::GetSigma(const std::vector<double>& v) {
    if (v.size() < 2) {
        std::cerr << "Common::GetSigma: Size of vector < 2. Returning -1.\n";
        return -1;
    }

    const double mean = Common::GetMean(v);

    double result(0);
    for (const auto& i : v) {
        result+= (i - mean)*(i - mean);
    }

    result = static_cast<double>(result/(v.size() - 1));
    return std::sqrt(result);
}
    
double Common::GetMeanGaus(const std::vector<double>& y, const double& min, const double& max, double& error) {

    TH1D h("","", 30, min, max);

    for (const auto& i : y) {
        h.Fill(i);
    }

    TF1 func("func","gaus", min, max);
    func.SetParameter(0, h.GetMaximum());
    func.SetParameter(1,1);
    func.SetParameter(2,0.01);

    const Int_t converge = h.Fit(&func,"Q");
    if (converge != 0) {
        std::cerr << "Common::GetMeanGaus: Fit didnt converge, returning 0\n";
        return 0;
    }
    
    if (func.GetChisquare() > 3) {
        std::cout << "Common::GetMeanGaus: Gaussian fit has chi^2/NDF = " << func.GetChisquare() << ", the result is probably nonsense\n";
    }

    error = func.GetParError(1);

    return func.GetParameter(1);
}

double Common::GetSigmaGaus(const std::vector<double>& y, const double& min, const double& max, double& error) {

    TH1D h("", "", 30, min, max);
    for (const auto& i : y) {
        h.Fill(i);
    }
    
    TF1 func("func","gaus", min, max);
    func.SetParameter(0, h.GetMaximum());
    func.SetParameter(1,1);
    func.SetParameter(2,0.001);

    const Int_t converge = h.Fit(&func,"Q");
    if (converge != 0) {
        std::cerr << "Common::GetSigmaGaus: Fit didnt converge, returning -1\n";
        return -1;
    }

    if (func.GetChisquare() > 3) {
        std::cout << "Common::GetSigmaGaus: Gaussian fit has chi^2/NDF " << func.GetChisquare() << ", the result is probably nonsense\n";
    }

    error = func.GetParError(2);

    return func.GetParameter(2);
}
    
std::vector<Systematic> Common::GetSystSameName(const std::vector<Systematic>& all,
                                                const std::string& name) {

    std::vector<Systematic> result;

    for (const auto& isyst : all) {
        if (isyst.name == name) {
            result.emplace_back(isyst);
        }
    }

    return result;
}
    
std::vector<std::string> Common::GetSystUniqueNames(const std::vector<Systematic>& all) {
    std::vector<std::string> result;

    for (const auto& isyst : all) {
        if (isyst.name == "") continue;
        if (std::find(result.begin(), result.end(), isyst.name) == result.end()) {
            result.emplace_back(isyst.name);
        }
    }

    return result;
}
    
bool Common::SystematicsAreConsistent(const std::vector<Systematic>& systs) {
    if (systs.size() == 0) return true;
    
    std::vector<std::string> samples;
    const SYSTEMATICTYPE type = systs.at(0).type;

    for (const auto& isyst : systs) {
        if (isyst.type != type) return false;
        if (std::find(samples.begin(), samples.end(), isyst.target) == samples.end()) {
            samples.emplace_back(isyst.target);
        } else {
            return false;
        }
    }

    return true;
}

int Common::GetSystematicIndex(const std::vector<Systematic>& syst, const std::string& name) {
    for (int isyst = 0; isyst < (int)syst.size(); ++isyst) {
        if (syst.at(isyst).target == name) return isyst;    
    }

    return -1;
}
    
float Common::CalculateTruthWmass(const Event& event) {
    if (event.isDilepton) return -1;
    if ((event.index_W_1 < 0) || (event.index_W_2 < 0)) return -1;

    TLorentzVector j1, j2;

    j1.SetPtEtaPhiE(event.truth_jet_paired_pt->at(event.index_W_1)/1e3,
                    event.truth_jet_paired_eta->at(event.index_W_1),
                    event.truth_jet_paired_phi->at(event.index_W_1),
                    event.truth_jet_paired_e->at(event.index_W_1)/1e3);
    j2.SetPtEtaPhiE(event.truth_jet_paired_pt->at(event.index_W_2)/1e3,
                    event.truth_jet_paired_eta->at(event.index_W_2),
                    event.truth_jet_paired_phi->at(event.index_W_2),
                    event.truth_jet_paired_e->at(event.index_W_2)/1e3);

    return (j1+j2).M();
}

void Common::AddMapUncertainty(std::map<std::string, std::pair<double,double> >& map,
                               const double& up,
                               const double& down,
                               const std::string& name) {

    auto itr = map.find(name);
    if (up >= 0 && down <= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(up, down);
        } else {
            itr->second.first  = std::hypot(itr->second.first, up);
            itr->second.second = -std::hypot(itr->second.second, down);
        }
    } else if (up <= 0 && down >= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(down, up);
        } else {
            itr->second.first  = std::hypot(itr->second.first, down);
            itr->second.second = -std::hypot(itr->second.second, up);
        }
    } else if (up >= 0 && down >= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(up, 0);
        } else {
            itr->second.first = std::hypot(itr->second.first, up);
        }
    } else if (up <= 0 && down <= 0) {
        if (itr == map.end()) {
            map[name] = std::make_pair(0, down);
        } else {
            itr->second.second = -std::hypot(itr->second.second, up);
        }
    } else {
        std::cerr << "Common::AddMapUncertainty: Weird composition of uncertainties" << std::endl;
        exit(EXIT_FAILURE);
    }
}

double Common::SumCategories(const std::map<std::string, std::pair<double,double> >& map,
                             const bool& isUp) {

    double sum(0);
    for (const auto& itr : map) {
        sum += isUp ? itr.second.first*itr.second.first : itr.second.second*itr.second.second; 
    }

    return std::sqrt(sum);
}

    
std::string Common::NiceRangeLabels(const std::string& in, const bool isEta) {

    //inputs like eta_0.00_1.00 and pt_0.00_30.00

    // remove the prefix
    std::string subString = in.substr(isEta ? 4 : 3, in.length());

    // split into 2 parts based on "_"
    const std::size_t pos = subString.find('_');
    if (pos == subString.length()) {
        std::cerr << "Common::NiceRangeLabels: Cannot split the string " << in << "\n";
        return ""; 
    }

    std::string first  = subString.substr(0, pos);
    std::string second = subString.substr(pos+1, subString.length());

    // merge the strings together
    // we can round pT but cannot round eta
    std::string result = isEta ? TString::Format("%.2f", std::stof(first)).Data() : std::to_string(static_cast<int>(std::stof(first)));
    result += " <";
    result += isEta ? " |#eta|" : " p_{T}";
    result += " #leq ";
    result += isEta ? TString::Format("%.2f", std::stof(second)).Data() : std::to_string(static_cast<int>(std::stof(second)));
    if (!isEta) {
        result += " GeV";
    }
    return result;

}