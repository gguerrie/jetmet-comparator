#include "WmassAnalysis/Comparator.h"
#include "WmassAnalysis/Common.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TCanvas.h"
#include "TDirectory.h"
#include "TH1D.h"
#include "TKey.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLine.h"
#include "TPad.h"
#include "TSystem.h"

#include <iostream>

Comparator::Comparator() :
    m_folder1_name(""),
    m_file1_name(""),
    m_histo1_name(""),
    m_folder2_name(""),
    m_file2_name(""),
    m_histo2_name(""),
    m_normalise(false) {

        SetAtlasStyle();
        AddAllLabels();
}

void Comparator::SetFirstHisto(const std::string& folder,
                               const std::string& file,
                               const std::string name) {
    m_folder1_name = folder;
    m_file1_name = file;
    m_histo1_name = name;
}

void Comparator::SetSecondHisto(const std::string& folder,
                                const std::string& file,
                                const std::string name) {
    m_folder2_name = folder;
    m_file2_name = file;
    m_histo2_name = name;
}

void Comparator::OpenRootFiles() {
    const std::string path1 = "../OutputHistos/"+m_folder1_name+"/"+m_file1_name+"_"+m_folder1_name+".root";
    const std::string path2 = "../OutputHistos/"+m_folder2_name+"/"+m_file2_name+"_"+m_folder2_name+".root";
    m_file1 = std::unique_ptr<TFile>(TFile::Open(path1.c_str()));
    m_file2 = std::unique_ptr<TFile>(TFile::Open(path2.c_str()));

    if (!m_file1 || !m_file2) {
        std::cout << "Comparator::OpenRootFiles: Cannot open the ROOT files" << std::endl;
        exit(EXIT_FAILURE);
    }
}

void Comparator::GetHistoList() {
    m_file1->cd(m_histo1_name.c_str());

    TIter next(gDirectory->GetListOfKeys());
    TKey *key;
    while ((key = static_cast<TKey*>(next()))) {
        if (!key) continue;
        if (key->IsFolder()) continue;
        m_histo_list.emplace_back(key->GetName());
    }

    delete key;
}

void Comparator::PlotAllHistos() {
    const std::string out_name = m_folder1_name+"_"+m_file1_name+"_"+m_histo1_name+m_folder2_name+"_"+m_file2_name+"_"+m_histo2_name;
    gSystem->mkdir(("../Comparison/"+out_name).c_str());
    
    for (const auto& iname : m_histo_list) {
        std::cout << "Comparator::PlotAllHistos: Plotting: " << iname << "\n";
        PlotSingleHisto(iname);
    }
}

void Comparator::CloseRootFiles() {
    m_file1->Close();
    m_file2->Close();
}

void Comparator::PlotSingleHisto(const std::string& name) const {
    TH1D* h1 = static_cast<TH1D*>(m_file1->Get((m_histo1_name+"/"+name).c_str()));
    TH1D* h2 = static_cast<TH1D*>(m_file2->Get((m_histo2_name+"/"+name).c_str()));

    if (!h1 || !h2) {
        std::cout << "Comparator::PlotSingleHisto: Cannot read one of the histograms, skipping\n";
        return;
    }

    if (m_normalise) {
        h1->Scale(1./h1->Integral());
        h2->Scale(1./h2->Integral());
    }

    auto itr = m_labels_map.find(name);
    if (itr == m_labels_map.end()) {
        std::cout << "Comparator::PlotSingleHisto: Cannot find labels map for: " << name << ", skipping\n";
        return;
    }

    const Labels label = itr->second;
    
    /// Canvas
    TCanvas c("","",800,600);
    TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
    TPad pad2("pad2","pad2", 0.0, 0.010, 1.0, 0.3);
    pad1.SetBottomMargin(0.001);
    pad1.SetBorderMode(0);
    pad2.SetBottomMargin(0.5);
    pad1.SetTicks(1,1);
    pad2.SetTicks(1,1);
    pad1.Draw();
    pad2.Draw();

    pad1.cd();

    h1->SetLineColor(kBlack);
    h2->SetLineColor(kRed);

    const float max = Common::GetMax(h1,h2);
    h1->GetYaxis()->SetRangeUser(0.0001, 1.7*max);
    const float bin_width = h1->GetBinWidth(1);
    if (m_normalise) {
        if (bin_width < 1) {
            h1->GetYaxis()->SetTitle(Form("Normalised / %.2f %s", bin_width, label.units.c_str()));
        } else {
            h1->GetYaxis()->SetTitle(Form("Normalised / %.1f %s", bin_width, label.units.c_str()));
        }
    } else {
        if (bin_width < 1) {
            h1->GetYaxis()->SetTitle(Form("Events / %.2f %s", bin_width, label.units.c_str()));
        } else {
            h1->GetYaxis()->SetTitle(Form("Events / %.1f %s", bin_width, label.units.c_str()));
        }
    }

    h1->GetYaxis()->SetLabelSize(0.05);
    h1->GetYaxis()->SetLabelFont(42);
    h1->GetYaxis()->SetTitleFont(42);
    h1->GetYaxis()->SetTitleSize(0.07);
    h1->GetYaxis()->SetTitleOffset(1.1);
    h1->Draw("HIST");
    h2->Draw("HIST same");

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.06);
    l1.SetNDC();
    l1.DrawLatex(0.21, 0.88, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.06);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(0.33, 0.88, "Internal");
    
    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.06);
    l3.SetTextFont(42);
    l3.SetNDC();
    l3.DrawLatex(0.21, 0.81, "#sqrt{s} = 13 TeV");

    TLegend leg(0.55, 0.75, 0.9, 0.92);
    if (m_file1_name != m_file2_name) {
        leg.AddEntry(h1, m_file1_name.c_str(), "l");
        leg.AddEntry(h2, m_file2_name.c_str(), "l");
        leg.SetFillColor(0);
        leg.SetLineColor(0);
        leg.SetBorderSize(0);
        leg.SetTextFont(72);
        leg.SetTextSize(0.045);
        leg.Draw("same");
    }
    
    TLegend leg2(0.55, 0.60, 0.90, 0.70);
    if (m_folder1_name != m_folder2_name) {
        leg2.AddEntry(h1, m_folder1_name.c_str(), "l");
        leg2.AddEntry(h2, m_folder2_name.c_str(), "l");
        leg2.SetFillColor(0);
        leg2.SetLineColor(0);
        leg2.SetBorderSize(0);
        leg2.SetTextFont(72);
        leg2.SetTextSize(0.045);
        leg2.Draw("same");
    }

    TLegend leg3(0.20, 0.65, 0.40, 0.75);
    if (m_histo1_name != m_histo2_name) {
        leg3.AddEntry(h1, m_histo1_name.c_str(), "l");
        leg3.AddEntry(h2, m_histo2_name.c_str(), "l");
        leg3.SetFillColor(0);
        leg3.SetLineColor(0);
        leg3.SetBorderSize(0);
        leg3.SetTextFont(72);
        leg3.SetTextSize(0.045);
        leg3.Draw("same");
    }
    
    pad1.RedrawAxis();

    pad2.cd();
    std::unique_ptr<TH1D> ratio(static_cast<TH1D*>(h1->Clone()));
    ratio->Divide(h2);
    
    ratio->GetXaxis()->SetLabelFont(42);
    ratio->GetXaxis()->SetLabelSize(0.15);
    ratio->GetXaxis()->SetLabelOffset(0.01);
    ratio->GetXaxis()->SetTitleFont(42);
    ratio->GetXaxis()->SetTitleSize(0.20);
    ratio->GetXaxis()->SetTitleOffset(0.9);
    ratio->GetXaxis()->SetNdivisions(505);
    ratio->GetXaxis()->SetTitle(label.x_axis.c_str());
    ratio->GetYaxis()->SetRangeUser(0.7,1.3);
    ratio->GetYaxis()->SetLabelFont(42);
    ratio->GetYaxis()->SetLabelSize(0.15);
    ratio->GetYaxis()->SetLabelOffset(0.03);
    ratio->GetYaxis()->SetTitleFont(42);
    ratio->GetYaxis()->SetTitleSize(0.15);
    ratio->GetYaxis()->SetTitleOffset(0.5);
    ratio->GetYaxis()->SetNdivisions(505);
    ratio->GetYaxis()->SetTitle("ratio");
    
    ratio->Draw("HIST");
    
    /// Draw line
    const float min_line = ratio->GetXaxis()->GetBinLowEdge(1);
    const float max_line = ratio->GetXaxis()->GetBinUpEdge(ratio->GetNbinsX());
    TLine line(min_line, 1, max_line, 1);
    line.SetLineColor(kRed);
    line.SetLineStyle(2);
    line.SetLineWidth(3);
    line.Draw("same");

    pad2.RedrawAxis();

    const std::string out_name = m_folder1_name+"_"+m_file1_name+"_"+m_histo1_name+m_folder2_name+"_"+m_file2_name+"_"+m_histo2_name;

    c.Print(("../Comparison/"+out_name+"/"+name+".png").c_str());
    c.Print(("../Comparison/"+out_name+"/"+name+".pdf").c_str());
    c.Print(("../Comparison/"+out_name+"/"+name+".eps").c_str());
}

void Comparator::AddAllLabels() {
    AddSingleLabel("jet1_pt", "leading jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet1_eta", "leading jet |#eta| [-]", "");
    AddSingleLabel("jet1_phi", "leading jet #phi [-]", "");
    AddSingleLabel("jet1_jvt", "leading jet JVT score [-]", "");
    AddSingleLabel("jet2_pt", "second jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet2_eta", "second jet |#eta| [-]", "");
    AddSingleLabel("jet2_phi", "second jet #phi [-]", "");
    AddSingleLabel("jet2_jvt", "second jet JVT score [-]", "");
    AddSingleLabel("jet3_pt", "third jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet3_eta", "third jet |#eta| [-]", "");
    AddSingleLabel("jet3_phi", "third jet #phi [-]", "");
    AddSingleLabel("jet3_jvt", "third jet JVT score [-]", "");
    AddSingleLabel("jet4_pt", "fourth jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet4_eta", "fourth jet |#eta| [-]", "");
    AddSingleLabel("jet4_phi", "fourth jet #phi [-]", "");
    AddSingleLabel("jet4_jvt", "fourth jet JVT score [-]", "");
    AddSingleLabel("jet5_pt", "fifth jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet5_eta", "fifth jet |#eta| [-]", "");
    AddSingleLabel("jet5_phi", "fifth jet #phi [-]", "");
    AddSingleLabel("jet5_jvt", "fifth jet JVT score [-]", "");
    AddSingleLabel("jet6_pt", "sixth jet p_{T} [GeV]", "GeV");
    AddSingleLabel("jet6_eta", "sixth jet |#eta| [-]", "");
    AddSingleLabel("jet6_phi", "sixth jet #phi [-]", "");
    AddSingleLabel("jet6_jvt", "sixth jet JVT score [-]", "");
    AddSingleLabel("bjet1_pt", "leading b-jet p_{T} [GeV]", "GeV");
    AddSingleLabel("bjet1_eta", "leading b-jet |#eta| [-]", "");
    AddSingleLabel("bjet1_phi", "leading b-jet #phi [-]", "");
    AddSingleLabel("bjet2_pt", "second b-jet p_{T} [GeV]", "GeV");
    AddSingleLabel("bjet2_eta", "second b-jet |#eta| [-]", "");
    AddSingleLabel("bjet2_phi", "second b-jet #phi [-]", "");
    
    AddSingleLabel("HT", "H_{T} [GeV]", "GeV");
    AddSingleLabel("jet_n", "jet multiplicity [-]", "");
    AddSingleLabel("bjet_n", "b-jet multiplicity [-]", "");
    
    AddSingleLabel("el_pt", "electron p_{T} [GeV]", "GeV");
    AddSingleLabel("el_eta", "electron |#eta| [-]", "");
    AddSingleLabel("el_phi", "electron #phi [-]", "");
    AddSingleLabel("mu_pt", "muon p_{T} [GeV]", "GeV");
    AddSingleLabel("mu_eta", "muon |#eta| [-]", "");
    AddSingleLabel("mu_phi", "muon #phi [-]", "");
    AddSingleLabel("met", "E_{T}^{miss} [GeV]", "GeV");
    AddSingleLabel("met_phi", "E_{T}^{miss} #phi [-]", "");
    AddSingleLabel("average_mu", "average #mu [-]", "");
    AddSingleLabel("actual_mu", "actual #mu [-]", "");
    AddSingleLabel("chi2", "#chi^2 [-]", "");
    
    AddSingleLabel("Wjet1_pt", "first jet from W p_{T} [GeV]", "GeV");
    AddSingleLabel("Wjet1_eta", "first jet from W |#eta| [-]", "");
    AddSingleLabel("Wjet1_phi", "first jet from W #phi [-]", "");
    AddSingleLabel("Wjet2_pt", "second jet from W p_{T} [GeV]", "GeV");
    AddSingleLabel("Wjet2_eta", "second jet from W |#eta| [-]", "");
    AddSingleLabel("Wjet2_phi", "second jet from W #phi [-]", "");
    AddSingleLabel("W_pt", "W p_{T} [GeV]", "GeV");
    AddSingleLabel("W_eta", "W |#eta| [-]", "");
    AddSingleLabel("W_phi", "W #phi [-]", "");
    AddSingleLabel("W_m", "W mass [GeV]", "GeV");
    AddSingleLabel("W_m_peak", "W mass [GeV]", "GeV");
    AddSingleLabel("W_m_peak_shifted", "W mass [GeV]", "GeV");
    AddSingleLabel("W_m_peak_60_90", "W mass [GeV]", "GeV");
    AddSingleLabel("W_m_possible", "W mass [GeV]", "GeV");
    AddSingleLabel("W_m_peak_possible", "W mass [GeV]", "GeV");
    AddSingleLabel("W_m_impossible", "W mass [GeV]", "GeV");
    AddSingleLabel("W_m_peak_impossible", "W mass [GeV]", "GeV");
    
    AddSingleLabel("truth_W_m", "particle truth W mass [GeV]", "GeV");
    AddSingleLabel("truth_W_pt", "particle truth W p_{T} [GeV]", "GeV");
    AddSingleLabel("truth_W_eta", "particle truth W |#eta| [-]", "");
    AddSingleLabel("reco_truth_ratio_W_m", "reco W mass /particle truth W mass [-]", "");
    AddSingleLabel("truth_W_jets_dR", "particle truth #Delta R jets from W [-]", "");
    AddSingleLabel("truth_W_jets_dR", "particle truth #Delta R jets from W [-]", "");
    AddSingleLabel("truth_W_jets_vec_sum_pt", "particle truth vector sum p_{T} jets from W [GeV]", "");
    AddSingleLabel("truth_W_jets_scalar_sum_pt", "particle truth scalar sum p_{T} jets from [GeV]", "");
    AddSingleLabel("truth_W_quark_pt", "parton truth W p_{T} [GeV]", "");
    AddSingleLabel("truth_W_quark_eta", "parton truth W |#eta| [-]", "");
    AddSingleLabel("truth_W_quark_m", "parton truth W mass [GeV]", "");
    AddSingleLabel("truth_W_identified_pt", "reco matched particle truth W p_{T} [GeV]", "");
    AddSingleLabel("truth_W_identified_eta", "reco matched particle truth W |#eta| [-]", "");
    AddSingleLabel("truth_W_identified_m", "reco matched particle truth W mass [GeV]", "");
    AddSingleLabel("jet_parton_ratio_pt", "particle truth jet p_{T}/parton p_{T} [-]", "");
    AddSingleLabel("jet_parton_ratio_e", "particle truth jet E/parton E [-]", "");
    AddSingleLabel("jet_parton_difference_m", "particle truth jet mass - parton mass [GeV]", "");
    AddSingleLabel("jet_parton_difference_eta", "particle truth jet #eta - parton #eta [-]", "");
    AddSingleLabel("jet_parton_difference_phi", "particle truth jet #phi - parton #phi [-]", "");
    AddSingleLabel("et_parton_W_ratio_m", "(j1 + j2 mass) / (parton1 + parton2 mass), [-]", "");
}

void Comparator::AddSingleLabel(const std::string& name,
                                const std::string& x,
                                const std::string& units) {
    Labels label;
    label.x_axis = x;
    label.units = units;

    m_labels_map[name] = label;
}
