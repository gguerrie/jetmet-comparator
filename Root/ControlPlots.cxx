#include "WmassAnalysis/ControlPlots.h"
#include "WmassAnalysis/Common.h"
#include "WmassAnalysis/Event.h"

#include "TFile.h"
#include "TLorentzVector.h"

#include <iostream>

ControlPlots::ControlPlots() :
    m_isMC(true) {
}

void ControlPlots::Init(const std::vector<std::string>& sf_syst_names) {

    m_sf_syst_names = sf_syst_names;

    for (std::size_t i = 0; i < m_sf_syst_names.size(); ++i) {
        m_jet1_pt.emplace_back("","",50, 0, 800);
        m_jet1_eta.emplace_back("","",50, 0, 2.5);
        m_jet1_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet1_jvt.emplace_back("","",50, 0, 1);
        m_jet2_pt.emplace_back("","",50, 0, 600);
        m_jet2_eta.emplace_back("","",50, 0, 2.5);
        m_jet2_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet2_jvt.emplace_back("","",50, 0, 1);
        m_jet3_pt.emplace_back("","",50, 0, 400);
        m_jet3_eta.emplace_back("","",50, 0, 2.5);
        m_jet3_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet3_jvt.emplace_back("","",50, 0, 1);
        m_jet4_pt.emplace_back("","",50, 0, 200);
        m_jet4_eta.emplace_back("","",50, 0, 2.5);
        m_jet4_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet4_jvt.emplace_back("","",50, 0, 1);
        m_jet5_pt.emplace_back("","",50, 0, 150);
        m_jet5_eta.emplace_back("","",50, 0, 2.5);
        m_jet5_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet5_jvt.emplace_back("","",50, 0, 1);
        m_jet6_pt.emplace_back("","",50, 0, 100);
        m_jet6_eta.emplace_back("","",50, 0, 2.5);
        m_jet6_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet6_jvt.emplace_back("","",50, 0, 1);
        m_bjet1_pt.emplace_back("","",50, 0, 800);
        m_bjet1_eta.emplace_back("","",50, 0, 2.5);
        m_bjet1_phi.emplace_back("","",50, -3.14, 3.14);
        m_bjet2_pt.emplace_back("","",50, 0, 400);
        m_bjet2_eta.emplace_back("","",50, 0, 2.5);
        m_bjet2_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet_pt.emplace_back("","",50, 0, 800);
        m_jet_eta.emplace_back("","",50, 0, 2.5);
        m_jet_phi.emplace_back("","",50, -3.14, 3.14);
        m_jet_pt_20_50.emplace_back("","",30, 20, 50);
        m_jet_eta_20_50.emplace_back("","",50, 0, 2.5);
        m_jet_pt_20_30.emplace_back("","",10, 20, 30);
        m_jet_eta_20_30.emplace_back("","",20, 0, 2.5);
        m_jet_pt_50.emplace_back("","",30, 50, 800);
        m_jet_eta_50.emplace_back("","",50, 0, 2.5);
        m_jet_pt_crack.emplace_back("","",20, 0, 400);
        m_jet_eta_crack.emplace_back("","",20, 1.35, 1.55);
        m_jet_phi_crack.emplace_back("","",20, -3.14, 3.14);
        m_jet_jvt_crack.emplace_back("","",50, 0, 1);
        m_HT.emplace_back("","",50, 0, 1800);
        m_jet_n.emplace_back("","",6, 3.5, 9.5);
        m_bjet_n.emplace_back("","",4, 0.5, 4.5);
        m_jet_el_dR.emplace_back("","",50, 0.0, 5.0);
        m_jet_mu_dR.emplace_back("","",50, 0.0, 5.0);
        
        m_el_pt.emplace_back("","",50, 0, 500);
        m_el_eta.emplace_back("","",50, 0, 2.5);
        m_el_phi.emplace_back("","",50, -3.14, 3.14);
        m_mu_pt.emplace_back("","",50, 0, 500);
        m_mu_eta.emplace_back("","",50, 0, 2.5);
        m_mu_phi.emplace_back("","",50, -3.14, 3.14);
        m_met.emplace_back("","",50, 0, 500);
        m_met_phi.emplace_back("","",50, -3.14, 3.14);
        m_average_mu.emplace_back("","",50, 0, 100);
        m_actual_mu.emplace_back("","",50, 0, 100);
        m_chi2.emplace_back("","",50, 0, 200);
        
        m_Wjet1_pt.emplace_back("","",50, 0, 300);
        m_Wjet1_eta.emplace_back("","",50, 0, 2.5);
        m_Wjet1_phi.emplace_back("","",50, -3.14, 3.14);
        m_Wjet2_pt.emplace_back("","",50, 0, 200);
        m_Wjet2_eta.emplace_back("","",50, 0, 2.5);
        m_Wjet2_phi.emplace_back("","",50, -3.14, 3.14);
        m_W_pt.emplace_back("","",50, 0, 600);
        m_W_eta.emplace_back("","",50, 0, 5);
        m_W_phi.emplace_back("","",50, -3.14, 3.14);
        m_W_m.emplace_back("","",100, 20, 180);
        m_W_m_peak.emplace_back("","",50, 60, 100);
        m_W_m_peak_shifted.emplace_back("","",50, 40, 90);
        m_W_m_peak_60_90.emplace_back("","",50, 60, 90);
    }
}

void ControlPlots::FillAllHistos(const Event& event,
                                 const std::size_t& index1,
                                 const std::size_t& index2,
                                 const double& chi2,
                                 const bool& isMC,
                                 const std::vector<float>& weights) {

    m_isMC = isMC;
    m_weights = weights;

    FillJetPlots(event);

    FillWbosonPlots(event, index1, index2);

    FillOtherPlots(event, chi2);
}

void ControlPlots::Finalise() {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        Common::AddOverflow(&m_jet1_pt.at(i));
        Common::AddOverflow(&m_jet1_eta.at(i));
        Common::AddOverflow(&m_jet1_phi.at(i));
        Common::AddOverflow(&m_jet1_jvt.at(i));
        Common::AddOverflow(&m_jet2_pt.at(i));
        Common::AddOverflow(&m_jet2_eta.at(i));
        Common::AddOverflow(&m_jet2_phi.at(i));
        Common::AddOverflow(&m_jet2_jvt.at(i));
        Common::AddOverflow(&m_jet3_pt.at(i));
        Common::AddOverflow(&m_jet3_eta.at(i));
        Common::AddOverflow(&m_jet3_phi.at(i));
        Common::AddOverflow(&m_jet3_jvt.at(i));
        Common::AddOverflow(&m_jet4_pt.at(i));
        Common::AddOverflow(&m_jet4_eta.at(i));
        Common::AddOverflow(&m_jet4_phi.at(i));
        Common::AddOverflow(&m_jet4_jvt.at(i));
        Common::AddOverflow(&m_jet5_pt.at(i));
        Common::AddOverflow(&m_jet5_eta.at(i));
        Common::AddOverflow(&m_jet5_phi.at(i));
        Common::AddOverflow(&m_jet5_jvt.at(i));
        Common::AddOverflow(&m_jet6_pt.at(i));
        Common::AddOverflow(&m_jet6_eta.at(i));
        Common::AddOverflow(&m_jet6_phi.at(i));
        Common::AddOverflow(&m_jet6_jvt.at(i));
        Common::AddOverflow(&m_bjet1_pt.at(i));
        Common::AddOverflow(&m_bjet1_eta.at(i));
        Common::AddOverflow(&m_bjet1_phi.at(i));
        Common::AddOverflow(&m_bjet2_pt.at(i));
        Common::AddOverflow(&m_bjet2_eta.at(i));
        Common::AddOverflow(&m_bjet2_phi.at(i));
        Common::AddOverflow(&m_jet_pt.at(i));
        Common::AddOverflow(&m_jet_eta.at(i));
        Common::AddOverflow(&m_jet_phi.at(i));
        Common::AddOverflow(&m_jet_pt_20_50.at(i));
        Common::AddOverflow(&m_jet_eta_20_50.at(i));
        Common::AddOverflow(&m_jet_pt_20_30.at(i));
        Common::AddOverflow(&m_jet_eta_20_30.at(i));
        Common::AddOverflow(&m_jet_pt_50.at(i));
        Common::AddOverflow(&m_jet_eta_50.at(i));
        Common::AddOverflow(&m_jet_pt_crack.at(i));
        Common::AddOverflow(&m_jet_eta_crack.at(i));
        Common::AddOverflow(&m_jet_phi_crack.at(i));
        Common::AddOverflow(&m_jet_jvt_crack.at(i));
        Common::AddOverflow(&m_HT.at(i));
        Common::AddOverflow(&m_jet_n.at(i));
        Common::AddOverflow(&m_bjet_n.at(i));
        Common::AddOverflow(&m_jet_el_dR.at(i));
        Common::AddOverflow(&m_jet_mu_dR.at(i));
        
        Common::AddOverflow(&m_el_pt.at(i));
        Common::AddOverflow(&m_el_eta.at(i));
        Common::AddOverflow(&m_el_phi.at(i));
        Common::AddOverflow(&m_mu_pt.at(i));
        Common::AddOverflow(&m_mu_eta.at(i));
        Common::AddOverflow(&m_mu_phi.at(i));
        Common::AddOverflow(&m_met.at(i));
        Common::AddOverflow(&m_met_phi.at(i));
        Common::AddOverflow(&m_average_mu.at(i));
        Common::AddOverflow(&m_actual_mu.at(i));
        Common::AddOverflow(&m_chi2.at(i));
        
        Common::AddOverflow(&m_Wjet1_pt.at(i));
        Common::AddOverflow(&m_Wjet1_eta.at(i));
        Common::AddOverflow(&m_Wjet1_phi.at(i));
        Common::AddOverflow(&m_Wjet2_pt.at(i));
        Common::AddOverflow(&m_Wjet2_eta.at(i));
        Common::AddOverflow(&m_Wjet2_phi.at(i));
        Common::AddOverflow(&m_W_pt.at(i));
        Common::AddOverflow(&m_W_eta.at(i));
        Common::AddOverflow(&m_W_phi.at(i));
        Common::AddOverflow(&m_W_m.at(i));
    }
}

void ControlPlots::Write(const std::string& name, TFile* file) {
    for (std::size_t i = 0; i < m_sf_syst_names.size(); ++i) {
        if (m_sf_syst_names.size() == 1) {
            file->cd(name.c_str());
        } else {
            file->cd(m_sf_syst_names.at(i).c_str());
        }
        m_jet1_pt.at(i).Write("jet1_pt");
        m_jet1_eta.at(i).Write("jet1_eta");
        m_jet1_phi.at(i).Write("jet1_phi");
        m_jet1_jvt.at(i).Write("jet1_jvt");
        m_jet2_pt.at(i).Write("jet2_pt");
        m_jet2_eta.at(i).Write("jet2_eta");
        m_jet2_phi.at(i).Write("jet2_phi");
        m_jet2_jvt.at(i).Write("jet2_jvt");
        m_jet3_pt.at(i).Write("jet3_pt");
        m_jet3_eta.at(i).Write("jet3_eta");
        m_jet3_phi.at(i).Write("jet3_phi");
        m_jet3_jvt.at(i).Write("jet3_jvt");
        m_jet4_pt.at(i).Write("jet4_pt");
        m_jet4_eta.at(i).Write("jet4_eta");
        m_jet4_phi.at(i).Write("jet4_phi");
        m_jet4_jvt.at(i).Write("jet4_jvt");
        m_jet5_pt.at(i).Write("jet5_pt");
        m_jet5_eta.at(i).Write("jet5_eta");
        m_jet5_phi.at(i).Write("jet5_phi");
        m_jet5_jvt.at(i).Write("jet5_jvt");
        m_jet6_pt.at(i).Write("jet6_pt");
        m_jet6_eta.at(i).Write("jet6_eta");
        m_jet6_phi.at(i).Write("jet6_phi");
        m_jet6_jvt.at(i).Write("jet6_jvt");
        m_bjet1_pt.at(i).Write("bjet1_pt");
        m_bjet1_eta.at(i).Write("bjet1_eta");
        m_bjet1_phi.at(i).Write("bjet1_phi");
        m_bjet2_pt.at(i).Write("bjet2_pt");
        m_bjet2_eta.at(i).Write("bjet2_eta");
        m_bjet2_phi.at(i).Write("bjet2_phi");
        m_jet_pt.at(i).Write("jet_pt");
        m_jet_eta.at(i).Write("jet_eta");
        m_jet_phi.at(i).Write("jet_phi");
        m_jet_pt_20_50.at(i).Write("jet_pt_20_50");
        m_jet_eta_20_50.at(i).Write("jet_eta_20_50");
        m_jet_pt_20_30.at(i).Write("jet_pt_20_30");
        m_jet_eta_20_30.at(i).Write("jet_eta_20_30");
        m_jet_pt_50.at(i).Write("jet_pt_50");
        m_jet_eta_50.at(i).Write("jet_eta_50");
        m_jet_pt_crack.at(i).Write("jet_pt_crack");
        m_jet_eta_crack.at(i).Write("jet_eta_crack");
        m_jet_phi_crack.at(i).Write("jet_phi_crack");
        m_jet_jvt_crack.at(i).Write("jet_jvt_crack");
        m_HT.at(i).Write("HT");
        m_jet_n.at(i).Write("jet_n");
        m_bjet_n.at(i).Write("bjet_n");
        m_jet_el_dR.at(i).Write("jet_el_dR");
        m_jet_mu_dR.at(i).Write("jet_mu_dR");
        
        m_el_pt.at(i).Write("el_pt");
        m_el_eta.at(i).Write("el_eta");
        m_el_phi.at(i).Write("el_phi");
        m_mu_pt.at(i).Write("mu_pt");
        m_mu_eta.at(i).Write("mu_eta");
        m_mu_phi.at(i).Write("mu_phi");
        m_met.at(i).Write("met");
        m_met_phi.at(i).Write("met_phi");
        m_average_mu.at(i).Write("average_mu");
        m_actual_mu.at(i).Write("actual_mu");
        m_chi2.at(i).Write("chi2");
        
        m_Wjet1_pt.at(i).Write("Wjet1_pt");
        m_Wjet1_eta.at(i).Write("Wjet1_eta");
        m_Wjet1_phi.at(i).Write("Wjet1_phi");
        m_Wjet2_pt.at(i).Write("Wjet2_pt");
        m_Wjet2_eta.at(i).Write("Wjet2_eta");
        m_Wjet2_phi.at(i).Write("Wjet2_phi");
        m_W_pt.at(i).Write("W_pt");
        m_W_eta.at(i).Write("W_eta");
        m_W_phi.at(i).Write("W_phi");
        m_W_m.at(i).Write("W_m");
        m_W_m_peak.at(i).Write("W_m_peak");
        m_W_m_peak_shifted.at(i).Write("W_m_peak_shifted");
        m_W_m_peak_60_90.at(i).Write("W_m_peak_60_90");
    }
}
    
void ControlPlots::FillJetPlots(const Event& event) {
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        m_jet1_pt.at(i).Fill(event.jet_pt->at(0)/1e3, m_weights.at(i));
        m_jet1_eta.at(i).Fill(std::fabs(event.jet_eta->at(0)), m_weights.at(i));
        m_jet1_phi.at(i).Fill(event.jet_phi->at(0), m_weights.at(i));
        m_jet1_jvt.at(i).Fill(event.jet_jvt->at(0), m_weights.at(i));
        m_jet2_pt.at(i).Fill(event.jet_pt->at(1)/1e3, m_weights.at(i));
        m_jet2_eta.at(i).Fill(std::fabs(event.jet_eta->at(1)), m_weights.at(i));
        m_jet2_phi.at(i).Fill(event.jet_phi->at(1), m_weights.at(i));
        m_jet2_jvt.at(i).Fill(event.jet_jvt->at(1), m_weights.at(i));
        m_jet3_pt.at(i).Fill(event.jet_pt->at(2)/1e3, m_weights.at(i));
        m_jet3_eta.at(i).Fill(std::fabs(event.jet_eta->at(2)), m_weights.at(i));
        m_jet3_phi.at(i).Fill(event.jet_phi->at(2), m_weights.at(i));
        m_jet3_jvt.at(i).Fill(event.jet_jvt->at(2), m_weights.at(i));
        m_jet4_pt.at(i).Fill(event.jet_pt->at(3)/1e3, m_weights.at(i));
        m_jet4_eta.at(i).Fill(std::fabs(event.jet_eta->at(3)), m_weights.at(i));
        m_jet4_phi.at(i).Fill(event.jet_phi->at(3), m_weights.at(i));
        m_jet4_jvt.at(i).Fill(event.jet_jvt->at(3), m_weights.at(i));
        if (event.jet_pt->size() > 4) {
            m_jet5_pt.at(i).Fill(event.jet_pt->at(4)/1e3, m_weights.at(i));
            m_jet5_eta.at(i).Fill(std::fabs(event.jet_eta->at(4)), m_weights.at(i));
            m_jet5_phi.at(i).Fill(event.jet_phi->at(4), m_weights.at(i));
            m_jet5_jvt.at(i).Fill(event.jet_jvt->at(4), m_weights.at(i));
        }
        if (event.jet_pt->size() > 5) {
            m_jet6_pt.at(i).Fill(event.jet_pt->at(5)/1e3, m_weights.at(i));
            m_jet6_eta.at(i).Fill(std::fabs(event.jet_eta->at(5)), m_weights.at(i));
            m_jet6_phi.at(i).Fill(event.jet_phi->at(5), m_weights.at(i));
            m_jet6_jvt.at(i).Fill(event.jet_jvt->at(5), m_weights.at(i));
        }
        m_jet_n.at(i).Fill(event.jet_pt->size(), m_weights.at(i));
        
        for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
            m_jet_pt.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
            m_jet_eta.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
            m_jet_phi.at(i).Fill(event.jet_phi->at(ijet), m_weights.at(i));
            if (event.jet_pt->at(ijet) < 50000) {
                m_jet_pt_20_50.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_jet_eta_20_50.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
            } else {
                m_jet_pt_50.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_jet_eta_50.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
            }
            if (event.jet_pt->at(ijet) < 30000) {
                m_jet_pt_20_30.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_jet_eta_20_30.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
            }
            if (std::fabs(event.jet_eta->at(ijet)) > 1.37 && std::fabs(event.jet_eta->at(ijet)) < 1.52) {
                m_jet_pt_crack.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_jet_eta_crack.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
                m_jet_phi_crack.at(i).Fill(event.jet_phi->at(ijet), m_weights.at(i));
                m_jet_jvt_crack.at(i).Fill(event.jet_jvt->at(ijet), m_weights.at(i));
            }
        }

        /// btagging
        std::size_t nbjets(0);
        for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
            if (!event.jet_isbtagged_DL1d_60->at(ijet)) continue;
            nbjets++;
            if (nbjets == 1) {
                m_bjet1_pt.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_bjet1_eta.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
                m_bjet1_phi.at(i).Fill(event.jet_phi->at(ijet), m_weights.at(i));
            }
            if (nbjets == 2) {
                m_bjet2_pt.at(i).Fill(event.jet_pt->at(ijet)/1e3, m_weights.at(i));
                m_bjet2_eta.at(i).Fill(std::fabs(event.jet_eta->at(ijet)), m_weights.at(i));
                m_bjet2_phi.at(i).Fill(event.jet_phi->at(ijet), m_weights.at(i));
            }
        }
        m_bjet_n.at(i).Fill(nbjets, m_weights.at(i));

        /// HT
        float ht(0.);
        for (const auto ipt : *event.jet_pt) {
            ht += ipt/1e3;
        }
        m_HT.at(i).Fill(ht, m_weights.at(i));
    }
}

void ControlPlots::FillWbosonPlots(const Event& event,
                                   const std::size_t& index1,
                                   const std::size_t& index2) {
    TLorentzVector j1, j2, W;
    j1.SetPtEtaPhiE(event.jet_pt->at(index1)/1e3,
                    event.jet_eta->at(index1),
                    event.jet_phi->at(index1),
                    event.jet_e->at(index1)/1e3);
    j2.SetPtEtaPhiE(event.jet_pt->at(index2)/1e3,
                    event.jet_eta->at(index2),
                    event.jet_phi->at(index2),
                    event.jet_e->at(index2)/1e3);

    W = j1 + j2;

    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        if (j1.Pt() > j2.Pt()) {
            m_Wjet1_pt.at(i).Fill(j1.Pt(), m_weights.at(i));
            m_Wjet1_eta.at(i).Fill(std::fabs(j1.Eta()), m_weights.at(i));
            m_Wjet1_phi.at(i).Fill(j1.Phi(), m_weights.at(i));
            m_Wjet2_pt.at(i).Fill(j2.Pt(), m_weights.at(i));
            m_Wjet2_eta.at(i).Fill(std::fabs(j2.Eta()), m_weights.at(i));
            m_Wjet2_phi.at(i).Fill(j2.Phi(), m_weights.at(i));
        } else {
            m_Wjet2_pt.at(i).Fill(j1.Pt(), m_weights.at(i));
            m_Wjet2_eta.at(i).Fill(std::fabs(j1.Eta()), m_weights.at(i));
            m_Wjet2_phi.at(i).Fill(j1.Phi(), m_weights.at(i));
            m_Wjet1_pt.at(i).Fill(j2.Pt(), m_weights.at(i));
            m_Wjet1_eta.at(i).Fill(std::fabs(j2.Eta()), m_weights.at(i));
            m_Wjet1_phi.at(i).Fill(j2.Phi(), m_weights.at(i));
        }

        m_W_pt.at(i).Fill(W.Pt(), m_weights.at(i));
        m_W_eta.at(i).Fill(std::fabs(W.Eta()), m_weights.at(i));
        m_W_phi.at(i).Fill(W.Phi(), m_weights.at(i));
        m_W_m.at(i).Fill(W.M(), m_weights.at(i));
        m_W_m_peak.at(i).Fill(W.M(), m_weights.at(i));
        m_W_m_peak_shifted.at(i).Fill(W.M(), m_weights.at(i));
        m_W_m_peak_60_90.at(i).Fill(W.M(), m_weights.at(i));
    }

}

void ControlPlots::FillOtherPlots(const Event& event, const double& chi2) {
    bool is_el(false);

    if (event.ejets_2015 || event.ejets_2016 || event.ejets_2017 || event.ejets_2018) is_el = true;
    else if (event.mujets_2015 || event.mujets_2016 || event.mujets_2017 || event.mujets_2018) is_el = false;
    else {
        std::cerr << "ControlPlots::FillWbosonPlots: Event is not el nor mu... skipping" << std::endl;
        return;
    }

    /// leptons
    for (std::size_t i = 0; i < m_weights.size(); ++i) {
        if (is_el) {
            m_el_pt.at(i).Fill(event.el_pt->at(0)/1e3, m_weights.at(i));
            m_el_eta.at(i).Fill(std::fabs(event.el_eta->at(0)), m_weights.at(i));
            m_el_phi.at(i).Fill(event.el_phi->at(0), m_weights.at(i));

            for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
                TLorentzVector jet, el;
                jet.SetPtEtaPhiE(event.jet_pt->at(ijet)/1e3,
                                 event.jet_eta->at(ijet),
                                 event.jet_phi->at(ijet),
                                 event.jet_e->at(ijet)/1e3);
                el.SetPtEtaPhiE(event.el_pt->at(0)/1e3,
                                event.el_eta->at(0),
                                event.el_phi->at(0),
                                event.el_e->at(0)/1e3);

                const double dR = jet.DeltaR(el);
                m_jet_el_dR.at(i).Fill(dR, m_weights.at(i));
            }
        } else {
            m_mu_pt.at(i).Fill(event.mu_pt->at(0)/1e3, m_weights.at(i));
            m_mu_eta.at(i).Fill(std::fabs(event.mu_eta->at(0)), m_weights.at(i));
            m_mu_phi.at(i).Fill(event.mu_phi->at(0), m_weights.at(i));
            
            for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
                TLorentzVector jet, mu;
                jet.SetPtEtaPhiE(event.jet_pt->at(ijet)/1e3,
                                 event.jet_eta->at(ijet),
                                 event.jet_phi->at(ijet),
                                 event.jet_e->at(ijet)/1e3);
                mu.SetPtEtaPhiE(event.mu_pt->at(0)/1e3,
                                event.mu_eta->at(0),
                                event.mu_phi->at(0),
                                event.mu_e->at(0)/1e3);

                const double dR = jet.DeltaR(mu);
                m_jet_mu_dR.at(i).Fill(dR, m_weights.at(i));
            }
        } 
    
        /// MET
        m_met.at(i).Fill(event.met_met/1e3, m_weights.at(i));
        m_met_phi.at(i).Fill(event.met_phi, m_weights.at(i));

        /// average/actual mu
        if (m_isMC) {
            m_average_mu.at(i).Fill(event.mu, m_weights.at(i));
            m_actual_mu.at(i).Fill(event.mu_actual, m_weights.at(i));
        } else {
            m_average_mu.at(i).Fill(event.mu/1.03, m_weights.at(i));
            m_actual_mu.at(i).Fill(event.mu_actual/1.03, m_weights.at(i));
        }

        /// chi2
        m_chi2.at(i).Fill(chi2, m_weights.at(i));
    }
}
