#include "WmassAnalysis/Extractor.h"
#include "WmassAnalysis/Common.h"
#include "AtlasUtils/AtlasStyle.h"

#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLatex.h"
#include "TPad.h"
#include "TSystem.h"

#include <algorithm>
#include <iostream>

Extractor::Extractor(const std::string& name) :
    m_name(name),
    m_fit_data(false),
    m_use_syst(false),
    m_PE_number(100),
    m_ttbar_name(""),
    m_neighbour_indices(1),
    m_obs_name("W_m"),
    m_fix_s_index(-1),
    m_fix_r_index(-1),
    m_ttbar_file(nullptr),
    m_data_file(nullptr) {

    SetAtlasStyle();
}

Extractor::~Extractor() {
}

void Extractor::SetTtbarName(const std::string& name) {
    m_ttbar_name = name;
}

void Extractor::SetBackgroundNames(const std::vector<std::string>& names) {
    m_background_names = names;
}

void Extractor::SetSpecialNames(const std::vector<std::string>& names) {
    m_special_names = names;
}

void Extractor::SetParameterS(const int& steps,
                              const float& min,
                              const float& max) {
    m_param_s.steps = steps;
    m_param_s.min = min;
    m_param_s.max = max;
}

void Extractor::SetParameterR(const int& steps,
                              const float& min,
                              const float& max) {
    m_param_r.steps = steps;
    m_param_r.min = min;
    m_param_r.max = max;
}

void Extractor::SetFitData(const bool& flag) {
    m_fit_data = flag;
}

void Extractor::SetUseSyst(const bool& flag) {
    m_use_syst = flag;
}

void Extractor::SetNumberOfPE(const int& n) {
    m_PE_number = n;
}

void Extractor::SetAllSysts(const std::vector<Systematic>& systs) {
    m_systematics = systs;
}

void Extractor::SetNeighbourIndices(const int& n) {
    m_neighbour_indices = n;
}

void Extractor::SetObsName(const std::string& name) {
    m_obs_name = name;
}
    
void Extractor::SetFixIndexS(const int& index) {
    m_fix_s_index = index;
}

void Extractor::SetFixIndexR(const int& index) {
    m_fix_r_index = index;
}

void Extractor::ExtractJES() {

    OpenRootFiles();

    FFFitter fitter{};
    fitter.SetNormalise(true);
    fitter.SetSubtractMinChi2(true);
    fitter.SetParameterS(m_param_s.steps, m_param_s.min, m_param_s.max);
    fitter.SetParameterR(m_param_r.steps, m_param_r.min, m_param_r.max);
    fitter.SetNneighbourIndices(m_neighbour_indices);
    fitter.FixParamS(m_fix_s_index);
    fitter.FixParamR(m_fix_r_index);

    const std::vector<std::vector<TH1D> > templates = GetNominalTemplates(fitter);

    /// Asimov fit also calculates all systematics 
    AsimovFit(fitter, templates);

    /// Run only fit to data
    if (m_fit_data) {
        DataFit(fitter, templates);
    }

    CloseRootFiles();
}

void Extractor::AsimovFit(FFFitter& fitter, const std::vector<std::vector<TH1D> >& histos) {

    std::unique_ptr<TH1D> asimov(static_cast<TH1D*>(m_ttbar_file->Get(("nominal/"+m_obs_name).c_str())));
    if (!asimov) {
        std::cerr << "Extractor::AsimovFit: Cannot read 'data' histogram" << std::endl;
        exit(EXIT_FAILURE);
    }
    fitter.SetChi2Option("UWCHI2");
    fitter.FindBestTemplate(histos, *asimov);
    bool is_ok(true);
    double best_s = fitter.GetBestValueS(&is_ok);
    if (!is_ok) {
        std::cout << "Extractor::AsimovFit: nominal s fit failed" << std::endl;
    }
    double best_r = fitter.GetBestValueR(&is_ok);
    if (!is_ok) {
        std::cout << "Extractor::AsimovFit: nominal r fit failed" << std::endl;
    }
    const double best_s_unc = fitter.GetParamSuncertaintyChi2(&is_ok);
    if (!is_ok) {
        std::cout << "Extractor::AsimovFit: unc s fit failed" << std::endl;
    }
    const double best_r_unc = fitter.GetParamRuncertaintyChi2(&is_ok);
    if (!is_ok) {
        std::cout << "Extractor::AsimovFit: unc s fit failed" << std::endl;
    }

    best_s = 1;
    best_r = 1;

    *m_result_file_s << "Asimov\n";
    *m_result_file_s << "S: " << best_s << " +- " << best_s_unc << "\n";
    *m_result_file_r << "Asimov\n";
    *m_result_file_r << "R: " << best_r << " +- " << best_r_unc << "\n";

    /// Make plot of the chi2
    std::unique_ptr<TH2D> curve = fitter.Create2Dchi2Histo();

    Make2DPlot(curve.get(), true);

    /// Run pseudoexperiments for stat uncertainty
    fitter.RunPseudoexperiments(histos, *asimov, m_PE_number);
    const std::vector<double> s_PE = fitter.GetPEvaluesS();
    const std::vector<double> r_PE = fitter.GetPEvaluesR();

    double s_error(0);
    double r_error(0);
    const double s_PE_sigma = Common::GetSigma(s_PE);
    const double r_PE_sigma = Common::GetSigma(r_PE);
    const double s_PE_sigma_gaus = Common::GetSigmaGaus(s_PE, 0.95, 1.05, s_error);
    const double r_PE_sigma_gaus = Common::GetSigmaGaus(r_PE, 0.95, 1.05, r_error);

    std::cout << "\n****************************************************************\n";
    std::cout << "********************* R E S U L T ASIMOV ***********************\n";
    std::cout << "****************************************************************\n\n";
    std::cout << "S = " << best_s << " +- " << best_s_unc << " (chi2), " << s_PE_sigma << " (PE sigma), " << s_PE_sigma_gaus << " +- " << s_error << " (PE Gaus fit)" << "\n";
    std::cout << "R = " << best_r << " +- " << best_r_unc << " (chi2), " << r_PE_sigma << " (PE sigma), " << r_PE_sigma_gaus << " +- " << r_error << " (PE Gaus fit)" << "\n";
    std::cout << "\n****************************************************************\n\n";

    if (!m_use_syst) return;

    /// Calculate syst uncertainty
    fitter.SetChi2Option("WWCHI2");
    std::cout << "Extractor::AsimovFit: Will process systematics\n\n";
    /// Loop over unique systematics
    const std::vector<std::string>& unique_systs = Common::GetSystUniqueNames(m_systematics);
    for (const auto& isyst : unique_systs) {
        std::cout << "Extractor::AsimovFit: Processing systematic: " << isyst << "\n";
        ProcessSingleSystematic(fitter, histos, isyst, best_s, best_r);
    }
    std::cout << "\nExtractor::AsimovFit: Finished processing systematics \n\n";

    /// calculate total unc
    *m_result_file_s << "\nSystematics\n";
    for (auto itr_s : m_all_syst_map_s) {
        std::cout << "Param s, systematic: " << itr_s.first << " unc up: " << itr_s.second.first << ", down: " << itr_s.second.second << "\n";
        *m_result_file_s << "Systematic: " << itr_s.first << ", up: " << itr_s.second.first << ", down: " << itr_s.second.second << "\n";
    }

    std::cout << "\n";
    *m_result_file_r << "\nSystematics\n";
    for (auto itr_r : m_all_syst_map_r) {
        std::cout << "Param r, systematic: " << itr_r.first << " unc up: " << itr_r.second.first << ", down: " << itr_r.second.second << "\n";
        *m_result_file_r << "Systematic: " << itr_r.first << ", up: " << itr_r.second.first << ", down: " << itr_r.second.second << "\n";
    }

    std::cout << "\n";
    *m_result_file_s << "\nCategories\n";
    for (auto itr_s : m_category_syst_map_s) {
        std::cout << "Param s, category: " << itr_s.first << ", up: " << itr_s.second.first << ", down: " << itr_s.second.second << "\n";
        *m_result_file_s << "Category: " << itr_s.first << ", up: " << itr_s.second.first << ", down: " << itr_s.second.second << "\n";
    }

    std::cout << "\n";
    *m_result_file_r << "\nCategories\n";
    for (auto itr_r : m_category_syst_map_r) {
        std::cout << "Param r, category: " << itr_r.first << ", up: " << itr_r.second.first << ", down: " << itr_r.second.second << "\n";
        *m_result_file_r << "Category: " << itr_r.first << ", up: " << itr_r.second.first << ", down: " << itr_r.second.second << "\n";
    }

    const double s_total_up   = Common::SumCategories(m_category_syst_map_s, true);
    const double s_total_down = Common::SumCategories(m_category_syst_map_s, false);
    const double r_total_up   = Common::SumCategories(m_category_syst_map_r, true);
    const double r_total_down = Common::SumCategories(m_category_syst_map_r, false);

    *m_result_file_s << "\nSyst up: " << s_total_up << ", down: " << -s_total_down << "\n";
    *m_result_file_r << "\nSyst up: " << r_total_up << ", down: " << -r_total_down << "\n";
    *m_result_file_s << "Total up: " << std::hypot(s_total_up, best_s_unc) << ", down: " << -std::hypot(s_total_down, best_s_unc) << "\n";
    *m_result_file_r << "Total up: " << std::hypot(r_total_up, best_r_unc) << ", down: " << -std::hypot(r_total_down, best_r_unc) << "\n";
    std::cout << "\nTotal s up: " << s_total_up << "\n";
    std::cout << "Total s down: " << -s_total_down << "\n";
    std::cout << "Total r up: " << r_total_up << "\n";
    std::cout << "Total r down: " << -r_total_down << "\n";
}

void Extractor::ProcessSingleSystematic(FFFitter& fitter,
                                        const std::vector<std::vector<TH1D> >& histos,
                                        const std::string& name,
                                        const double& mean_s,
                                        const double& mean_r) {

    const std::vector<Systematic>& systs = Common::GetSystSameName(m_systematics, name);
    /// Now each systematic should impact only one sample!
    if (!Common::SystematicsAreConsistent(systs)) {
        std::cerr << "Extractor::ProcessSingleSystematic: There is something really weird happening, investigate!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (systs.size() == 0) return;

    bool is_ok(true);

    const std::string& categoryName = systs.at(0).category;

    if (systs.at(0).type == SYSTEMATICTYPE::ONESIDED) {
        /// Run only one fit
        std::unique_ptr<TH1D> up = GetSystShiftedHist(systs, true, false);

        fitter.FindBestTemplate(histos, *up);

        const double unc_s = fitter.GetBestValueS(&is_ok) - mean_s;
        if (!is_ok) {
            std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the s fit\n" << std::endl;
        }
        const double unc_r = fitter.GetBestValueR(&is_ok) - mean_r;
        if (!is_ok) {
            std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the r fit\n" << std::endl;
        }

        // "normal" systematics
        if (systs.at(0).target == systs.at(0).reference_file) {
            m_all_syst_map_s[name] = std::make_pair(unc_s, -unc_s);
            m_all_syst_map_r[name] = std::make_pair(unc_r, -unc_r);

            Common::AddMapUncertainty(m_category_syst_map_s, unc_s, -unc_s, categoryName);
            Common::AddMapUncertainty(m_category_syst_map_r, unc_r, -unc_r, categoryName);

        } else { // special cases
            std::unique_ptr<TH1D> special = GetSystShiftedHist(systs, true, true);
            fitter.FindBestTemplate(histos, *special);

            const double ref_s = fitter.GetBestValueS(&is_ok) - mean_s;
            if (!is_ok) {
                std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the reference s fit\n" << std::endl;
            }
            const double ref_r = fitter.GetBestValueR(&is_ok) - mean_r;
            if (!is_ok) {
                std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the reference r fit\n" << std::endl;
            }

            m_all_syst_map_s[name] = std::make_pair(unc_s - ref_s, -(unc_s - ref_s));
            m_all_syst_map_r[name] = std::make_pair(unc_r - ref_r, -(unc_r - ref_r));
            
            Common::AddMapUncertainty(m_category_syst_map_s, unc_s - ref_s, -(unc_s - ref_s), categoryName);
            Common::AddMapUncertainty(m_category_syst_map_r, unc_r - ref_r, -(unc_r - ref_r), categoryName);
        }

    } else if (systs.at(0).type == SYSTEMATICTYPE::TWOSIDED || systs.at(0).type == SYSTEMATICTYPE::NORMALISATION) {
        /// Run two fits
        std::unique_ptr<TH1D> up = GetSystShiftedHist(systs, true, false);
        std::unique_ptr<TH1D> down = GetSystShiftedHist(systs, false, false);

        fitter.FindBestTemplate(histos, *up);
        const double unc_s_up = fitter.GetBestValueS(&is_ok) - mean_s;
        if (!is_ok) {
            std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the s up fit\n" << std::endl;
        }
        const double unc_r_up = fitter.GetBestValueR(&is_ok) - mean_r;
        if (!is_ok) {
            std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the r up fit\n" << std::endl;
        }

        fitter.FindBestTemplate(histos, *down);
        const double unc_s_down = fitter.GetBestValueS(&is_ok) - mean_s;
        if (!is_ok) {
            std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the s down fit\n" << std::endl;
        }
        const double unc_r_down = fitter.GetBestValueR(&is_ok) - mean_r;
        if (!is_ok) {
            std::cerr << "Extractor::ProcessSingleSystematic: systematic: " << name << " failed the r down fit\n" << std::endl;
        }

        m_all_syst_map_s[name] = std::make_pair(unc_s_up, unc_s_down);
        m_all_syst_map_r[name] = std::make_pair(unc_r_up, unc_r_down);
            
        Common::AddMapUncertainty(m_category_syst_map_s, unc_s_up, unc_s_down, categoryName);
        Common::AddMapUncertainty(m_category_syst_map_r, unc_r_up, unc_r_down, categoryName);

    } else {
        std::cerr << "Extractor::ProcessSingleSystematic: Unknown systematic type!" << std::endl;
        exit(EXIT_FAILURE);
    }
}

std::unique_ptr<TH1D> Extractor::GetSystShiftedHist(const std::vector<Systematic>& syst,
                                                    const bool& is_up,
                                                    const bool& use_ref) const {
    std::unique_ptr<TH1D> result(nullptr);

    /// Process ttbar
    int index = Common::GetSystematicIndex(syst, m_ttbar_name);
    const bool is_special = (syst.at(0).target != syst.at(0).up_file) && ((syst.at(0).target != syst.at(0).down_file));
    std::size_t special_index(0);
    if (use_ref || is_special) {
        if (syst.size() != 1) {
            std::cerr << "Extractor::GetSystShiftedHist: Reference systematic is used but there are more than one... This should not happen" << std::endl;
            exit(EXIT_FAILURE);
        }

        index = -1;

        auto itr = std::find(m_special_names.begin(), m_special_names.end(), syst.at(0).up_file);
        if (itr == m_special_names.end()) {
            std::cerr << "Extractor::GetSystShiftedHist: Cannot find special syst in the list of special systs: " << syst.at(0).up_file << std::endl;
            exit(EXIT_FAILURE);    
        }

        if (use_ref) {
            itr= std::find(m_special_names.begin(), m_special_names.end(), syst.at(0).reference_file);
            if (itr == m_special_names.end()) {
                std::cerr << "Extractor::GetSystShiftedHist: Cannot find reference in the list of special systs: " << syst.at(0).reference_file << std::endl;
                exit(EXIT_FAILURE);    
            }
        } 

        special_index = std::distance(m_special_names.begin(), itr);
    }

    if (index < 0) { // doesnt exist
        const std::string path_sig = use_ref ? syst.at(0).reference_histo : (is_up ? syst.at(0).up_histo : syst.at(0).down_histo);
        if (is_special && (syst.at(0).target == m_ttbar_name)) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_special_files.at(special_index)->Get((path_sig+"/"+m_obs_name).c_str())));
        } else if (syst.at(0).target == m_ttbar_name) {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_file->Get((path_sig+"/"+m_obs_name).c_str())));
        } else {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_file->Get(("nominal/"+m_obs_name).c_str())));
        }
    } else {
        const std::string path_sig = is_up ? syst.at(index).up_histo : syst.at(index).down_histo;

        if (use_ref) {
            if (syst.at(0).target == m_ttbar_name) {
                result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_special_files.at(special_index)->Get((path_sig+"/"+m_obs_name).c_str())));
            } else {
                result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_file->Get((path_sig+"/"+m_obs_name).c_str())));
            }
        } else {
            result = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_ttbar_file->Get((path_sig+"/"+m_obs_name).c_str())));
        }

        if (!result) {
            std::cerr << "Extractor::GetSystShiftedHist: Cannot read ttbar file!" << std::endl;
            exit(EXIT_FAILURE);    
        }

        if (syst.at(index).type == SYSTEMATICTYPE::NORMALISATION) {
           if (is_up) {
               result->Scale(1+syst.at(index).norm_up);
           } else {
               result->Scale(1-syst.at(index).norm_down);
           }
        }
    }

    /// process bkgs
    for (std::size_t ibkg = 0; ibkg < m_background_names.size(); ++ibkg) {
        index = Common::GetSystematicIndex(syst, m_background_names.at(ibkg));
        if (index < 0) {
            std::unique_ptr<TH1D> tmp(nullptr);
            const std::string path_sig = is_up ? syst.at(0).up_histo : syst.at(0).down_histo;
            if (is_special && (syst.at(0).target == m_background_names.at(ibkg))) {
                tmp = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_special_files.at(special_index)->Get((path_sig+"/"+m_obs_name).c_str())));
            } else if (syst.at(0).target == m_background_names.at(ibkg)) {
                tmp = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files.at(ibkg)->Get((path_sig+"/"+m_obs_name).c_str())));
            } else {
                tmp = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files.at(ibkg)->Get(("nominal/"+m_obs_name).c_str())));
            }
            if (!tmp) {
                std::cerr << "Extractor::GetSystShiftedHist: Cannot find histogram: nominal/" << m_obs_name << " for bkg: " << m_background_names.at(ibkg) << std::endl;
                exit(EXIT_FAILURE);
            }
            result->Add(tmp.get());
        } else {
            const std::string path_bkg = is_up ? syst.at(index).up_histo : syst.at(index).down_histo;
            std::unique_ptr<TH1D> tmp(nullptr);
            if (use_ref) {
                if (syst.at(0).target == m_background_names.at(ibkg)) {
                    tmp = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_special_files.at(special_index)->Get((path_bkg+"/"+m_obs_name).c_str())));
                } else {
                    tmp = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files.at(ibkg)->Get((path_bkg+"/"+m_obs_name).c_str())));
                }
            } else {
                tmp = std::unique_ptr<TH1D>(static_cast<TH1D*>(m_background_files.at(ibkg)->Get((path_bkg+"/"+m_obs_name).c_str())));
            }
            if (!tmp) {
                std::cerr << "Extractor::GetSystShiftedHist: Cannot find histogram: " << path_bkg+"/"+m_obs_name << " for bkg: " << m_background_names.at(ibkg) << std::endl;
                exit(EXIT_FAILURE);
            }
            if (syst.at(index).type == SYSTEMATICTYPE::NORMALISATION) {
               if (is_up) {
                   tmp->Scale(1+syst.at(index).norm_up);
               } else {
                   tmp->Scale(1-syst.at(index).norm_down);
               }
            }
            result->Add(tmp.get());
        }
    }

    return result;
}

void Extractor::DataFit(FFFitter& fitter, const std::vector<std::vector<TH1D> >& histos) {
    
    std::unique_ptr<TH1D> data(static_cast<TH1D*>(m_data_file->Get(("nominal/"+m_obs_name).c_str())));
    if (!data) {
        std::cerr << "Extractor::DataFit: Cannot read data histogram" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    fitter.SetChi2Option("UWCHI2");
    fitter.FindBestTemplate(histos, *data);
    const std::vector<double> tmp = fitter.GetBestProfileS();
    const std::vector<double> tmp1 = fitter.GetBestProfileR();
    
    /// Make plot of the chi2
    std::unique_ptr<TH2D> curve = fitter.Create2Dchi2Histo();

    Make2DPlot(curve.get(), false);

    bool is_ok(true);

    std::cout << "Fit result data: s = " << fitter.GetBestValueS(&is_ok) << " +- " << fitter.GetParamSuncertaintyChi2(&is_ok) << "\n";
    std::cout << "Fit result data: r = " << fitter.GetBestValueR(&is_ok) << " +- " << fitter.GetParamRuncertaintyChi2(&is_ok) << "\n";

    *m_result_file_s << "\nData\n";
    *m_result_file_r << "\nData\n";
    *m_result_file_s << "Result: " << fitter.GetBestValueS(&is_ok) << " +- " << fitter.GetParamSuncertaintyChi2(&is_ok);
    *m_result_file_r << "Result: " << fitter.GetBestValueR(&is_ok) << " +- " << fitter.GetParamRuncertaintyChi2(&is_ok);
}

std::vector<std::vector<TH1D> > Extractor::GetNominalTemplates(const FFFitter& fitter) const {
    std::vector<std::vector<TH1D> > result;
    /// Add the smeared histos
    for (int is = 0; is < fitter.GetSsteps(); ++is) {
        std::vector<TH1D> tmp_vec;
        for (int ir = 0; ir < fitter.GetRsteps(); ++ir) {
            const std::string histo_name = "nominal/param_s_"+std::to_string(is)+"/param_r_"+std::to_string(ir);
            std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(m_ttbar_file->Get((histo_name+"/"+m_obs_name).c_str())));
            if (!tmp) {
                std::cerr << "Extractor::GetNominalTemplates: Cannot read histogram: " << histo_name+"/"+m_obs_name << std::endl;
                exit(EXIT_FAILURE);
            }
            tmp_vec.emplace_back(std::move(*tmp));
        }
        result.emplace_back(tmp_vec);
    }
    
    /// Add ttbar non-smeared
    std::unique_ptr<TH1D> bkg(static_cast<TH1D*>(m_ttbar_file->Get(("nominal/"+m_obs_name+"_impossible").c_str())));
    if (!bkg) {
        std::cerr << "Extractor::GetNominalTemplates: Cannot read histogram: nominal/"+m_obs_name+"_impossible" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Add backgrounds
    for (auto& ifile : m_background_files) {
        std::unique_ptr<TH1D> tmp(static_cast<TH1D*>(ifile->Get(("nominal/"+m_obs_name).c_str())));
        if (!tmp) {
            std::cerr << "Extractor::GetNominalTemplates: Cannot read histogram: nominal/"+m_obs_name << std::endl;
            exit(EXIT_FAILURE);
        }
        bkg->Add(tmp.get());
    }
    
    /// Combine smeared signal + bkg
    for (auto& i : result) {
        for (auto& j : i) {
            j.Add(bkg.get());
        }
    }

    return result;
}

void Extractor::OpenRootFiles() {
    const std::string path = "../OutputHistos/"+m_name+"/";
    m_ttbar_file = std::unique_ptr<TFile>(TFile::Open((path+m_ttbar_name+"_"+m_name+".root").c_str()));

    if (!m_ttbar_file) {
        std::cerr << "Extractor::OpenRootFiles: Cannot open ttbar file" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    m_data_file = std::unique_ptr<TFile>(TFile::Open((path+"Data_"+m_name+".root").c_str()));
    if (!m_data_file) {
        std::cerr << "Extractor::OpenRootFiles: Cannot open data file" << std::endl;
        exit(EXIT_FAILURE);
    }

    for (const auto& ibkg : m_background_names) {
        std::unique_ptr<TFile> tmp(TFile::Open((path+ibkg+"_"+m_name+".root").c_str()));
        if (!tmp) {
            std::cerr << "Extractor::OpenRootFiles: Cannot open " << ibkg << " file" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_background_files.emplace_back(std::move(tmp));
    }
    
    for (const auto& ispecial : m_special_names) {
        std::unique_ptr<TFile> tmp(TFile::Open((path+ispecial+"_"+m_name+".root").c_str()));
        if (!tmp) {
            std::cerr << "Extractor::OpenRootFiles: Cannot open " << ispecial << " file" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_special_files.emplace_back(std::move(tmp));
    }

    /// open output text files with results
    m_result_file_s = std::unique_ptr<std::ofstream>(new std::ofstream());
    m_result_file_r = std::unique_ptr<std::ofstream>(new std::ofstream());
    
    gSystem->mkdir(("../FitPlots/"+m_name).c_str());

    const std::string fixed_s = m_fix_s_index > -1 ? "_fixed_s" : "";
    const std::string fixed_r = m_fix_r_index > -1 ? "_fixed_r" : "";

    m_result_file_s->open(("../FitPlots/"+m_name+"/Result_parameter_s"+fixed_s+fixed_r+".txt").c_str(), std::ios::trunc);
    m_result_file_r->open(("../FitPlots/"+m_name+"/Result_parameter_r"+fixed_s+fixed_r+".txt").c_str(), std::ios::trunc);

    if (!m_result_file_s->is_open() || !m_result_file_s->good()) {
        std::cerr << "Extractor::OpenRootFiles: Cannot open results text file for parameter s" << std::endl;
       exit(EXIT_FAILURE); 
    }
    if (!m_result_file_r->is_open() || !m_result_file_r->good()) {
        std::cerr << "Extractor::OpenRootFiles: Cannot open results text file for parameter r" << std::endl;
       exit(EXIT_FAILURE); 
    }
}

void Extractor::CloseRootFiles() {
    m_ttbar_file->Close();
    m_data_file->Close();

    for (auto& ibkg : m_background_files) {
        ibkg->Close();
    }
    
    for (auto& ispecial : m_special_files) {
        ispecial->Close();
    }

    m_result_file_s->close();
    m_result_file_r->close();
}

void Extractor::Make2DPlot(TH2D* curve, const bool& is_asimov) const {


    TCanvas c("","",800,600);
    c.cd();
    
    TPad pad("","",0,0.0,1,1);
    pad.SetRightMargin(0.2);
    pad.SetLeftMargin(0.13);
    pad.SetTopMargin(0.09);
    pad.SetBottomMargin(0.13);
    pad.Draw();
    pad.cd();

    curve->GetXaxis()->SetTitleOffset(1.1);
    curve->GetXaxis()->SetTitleSize(1.2*curve->GetXaxis()->GetTitleSize());
    curve->GetYaxis()->SetTitleOffset(1.3);
    curve->GetYaxis()->SetTitleSize(1.2*curve->GetYaxis()->GetTitleSize());
    curve->GetZaxis()->SetTitleSize(1.2*curve->GetZaxis()->GetTitleSize());
    curve->GetZaxis()->SetTitleOffset(1.2);
    
    curve->Draw("COLZ");
    
    // set labels
    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.04);
    l1.SetNDC();
    l1.DrawLatex(0.15, 0.92, "ATLAS");
    
    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.04);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(0.25, 0.92, "Internal");
    
    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.04);
    l3.SetTextFont(42);
    l3.SetNDC();
    l3.DrawLatex(0.37, 0.92, "#sqrt{s} = 13 TeV");	
    
    const std::string fixed_s = m_fix_s_index > -1 ? "_fixed_s" : "";
    const std::string fixed_r = m_fix_r_index > -1 ? "_fixed_r" : "";

    if (is_asimov) {
        c.Print(("../FitPlots/"+m_name+"/Asimov_2D_curve"+fixed_s+fixed_r+".png").c_str());
        c.Print(("../FitPlots/"+m_name+"/Asimov_2D_curve"+fixed_s+fixed_r+".eps").c_str());
        c.Print(("../FitPlots/"+m_name+"/Asimov_2D_curve"+fixed_s+fixed_r+".pdf").c_str());
    } else {
        c.Print(("../FitPlots/"+m_name+"/Data_2D_curve"+fixed_s+fixed_r+".png").c_str());
        c.Print(("../FitPlots/"+m_name+"/Data_2D_curve"+fixed_s+fixed_r+".eps").c_str());
        c.Print(("../FitPlots/"+m_name+"/Data_2D_curve"+fixed_s+fixed_r+".pdf").c_str());
    }
}
