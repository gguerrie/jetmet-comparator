#include "WmassAnalysis/FileProcessor.h"
#include "WmassAnalysis/HistoMaker.h"
#include "WmassAnalysis/WeightManager.h"
#include "Xsection/SampleXsection.h"

#include "ForwardFolding/ForwardFolding/Folder.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TKey.h"
#include "TSystem.h"

#include <algorithm>
#include <fstream>
#include <iostream>

FileProcessor::FileProcessor(const std::string& name,
                             const std::string& dataset_type,
                             const std::string& systematics) :
    m_name(name),
    m_dataset_type(dataset_type),
    m_systematics(systematics),
    m_leading_pt_min(0),
    m_leading_pt_max(999999),
    m_subleading_pt_min(0),
    m_subleading_pt_max(999999),
    m_leading_eta_min(-1),
    m_leading_eta_max(999999),
    m_subleading_eta_min(-1),
    m_subleading_eta_max(999999),
    m_average_pt_min(-1),
    m_average_pt_max(999999),
    m_npv_min(-1),
    m_npv_max(10000),
    m_mu_min(-1),
    m_mu_max(10000),
    m_runNumber_min(0),
    m_runNumber_max(1000000),
    m_createHerwigCorrection(false),
    m_critical_reco_dR(0.5),
    m_critical_truth_dR(0.5) {

    m_eta_bins = {0.0,1.0,2.5};
    m_pt_bins  = {0.0,30.0,55.0,110.0,200.0,400.0,800.0,1500.0};
}

void FileProcessor::ReadConfig() {
   
    /// set names of processes 
    m_process.emplace_back("ttbar_PP8_FS");
    m_process.emplace_back("ttbar_PP8_AFII");
    m_process.emplace_back("ttbar_aMCNLO_Pythia8_AFII");
    m_process.emplace_back("ttbar_aMCNLO_Herwig7_1_3_AFII");
    m_process.emplace_back("ttbar_PH7_1_3_AFII");
    m_process.emplace_back("ttbar_PP8_mass_172_AFII");
    m_process.emplace_back("ttbar_PP8_mass_173_AFII");
    m_process.emplace_back("ttbar_PP8_hdamp_AFII");
    m_process.emplace_back("ttbar_PP8_shower_decor_AFII");
    m_process.emplace_back("Wjets_Sherpa_FS");
    m_process.emplace_back("Zjets_Sherpa_FS");
    m_process.emplace_back("SingleTop_PP8_s_chan_FS");
    m_process.emplace_back("SingleTop_PP8_t_chan_FS");
    m_process.emplace_back("SingleTop_PP8_tW_chan_FS");
    m_process.emplace_back("SingleTop_DS_PP8_tW_chan_FS");
    m_process.emplace_back("SingleTop_PP8_s_chan_AFII");
    m_process.emplace_back("SingleTop_PP8_t_chan_AFII");
    m_process.emplace_back("SingleTop_PP8_tW_chan_AFII");
    m_process.emplace_back("SingleTop_PH7_0_4_s_chan_AFII");
    m_process.emplace_back("SingleTop_PH7_0_4_t_chan_AFII");
    m_process.emplace_back("SingleTop_PH7_0_4_tW_chan_AFII");
    m_process.emplace_back("SingleTop_aMCNLO_Pythia8_s_chan_AFII");
    m_process.emplace_back("SingleTop_aMCNLO_Pythia8_t_chan_AFII");
    m_process.emplace_back("SingleTop_aMCNLO_Pythia8_tW_chan_AFII");
    m_process.emplace_back("Diboson_Sherpa_FS");
    m_process.emplace_back("ttV_aMCNLO_Pythia8_FS");
    m_process.emplace_back("ttH_PP8_FS");
    m_process.emplace_back("Multijet");
    m_process.emplace_back("Data");

    if (m_dataset_type != "full") {
        if (std::find(m_process.begin(), m_process.end(), m_dataset_type) != m_process.end()) {
            m_process.clear();
            m_process.emplace_back(m_dataset_type);
            if (m_dataset_type == "Data") m_process.emplace_back("Multijet");
        } else {
            std::cerr << "FileProcessor::ReadConfig: Unknown process type: " << m_dataset_type << std::endl;
            exit(EXIT_FAILURE);
        }
    }
}

void FileProcessor::SetCriticalRecoDR(const float& dR) {
    m_critical_reco_dR = dR;
}

void FileProcessor::SetCriticalTruthDR(const float& dR) {
    m_critical_truth_dR = dR;
}

void FileProcessor::SetLeadingWjetCuts(const float& min, const float& max) {
    m_leading_pt_min = min;
    m_leading_pt_max = max;
}

void FileProcessor::SetSubLeadingWjetCuts(const float& min, const float& max) {
    m_subleading_pt_min = min;
    m_subleading_pt_max = max;
}

void FileProcessor::SetLeadingWjetEtaCuts(const float& min, const float& max) {
    m_leading_eta_min = min;
    m_leading_eta_max = max;
}

void FileProcessor::SetSubLeadingWjetEtaCuts(const float& min, const float& max) {
    m_subleading_eta_min = min;
    m_subleading_eta_max = max;
}
    
void FileProcessor::SetAverageWjetCuts(const float& min, const float& max) {
    m_average_pt_min = min;
    m_average_pt_max = max;
}

void FileProcessor::SetNpvCuts(const int& min, const int& max) {
    m_npv_min = min;
    m_npv_max = max;
}

void FileProcessor::SetMuCuts(const float& min, const float& max) {
    m_mu_min = min;
    m_mu_max = max;
}

void FileProcessor::SetRunNumberCuts(const unsigned int& min, const unsigned int& max) {
    m_runNumber_min = min;
    m_runNumber_max = max;
}

void FileProcessor::SetCreateHerwigCorrection(const bool& flag) {
    m_createHerwigCorrection = flag;
}

void FileProcessor::ProcessAllFiles(const Chi2Params& par, const double& chi2_cut) {

    /// Read File list
    ReadFileList("scripts/file_list.txt");

    /// Read list of systematics
    if (m_systematics == "nominal") {
        m_syst_list.emplace_back("nominal");
    }
    if (m_systematics == "syst") {
        m_syst_list = ReadSystList();
    }

    if (m_systematics == "sfsyst") {
        m_syst_list.emplace_back("sfsyst");
    }

    /// Create empty output files
    static const std::string out_path = "../OutputHistos";
    if (m_systematics == "nominal") {
        std::cout << "FileProcessor::ProcessAllFiles: Creating empty output files\n";
        CreateOutputFiles(out_path);
    }

    /// Get the cross-section tool
    SampleXsection xSecTool{};
    if (!xSecTool.readFromFile("data/XSection-MC15-13TeV.data")) {
        std::cerr << "FileProcessor::ProcessAllFiles: Cannot open data file with cross-sections" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Get the WeightManager tool
    WeightManager weight_manager{};

    if (!weight_manager.ReadFile("scripts/sumW.txt")) {
        std::cerr << "FileProcessor::ProcessAllFiles: Cannot read the file with sum weights" << std::endl;
        exit(EXIT_FAILURE);
    }

    Folder folder{};
    folder.SetParameterS(41,0.95,1.05);
    folder.SetParameterR(41,0.80,1.20);

    const std::pair<int, int> folding_sizes = folder.GetFoldingSizes();

    static const std::vector<std::string> only_nominal = {"Data",
                                                          "Multijet",
                                                          "SingleTop_DS_PP8_tW_chan_FS",
                                                          "SingleTop_PP8_s_chan_AFII",
                                                          "SingleTop_PP8_t_chan_AFII",
                                                          "SingleTop_PP8_tW_chan_AFII",
                                                          "SingleTop_PH7_0_4_s_chan_AFII",
                                                          "SingleTop_PH7_0_4_t_chan_AFII",
                                                          "SingleTop_PH7_0_4_tW_chan_AFII",
                                                          "SingleTop_aMCNLO_Pythia8_s_chan_AFII",
                                                          "SingleTop_aMCNLO_Pythia8_t_chan_AFII",
                                                          "SingleTop_aMCNLO_Pythia8_tW_chan_AFII",
                                                          "ttbar_PP8_mass_172_AFII",
                                                          "ttbar_PP8_mass_173_AFII",
                                                          "ttbar_PP8_hdamp_AFII"};

    static const std::vector<std::string> non_syst = {
                                                      "ttbar_PP8_AFII",
                                                      "ttbar_PH7_1_3_AFII",
                                                      "ttbar_PP8_AFII",
                                                      "ttbar_aMCNLO_Herwig7_1_3_AFII",
                                                      "ttbar_aMCNLO_Pythia8_AFII",
                                                      "ttbar_PP8_shower_decor_AFII"
                                                     };

    static const std::vector<std::string> corrections = {
                                                      "ttbar_PH7_1_3_AFII",
                                                      "ttbar_aMCNLO_Herwig7_1_3_AFII",
                                                      "ttbar_aMCNLO_Pythia8_AFII",
                                                     };

    /// Main loop over systematics
    for (std::size_t isyst = 0; isyst < m_syst_list.size(); ++isyst) {
        std::cout << "\nFileProcessor::ProcessAllFiles: Looping over systematic: " << m_syst_list.at(isyst) << ", " << isyst+1 << " out of " << m_syst_list.size() << " systs\n";
        std::vector<HistoMaker> histoMaker;

        for (const auto& iprocess : m_process) {
            const std::vector<std::string> sfsyst_list = weight_manager.GetSfSystList(m_systematics, iprocess);
            histoMaker.emplace_back(iprocess, m_systematics, m_eta_bins, m_pt_bins, sfsyst_list, folding_sizes);
            histoMaker.back().SetCriticalRecoDR(m_critical_reco_dR);
            histoMaker.back().SetCriticalTruthDR(m_critical_truth_dR);
            histoMaker.back().SetLeadingWjetCuts(m_leading_pt_min, m_leading_pt_max);
            histoMaker.back().SetSubLeadingWjetCuts(m_subleading_pt_min, m_subleading_pt_max);
            histoMaker.back().SetLeadingWjetEtaCuts(m_leading_eta_min, m_leading_eta_max);
            histoMaker.back().SetSubLeadingWjetEtaCuts(m_subleading_eta_min, m_subleading_eta_max);
            histoMaker.back().SetAverageWjetCuts(m_average_pt_min, m_average_pt_max);
            histoMaker.back().SetNpvCuts(m_npv_min, m_npv_max);
            histoMaker.back().SetMuCuts(m_mu_min, m_mu_max);
            histoMaker.back().SetRunNumberCuts(m_runNumber_min, m_runNumber_max);
        }

        /// Loop over files
        for (std::size_t ifile = 0; ifile < m_file_path.size(); ++ifile) {

            if (m_systematics != "nominal" && 
                (std::find(only_nominal.begin(), only_nominal.end(), m_file_type.at(ifile)) != only_nominal.end()) ) continue;

            /// This is very special as we need to run on sf but not on syst
            if (m_systematics == "syst" && 
                (std::find(non_syst.begin(), non_syst.end(), m_file_type.at(ifile)) != non_syst.end()) ) continue;

            /// Skip files that are not specified
            if (m_dataset_type != "full") {
                if (m_file_type.at(ifile) != m_dataset_type) continue;
            }
        
            bool applyCorrection(false);
        
            if (std::find(corrections.begin(), corrections.end(), m_file_type.at(ifile)) != corrections.end()) {
                applyCorrection = true;
            }

            std::cout << "\n\tFileProcessor::ProcessAllFiles: Processing file: " << m_file_path.at(ifile) << ", " << ifile+1 << " out of " << m_file_path.size() << " files\n";

            const bool isMC = m_dsid.at(ifile) > 100000 ? true : false;


            /// calculate Xsection
            const float xSec = xSecTool.getXsection(m_dsid.at(ifile));
            if (xSec < 0 && m_dsid.at(ifile) > 100000) {
                std::cerr << "FileProcessor::ProcessAllFiles: Negative cross-section found, skipping the file" << std::endl;
                exit(EXIT_FAILURE);
            }
            
            static constexpr float lumi_1516 = 32988.1 + 3219.56; 
            static constexpr float lumi_17 = 44307.4; 
            static constexpr float lumi_18 = 58450.1;

            float lumi(-1);

            if (m_campaign.at(ifile) == "mc16a") {
                lumi = lumi_1516;
            } else if (m_campaign.at(ifile) == "mc16d") {
                lumi = lumi_17;
            } else if (m_campaign.at(ifile) == "mc16e") {
                lumi = lumi_18;
            } else {
                std::cerr << "FileProcessor::ProcessAllFiles: Unknown campaign type: " << m_campaign.at(ifile) << ", skipping the file" << std::endl;
                exit(EXIT_FAILURE);
            }

            if (isMC) {
                /// pass relevant info to WeightManager
                weight_manager.Clear();
                weight_manager.SetDsidAf2CampaignType(m_dsid.at(ifile), m_af2.at(ifile), m_campaign.at(ifile), m_file_type.at(ifile));
                weight_manager.SetXsecLumi(xSec, lumi);
                weight_manager.ProcessNominalNormalisation();

                if (m_systematics == "sfsyst") {
                    if (applyCorrection) {
                        weight_manager.ReadCorrectionFile();
                    }
                    /// want to calculate it now as it is expensive 
                    weight_manager.ProcessModellingNormalisation();
                }
            }
           
            /// Identify which element of the vector needs to be filled
            std::size_t position(9999);
            auto itr = std::find(m_process.begin(), m_process.end(), m_file_type.at(ifile));
            if (itr == m_process.end()) {
                std::cerr << "FileProcessor::ProcessAllFiles: Unknown file type: " << m_file_type.at(ifile) << " skipping the file" << std::endl;
                exit(EXIT_FAILURE);
            }
            position = std::distance(m_process.begin(), itr);
            
            histoMaker.at(position).SetCurrentSyst(m_syst_list.at(isyst));
            histoMaker.at(position).SetIsMC(isMC);
            
            histoMaker.at(position).SetChi2Params(par);
            histoMaker.at(position).SetChi2Cut(chi2_cut);

            /// Fill histograms, contains event loop
            if (m_systematics == "nominal") {
                histoMaker.at(position).FillHistos(weight_manager, folder, "nominal", m_file_path.at(ifile));

            } else if (m_systematics == "syst") {
                /// Name of the tree is the name of the systematics
                histoMaker.at(position).FillHistos(weight_manager, folder, m_syst_list.at(isyst), m_file_path.at(ifile));

            } else if (m_systematics == "sfsyst") {
                /// SF systematics use the nominal tree
                histoMaker.at(position).FillHistos(weight_manager, folder, "nominal", m_file_path.at(ifile));

            } else {
                std::cerr << "FileProcessor::ProcessAllFiles: Unknown syst type: " << m_systematics << std::endl;
                exit(EXIT_FAILURE);
            }

            // Run on fakes
            if (m_file_type.at(ifile) != "Data") continue;
            std::cout << "\tFileProcessorSingleLep::ProcessAllFiles: Running on: " << m_file_type.at(ifile) << " for fakes" << std::endl;
            auto itr_fakes = std::find(m_process.begin(), m_process.end(), "Multijet");
            if (itr_fakes == m_process.end()) {
                std::cerr << "FileProcessorSingleLep::ProcessAllFiles: Unknown file type: " << m_file_type.at(ifile) << " skipping the file" << std::endl;
                exit(EXIT_FAILURE);
            }
            const std::size_t position_fakes = std::distance(m_process.begin(), itr_fakes);
            histoMaker.at(position_fakes).SetCurrentSyst("nominal");
            histoMaker.at(position_fakes).SetIsMC(false);
            histoMaker.at(position_fakes).SetChi2Params(par);
            histoMaker.at(position_fakes).SetChi2Cut(chi2_cut);
            histoMaker.at(position_fakes).FillHistos(weight_manager, folder, "nominal_Loose", m_file_path.at(ifile));
        }

        std::cout << "\nFileProcessor::ProcessAllFiles: Writing to histos\n";
        /// Finalise and write the histos
        for (std::size_t iprocess = 0; iprocess < m_process.size(); ++iprocess) {
            if (m_systematics != "nominal" && 
                (std::find(only_nominal.begin(), only_nominal.end(), m_process.at(iprocess)) != only_nominal.end())) continue;
            if (m_systematics == "syst" && 
                (std::find(non_syst.begin(), non_syst.end(), m_process.at(iprocess)) != non_syst.end())) continue;
            histoMaker.at(iprocess).Finalise();
            histoMaker.at(iprocess).WriteHistosToFile(out_path, m_name);
        }
    }

    /// Rename the files for the nominal run
    if (m_systematics == "nominal") {
        RenameOutputFiles(out_path);
    }

    /// create SFs for Herwig samples to match Pythia for truth W mass
    if (m_createHerwigCorrection && m_systematics == "nominal") {
        CreateHerwigCorrection();
    }
    
}

void FileProcessor::ReadFileList(const std::string& path) {
    std::cout << "\nFileProcessor::ReadFileList: Reading file list from: " << path << std::endl;
    std::string file_path, file_type, af2, campaign;
    int dsid;

    /// Open file
    std::fstream file(path.c_str(), std::ios_base::in);

    if (!file.is_open()) {
        std::cerr << "FileProcessor::ReadFileList: Cannot open the file list at" << std::endl;
        exit(EXIT_FAILURE);
    }

    while (file >> file_path >> file_type >> af2 >> dsid >> campaign) {
        m_file_path.emplace_back(file_path);
        m_file_type.emplace_back(file_type);
        m_dsid.emplace_back(dsid);
        m_af2.emplace_back(af2);
        m_campaign.emplace_back(campaign);
    }

    if (m_file_path.size() == 0) {
        std::cerr << "FileProcessor::ReadFileList: File list is empty?" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// Check consistency
    if (m_file_path.size() != m_file_type.size()) {
        std::cerr << "FileProcessor::ReadFileList: Size of file_path and file_type does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_file_path.size() != m_campaign.size()) {
        std::cerr << "FileProcessor::ReadFileList: Size of file_path and campaign does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (m_file_path.size() != m_dsid.size()) {
        std::cerr << "FileProcessor::ReadFileList: Size of file_path and dsid does not match!" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "\nFileProcessor::ReadFileList: File list read successfully \n\n";
}

std::vector<std::string> FileProcessor::ReadSystList() const {
    std::vector<std::string> result;

    std::size_t position(0);
    bool found(false);

    for (std::size_t i = 0; i < m_file_type.size(); ++i) {

        /// Get the list of systematics from the nominal ttbar file
        if (m_file_type.at(i) == "ttbar_PP8_FS") {
            found = true;
            position = i;
            break;
        }
    }

    if (!found) {
        std::cerr << "FileProcessor::ReadSystList: Did not find a ttbar_PP8_FS file which we need to read list of systs" << std::endl;
        exit(EXIT_FAILURE);
    }

    /// open the file
    std::unique_ptr<TFile> file (new TFile(m_file_path.at(position).c_str(), "READ"));
    if (!file) {
        std::cerr << "FileProcessor::ReadSystList: Cannot open file at: " << m_file_path.at(position) << std::endl;
        exit(EXIT_FAILURE);
    }

    file->cd();
    TIter next(gDirectory->GetListOfKeys());
    TKey *key;

    static const std::vector<std::string> exclude = {"nominal",
                                                     "sumWeights",
                                                     "truth",
                                                     "particleLevel",
                                                     "AnalysisTracking",
                                                     "ejets_2015",
                                                     "ejets_2016",
                                                     "ejets_2017",
                                                     "ejets_2018",
                                                     "mujets_2015",
                                                     "mujets_2016",
                                                     "mujets_2017",
                                                     "mujets_2018"};

    while ((key = static_cast<TKey*>(next()))) {
        if (!key) continue;
        if (key->IsFolder()) {
            const std::string name = key->GetName();
            if (std::find(exclude.begin(), exclude.end(), name) == exclude.end()) {
                result.emplace_back(name);
            }
        }
    }

    file->Close();
    delete key;

    return result;
}

void FileProcessor::CreateOutputFiles(const std::string& path) const {
    gSystem->MakeDirectory((path+"/"+m_name).c_str());
    for (const auto& iprocess : m_process) {
        const std::string s = path +"/"+ m_name +"/"+ iprocess + "_"+m_name+".root.temp";
        std::unique_ptr<TFile> f(new TFile(s.c_str(), "RECREATE"));
        f->Close();
    }
}

void FileProcessor::RenameOutputFiles(const std::string& path) const {
    std::cout << "\nFileProcessor::RenameOutputFiles: Everything went successfully, renaming files\n";
    for (const auto& iprocess : m_process) {
        const std::string in  = path+"/"+m_name+"/"+iprocess+"_"+m_name+".root.temp";
        const std::string out = path+"/"+m_name+"/"+iprocess+"_"+m_name+".root";
        gSystem->Rename(in.c_str(),out.c_str());
    } 
}
        
void FileProcessor::CreateHerwigCorrection() const {
    std::cout <<  "FileProcessor::CreateHerwigCorrection: Creating Herwig correction files\n";
    /// Open the relevant root files
    const std::string path = "../OutputHistos/" + m_name + "/";
    std::unique_ptr<TFile> herwig_713 (TFile::Open((path + "ttbar_PH7_1_3_AFII_" + m_name + ".root").c_str(), "READ"));
    if (!herwig_713) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot open Powheg+Herwig 7.1.3 file!" << std::endl;
        exit(EXIT_FAILURE);    
    }
    std::unique_ptr<TFile> amcnlo_herwig (TFile::Open((path + "ttbar_aMCNLO_Herwig7_1_3_AFII_" + m_name + ".root").c_str(), "READ"));
    if (!amcnlo_herwig) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot open aMC@NLO + Herwig 7.1.3 file!" << std::endl;
        exit(EXIT_FAILURE);    
    }
    std::unique_ptr<TFile> amcnlo_pythia (TFile::Open((path + "ttbar_aMCNLO_Pythia8_AFII_" + m_name + ".root").c_str(), "READ"));
    if (!amcnlo_pythia) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot open aMC@NLO + Pythia file!" << std::endl;
        exit(EXIT_FAILURE);    
    }
    std::unique_ptr<TFile> pp8 (TFile::Open((path + "ttbar_PP8_AFII_" + m_name + ".root").c_str(), "READ"));
    if (!pp8) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot open PP8 file!" << std::endl;
        exit(EXIT_FAILURE); 
    }

    std::unique_ptr<TH1D> h_herwig_713(dynamic_cast<TH1D*>(herwig_713->Get("nominal/truth_W_m")));
    if (!h_herwig_713) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot read histogram from Powheg+Herwig 7.1.3 file" << std::endl;
        exit(EXIT_FAILURE); 
    }

    std::unique_ptr<TH1D> h_amcnlo_herwig(dynamic_cast<TH1D*>(amcnlo_herwig->Get("nominal/truth_W_m")));
    if (!h_amcnlo_herwig) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot read histogram from aMC@NLO+Herwig 7.1.3 file" << std::endl;
        exit(EXIT_FAILURE); 
    }

    std::unique_ptr<TH1D> h_amcnlo_pythia(dynamic_cast<TH1D*>(amcnlo_pythia->Get("nominal/truth_W_m")));
    if (!h_amcnlo_pythia) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot read histogram from aMC@NLO+Pythia 8 file" << std::endl;
        exit(EXIT_FAILURE); 
    }

    std::unique_ptr<TH1D> h_pp8(dynamic_cast<TH1D*>(pp8->Get("nominal/truth_W_m")));
    if (!h_pp8) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot read histogram from PP8 file" << std::endl;
        exit(EXIT_FAILURE); 
    }

    /// Open the output file
    std::unique_ptr<TFile> output(TFile::Open("data/HerwigCorrections.root", "RECREATE"));
    if (!output) {
        std::cerr << "FileProcessor::CreateHerwigCorrection: Cannot open the output file!" << std::endl;
        exit(EXIT_FAILURE);
    }

    h_herwig_713->Divide(h_pp8.get());
    h_amcnlo_herwig->Divide(h_pp8.get());
    h_amcnlo_pythia->Divide(h_pp8.get());

    output->cd();
    h_herwig_713->Write("PowhegHerwig_7_1_3");
    h_amcnlo_herwig->Write("aMCNLOHerwig");
    h_amcnlo_pythia->Write("aMCNLOPythia");

    /// Close the files
    output->Close();
    herwig_713->Close();
    amcnlo_herwig->Close();
    amcnlo_pythia->Close();
    pp8->Close();
}
