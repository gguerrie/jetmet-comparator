#include "WmassAnalysis/FoldingPlots.h"
#include "WmassAnalysis/Common.h"
#include "WmassAnalysis/Event.h"

#include "ForwardFolding/ForwardFolding/Folder.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TLorentzVector.h"

#include <iostream>

FoldingPlots::FoldingPlots() : 
    m_critical_truth_dR(0.5),
    m_size_s(0),
    m_size_r(0) {
}
    
void FoldingPlots::SetCriticalTruthDR(const float& dR) {
    m_critical_truth_dR = dR;
}

void FoldingPlots::Init(const std::pair<int,int>& sizes) {

    m_size_s = sizes.first;
    m_size_r = sizes.second;

    m_jet1_impossible = std::unique_ptr<TH1D>(new TH1D("","",50,0,300));
    m_jet2_impossible = std::unique_ptr<TH1D>(new TH1D("","",50,0,200));
    m_w_m_impossible = std::unique_ptr<TH1D>(new TH1D("","",100,20,180));
    m_w_m_peak_impossible = std::unique_ptr<TH1D>(new TH1D("","",50,60,100));
    m_w_m_peak_shifted_impossible = std::unique_ptr<TH1D>(new TH1D("","",50,40,90));
    m_w_m_peak_60_90_impossible = std::unique_ptr<TH1D>(new TH1D("","",50,60,90));
    m_jet1_possible = std::unique_ptr<TH1D>(new TH1D("","",50,0,300));
    m_jet2_possible = std::unique_ptr<TH1D>(new TH1D("","",50,0,200));
    m_w_m_possible = std::unique_ptr<TH1D>(new TH1D("","",100,20,180));
    m_w_m_peak_possible = std::unique_ptr<TH1D>(new TH1D("","",50,60,100));
    m_w_m_peak_shifted_possible = std::unique_ptr<TH1D>(new TH1D("","",50,40,90));
    m_w_m_peak_60_90_possible = std::unique_ptr<TH1D>(new TH1D("","",50,60,90));

    for (int is = 0; is < sizes.first; ++is) {
        std::vector<TH1D> tmp_j1;
        std::vector<TH1D> tmp_j2;
        std::vector<TH1D> tmp_w_m;
        std::vector<TH1D> tmp_w_m_peak;
        std::vector<TH1D> tmp_w_m_peak_shifted;
        std::vector<TH1D> tmp_w_m_peak_60_90;

        for (int ir = 0; ir < sizes.second; ++ir) {
            tmp_j1.emplace_back("","",50,0,300);
            tmp_j2.emplace_back("","",50,0,200);
            tmp_w_m.emplace_back("","",100,20,180);
            tmp_w_m_peak.emplace_back("","",50,60,100);
            tmp_w_m_peak_shifted.emplace_back("","",50,40,90);
            tmp_w_m_peak_60_90.emplace_back("","",50,60,90);
        }

        m_jet1_smear.emplace_back(tmp_j1);
        m_jet2_smear.emplace_back(tmp_j2);
        m_w_m_smear.emplace_back(tmp_w_m);
        m_w_m_peak_smear.emplace_back(tmp_w_m_peak);
        m_w_m_peak_shifted_smear.emplace_back(tmp_w_m_peak_shifted);
        m_w_m_peak_60_90_smear.emplace_back(tmp_w_m_peak_60_90);
    }
}


void FoldingPlots::FillAllHistos(const Event& event,
                                 const Folder& folder,
                                 const int& index1,
                                 const int& index2,
                                 const float& weight) {

    bool is_impossible(false);
    if (event.isDilepton) is_impossible = true;
    if (event.truth_jet_failed_to_pair_match->at(index1)) is_impossible = true;
    if (event.truth_jet_failed_to_pair_match->at(index2)) is_impossible = true;
    if (event.truth_jet_closest_truth_dR->at(index1) < m_critical_truth_dR) is_impossible = true;
    if (event.truth_jet_closest_truth_dR->at(index2) < m_critical_truth_dR) is_impossible = true;

    TLorentzVector j1, j2;
    if (event.jet_pt->at(index1) > event.jet_pt->at(index2)) {
        j1.SetPtEtaPhiE(event.jet_pt->at(index1)/1e3,
                        event.jet_eta->at(index1),
                        event.jet_phi->at(index1),
                        event.jet_e->at(index1)/1e3);
        j2.SetPtEtaPhiE(event.jet_pt->at(index2)/1e3,
                        event.jet_eta->at(index2),
                        event.jet_phi->at(index2),
                        event.jet_e->at(index2)/1e3);
    } else {
        j2.SetPtEtaPhiE(event.jet_pt->at(index1)/1e3,
                        event.jet_eta->at(index1),
                        event.jet_phi->at(index1),
                        event.jet_e->at(index1)/1e3);
        j1.SetPtEtaPhiE(event.jet_pt->at(index2)/1e3,
                        event.jet_eta->at(index2),
                        event.jet_phi->at(index2),
                        event.jet_e->at(index2)/1e3);
    }

    const TLorentzVector W = j1 + j2;

    if (is_impossible) {
        m_jet1_impossible->Fill(j1.Pt(), weight);
        m_jet2_impossible->Fill(j2.Pt(), weight);
        m_w_m_impossible->Fill(W.M(), weight);
        m_w_m_peak_impossible->Fill(W.M(), weight);
        m_w_m_peak_shifted_impossible->Fill(W.M(), weight);
        m_w_m_peak_60_90_impossible->Fill(W.M(), weight);
    } else {
        const float truth_j1_pt = event.truth_jet_paired_pt->at(index1)/1e3;
        const float truth_j2_pt = event.truth_jet_paired_pt->at(index2)/1e3;

        const std::vector<std::vector<float> >& smeared_j1 = folder.Smear(j1.Pt(), truth_j1_pt);
        const std::vector<std::vector<float> >& smeared_j2 = folder.Smear(j2.Pt(), truth_j2_pt);

        const float mass_j1 = j1.M();
        const float mass_j2 = j2.M();

        m_jet1_possible->Fill(j1.Pt(), weight);
        m_jet2_possible->Fill(j2.Pt(), weight);
        m_w_m_possible->Fill(W.M(), weight);
        m_w_m_peak_possible->Fill(W.M(), weight);
        m_w_m_peak_shifted_possible->Fill(W.M(), weight);
        m_w_m_peak_60_90_possible->Fill(W.M(), weight);
        for (std::size_t is = 0; is < smeared_j1.size(); ++is) {
            for (std::size_t ir = 0; ir < smeared_j1.at(is).size(); ++ir) {
                m_jet1_smear.at(is).at(ir).Fill(smeared_j1.at(is).at(ir), weight);
                m_jet2_smear.at(is).at(ir).Fill(smeared_j2.at(is).at(ir), weight);
                TLorentzVector smeared_1, smeared_2;
                smeared_1.SetPtEtaPhiM(smeared_j1.at(is).at(ir),
                                       j1.Eta(),
                                       j1.Phi(),
                                       mass_j1);
                smeared_2.SetPtEtaPhiM(smeared_j2.at(is).at(ir),
                                       j2.Eta(),
                                       j2.Phi(),
                                       mass_j2);


                const TLorentzVector smeared_W = smeared_1 + smeared_2;
                m_w_m_smear.at(is).at(ir).Fill(smeared_W.M(), weight);
                m_w_m_peak_smear.at(is).at(ir).Fill(smeared_W.M(), weight);
                m_w_m_peak_shifted_smear.at(is).at(ir).Fill(smeared_W.M(), weight);
                m_w_m_peak_60_90_smear.at(is).at(ir).Fill(smeared_W.M(), weight);
            }
        }
    }
}

void FoldingPlots::Finalise() {
    Common::AddOverflow(m_jet1_impossible.get());
    Common::AddOverflow(m_jet2_impossible.get());
    Common::AddOverflow(m_w_m_impossible.get());
    Common::AddOverflow(m_jet1_possible.get());
    Common::AddOverflow(m_jet2_possible.get());
    Common::AddOverflow(m_w_m_possible.get());
    for (int is = 0; is < m_size_s; ++is) {
        for (int ir = 0; ir < m_size_r; ++ir) {
            Common::AddOverflow(&m_jet1_smear.at(is).at(ir));
            Common::AddOverflow(&m_jet2_smear.at(is).at(ir));
            Common::AddOverflow(&m_w_m_smear.at(is).at(ir));
        }
    }
}

void FoldingPlots::Write(TFile* file) {
    file->cd("nominal");
    m_jet1_impossible->Write("Wjet1_pt_impossible");
    m_jet2_impossible->Write("Wjet2_pt_impossible");
    m_w_m_impossible->Write("W_m_impossible");
    m_w_m_peak_impossible->Write("W_m_peak_impossible");
    m_w_m_peak_shifted_impossible->Write("W_m_peak_shifted_impossible");
    m_w_m_peak_60_90_impossible->Write("W_m_peak_60_90_impossible");
    m_jet1_possible->Write("Wjet1_pt_possible");
    m_jet2_possible->Write("Wjet2_pt_possible");
    m_w_m_possible->Write("W_m_possible");
    m_w_m_peak_possible->Write("W_m_peak_possible");
    m_w_m_peak_shifted_possible->Write("W_m_peak_shifted_possible");
    m_w_m_peak_60_90_possible->Write("W_m_peak_60_90_possible");
    
    for (int is = 0; is < m_size_s; ++is) {
       file->cd("nominal");
       gDirectory->mkdir(("param_s_"+std::to_string(is)).c_str());
       file->cd(("nominal/param_s_"+std::to_string(is)).c_str());
        
        for (int ir = 0; ir < m_size_r; ++ir) {
            file->cd(("nominal/param_s_"+std::to_string(is)).c_str());
            gDirectory->mkdir(("param_r_"+std::to_string(ir)).c_str());
            file->cd(("nominal/param_s_"+std::to_string(is)+"/"+"param_r_"+std::to_string(ir)).c_str());
            m_jet1_smear.at(is).at(ir).Write("Wjet1_pt");
            m_jet2_smear.at(is).at(ir).Write("Wjet2_pt");
            m_w_m_smear.at(is).at(ir).Write("W_m");
            m_w_m_peak_smear.at(is).at(ir).Write("W_m_peak");
            m_w_m_peak_shifted_smear.at(is).at(ir).Write("W_m_peak_shifted");
            m_w_m_peak_60_90_smear.at(is).at(ir).Write("W_m_peak_60_90");
        }
    }
}
