#include "WmassAnalysis/HistoMaker.h"
#include "WmassAnalysis/Common.h"
#include "WmassAnalysis/Event.h"
#include "WmassAnalysis/WeightManager.h"

#include "TDirectory.h"
#include "TFile.h"
#include "TTree.h"

#include <algorithm>
#include <iostream>

HistoMaker::HistoMaker(const std::string& type,
                       const std::string& systematics,
                       const std::vector<float>& eta_bins,
                       const std::vector<float>& pt_bins,
                       const std::vector<std::string>& sfsyst_list,
                       const std::pair<int,int>& folding_sizes) :
    m_critical_reco_dR(0.5),
    m_critical_truth_dR(0.5),
    m_type(type),
    m_systematics(systematics),
    m_current_syst("nominal"),
    m_isMC(true),
    m_isTT(false),
    m_isPP8(false),
    m_isNominal(false),
    m_tree_name("nominal"),
    m_chi2_cut(9999),
    m_leading_pt_min(0),
    m_leading_pt_max(999999),
    m_subleading_pt_min(0),
    m_subleading_pt_max(999999),
    m_leading_eta_min(-1),
    m_leading_eta_max(999999),
    m_subleading_eta_min(-1),
    m_subleading_eta_max(999999),
    m_average_pt_min(-1),
    m_average_pt_max(999999),
    m_npv_min(-1),
    m_npv_max(10000),
    m_mu_min(-1),
    m_mu_max(10000),
    m_runNumber_min(0),
    m_runNumber_max(1000000),
    m_applyCorrection(false),
    m_sfsyst_list(sfsyst_list),
    m_eta_bins(eta_bins),
    m_pt_bins(pt_bins) {
    
    /// Check if the file is ttbar
    static const std::vector<std::string> ttbar_processes = {"ttbar_PP8_FS",
                                                             "ttbar_PP8_AFII",
                                                             "ttbar_PH7_1_3_AFII",
                                                             "ttbar_aMCNLO_Pythia8_AFII",
                                                             "ttbar_aMCNLO_Herwig7_1_3_AFII"};
    
    if (std::find(ttbar_processes.begin(), ttbar_processes.end(), m_type) != ttbar_processes.end()){
        m_isTT = true;
    }

    if (m_type == "ttbar_PP8_FS" || m_type == "ttbar_PP8_FS") {
        m_isPP8 = true;
    }
    
    static const std::vector<std::string> corrections = {
                                                      "ttbar_PH7_1_3_AFII",
                                                      "ttbar_aMCNLO_Herwig7_1_3_AFII",
                                                      "ttbar_aMCNLO_Pythia8_AFII",
                                                     };
    
    if (m_systematics == "sfsyst" && 
            std::find(corrections.begin(), corrections.end(), m_type) != corrections.end()) {

        m_sfsyst_list.emplace_back("HerwigCorrection");
        m_applyCorrection = true;
    }

    if (m_systematics == "nominal") {
        m_isNominal = true;
    }

    /// Allocating on heap as these are huge objects
    m_control_plots = std::unique_ptr<ControlPlots> (new ControlPlots());

    if (m_isTT && m_isNominal) {
        m_truth_plots = std::unique_ptr<TruthPlots> (new TruthPlots());
        if (m_isPP8) {
            m_folding_plots = std::unique_ptr<FoldingPlots> (new FoldingPlots());
        }
    }
    
    if (type == "Multijet") {
        m_sfsyst_list.emplace_back("Alt");

        m_IFFtools_el = std::unique_ptr<CP::AsymptMatrixTool>(new CP::AsymptMatrixTool("AsymptMatrixTool_el"));
        const std::string inputMM_el = "data/Efficiency2D_Data_el.xml";
        m_IFFtools_el->setProperty("Selection", "1T");
        m_IFFtools_el->setProperty("Process", "=1F[T]");
        m_IFFtools_el->setProperty("InputFiles", std::vector<std::string>{inputMM_el});
        m_IFFtools_el->setProperty("EnergyUnit", "GeV");
        m_IFFtools_el->setOutputLevel(MSG::INFO);
        m_IFFtools_el->setProperty("ConvertWhenMissing", true);
        m_IFFtools_el->initialize();

        m_IFFtools_el_alt = std::unique_ptr<CP::AsymptMatrixTool>(new CP::AsymptMatrixTool("AsymptMatrixTool_el_alt"));
        const std::string inputMM_el_alt = "data/Efficiency2D_Data_el_param2.xml";
        m_IFFtools_el_alt->setProperty("Selection", "1T");
        m_IFFtools_el_alt->setProperty("Process", "=1F[T]");
        m_IFFtools_el_alt->setProperty("InputFiles", std::vector<std::string>{inputMM_el_alt});
        m_IFFtools_el_alt->setProperty("EnergyUnit", "GeV");
        m_IFFtools_el_alt->setOutputLevel(MSG::INFO);
        m_IFFtools_el_alt->setProperty("ConvertWhenMissing", true);
        m_IFFtools_el_alt->initialize();

        m_IFFtools_mu = std::unique_ptr<CP::AsymptMatrixTool>(new CP::AsymptMatrixTool("AsymptMatrixTool_mu"));
        const std::string inputMM_mu = "data/Efficiency2D_Data_mu.xml";
        m_IFFtools_mu->setProperty("Selection", "1T");
        m_IFFtools_mu->setProperty("Process", "=1F[T]");
        m_IFFtools_mu->setProperty("InputFiles", std::vector<std::string>{inputMM_mu});
        m_IFFtools_mu->setProperty("EnergyUnit", "GeV");
        m_IFFtools_mu->setOutputLevel(MSG::INFO);
        m_IFFtools_mu->setProperty("ConvertWhenMissing", true);
        m_IFFtools_mu->initialize();

        m_IFFtools_mu_alt = std::unique_ptr<CP::AsymptMatrixTool>(new CP::AsymptMatrixTool("AsymptMatrixTool_mu_alt"));
        const std::string inputMM_mu_alt = "data/Efficiency2D_Data_mu_param2.xml";
        m_IFFtools_mu_alt->setProperty("Selection", "1T");
        m_IFFtools_mu_alt->setProperty("Process", "=1F[T]");
        m_IFFtools_mu_alt->setProperty("InputFiles", std::vector<std::string>{inputMM_mu_alt});
        m_IFFtools_mu_alt->setProperty("EnergyUnit", "GeV");
        m_IFFtools_mu_alt->setOutputLevel(MSG::INFO);
        m_IFFtools_mu_alt->setProperty("ConvertWhenMissing", true);
        m_IFFtools_mu_alt->initialize();
    }
    
    m_control_plots->Init(m_sfsyst_list);

    if (m_isTT && m_isNominal) {
        m_truth_plots->Init(eta_bins, pt_bins);
        if (m_isPP8) {
            m_folding_plots->Init(folding_sizes);
        }
    }
}

void HistoMaker::SetCriticalRecoDR(const float& dR) {
    m_critical_reco_dR = dR;
}

void HistoMaker::SetCriticalTruthDR(const float& dR) {
    m_critical_truth_dR = dR;
}

void HistoMaker::SetChi2Params(const Chi2Params& par) {
    m_matcher.SetChi2Params(par);
}

void HistoMaker::SetChi2Cut(const double& chi2_cut) {
    m_chi2_cut = chi2_cut;
}

void HistoMaker::SetLeadingWjetCuts(const float& min, const float& max) {
    m_leading_pt_min = min;
    m_leading_pt_max = max;
}

void HistoMaker::SetSubLeadingWjetCuts(const float& min, const float& max) {
    m_subleading_pt_min = min;
    m_subleading_pt_max = max;
}

void HistoMaker::SetLeadingWjetEtaCuts(const float& min, const float& max) {
    m_leading_eta_min = min;
    m_leading_eta_max = max;
}

void HistoMaker::SetSubLeadingWjetEtaCuts(const float& min, const float& max) {
    m_subleading_eta_min = min;
    m_subleading_eta_max = max;
}
    
void HistoMaker::SetAverageWjetCuts(const float& min, const float& max) {
    m_average_pt_min = min;
    m_average_pt_max = max;
}

void HistoMaker::SetNpvCuts(const int& min, const int& max) {
    m_npv_min = min;
    m_npv_max = max;
}

void HistoMaker::SetMuCuts(const float& min, const float& max) {
    m_mu_min = min;
    m_mu_max = max;
}

void HistoMaker::SetRunNumberCuts(const unsigned int& min, const unsigned int& max) {
    m_runNumber_min = min;
    m_runNumber_max = max;
}

void HistoMaker::FillHistos(const WeightManager& wei_manager,
                            const Folder& /*folder*/,
                            const std::string& tree_name,
                            const std::string& path) {

    
    /// Set the tree name
    m_tree_name = tree_name;

    std::unique_ptr<TFile> file(new TFile(path.c_str(), "READ"));
    if (!file) {
        std::cerr << "HistoMaker::FillHistos: Cannot open input file: " << path << ", skipping" << std::endl;
        return;
    }
    TTree *tree = (static_cast<TTree*>(file->Get(tree_name.c_str())));
    if (!tree) {
        std::cerr << "HistoMaker::FillHistos: Cannot read tree: " << tree_name << " from file: " << path <<", skipping" << std::endl;
        return;
    }

    const int nentries = tree->GetEntries();

    bool use_loose(false);
    bool is_syst(false);
    if (m_type == "Multijet") use_loose = true;
    if (m_systematics == "syst") is_syst = true;

    if (m_isTT && m_isNominal) {
        m_truth_plots->SetCriticalRecoDR(m_critical_reco_dR);
        m_truth_plots->SetCriticalTruthDR(m_critical_truth_dR);
        if (m_isPP8) {
            m_folding_plots->SetCriticalTruthDR(m_critical_truth_dR);
        }
    }

    Event event(tree, m_isMC, m_isTT, use_loose, is_syst);

    /// Run event loop
    for (int ievent = 0; ievent < nentries; ++ievent) {
        event.GetEntry(ievent);
        if (ievent % 100000 == 0) {
            std::cout << "\t\tProcessing event " << ievent << ", out of " << nentries << " events\n";
        }
        
        if (event.ejets_2015 || event.ejets_2016 || event.ejets_2017 || event.ejets_2018) m_lepton_type = Common::LEPTONTYPE::EL;
        else if (event.mujets_2015 || event.mujets_2016 || event.mujets_2017 || event.mujets_2018) m_lepton_type = Common::LEPTONTYPE::MU;

        // apply event wide cuts
        if (event.npvx < m_npv_min) continue;
        if (event.npvx > m_npv_max) continue;

        // apply run number cuts
        if (m_isMC) {
            if (event.randomRunNumber < m_runNumber_min) continue;
            if (event.randomRunNumber > m_runNumber_max) continue;
        } else {
            if (event.runNumber < m_runNumber_min) continue;
            if (event.runNumber > m_runNumber_max) continue;
        }

        // Mu cuts
        // Use average mu for mc16a and actual mu for mc16d/e
        if (event.ejets_2015 || event.ejets_2016 || event.mujets_2015 || event.mujets_2016) {
            if (m_isMC) {
                if (event.mu < m_mu_min) continue;
                if (event.mu > m_mu_max) continue;
            } else {
                if (event.mu/1.03 < m_mu_min) continue;
                if (event.mu/1.03 > m_mu_max) continue;
            }
        } else {
            if (m_isMC) {
                if (event.mu_actual < m_mu_min) continue;
                if (event.mu_actual > m_mu_max) continue;
            } else {
                if (event.mu_actual/1.03 < m_mu_min) continue;
                if (event.mu_actual/1.03 > m_mu_max) continue;
            }
        }

        float wmass(-1);
        if (m_systematics == "sfsyst") {
            wmass = Common::CalculateTruthWmass(event); 
        }
        
        /// Prepare vector with weights
        std::vector<float> wei_vec;
        if (m_isMC) {
            if (m_systematics == "sfsyst") {
                wei_vec = wei_manager.GetSfSystWeights(event, m_applyCorrection, wmass, m_type);
            } else {
                wei_vec.emplace_back(wei_manager.GetNominalWeight(event));
            }

            if (wei_vec.size() != m_sfsyst_list.size()) {
                std::cerr << "HistoMaker::FillHistos: Size of sf names and sf weights do not match" << std::endl;
                std::cerr << "HistoMaker::FillHistos: Size of sf names: " << m_sfsyst_list.size() << ", size of weights: " << wei_vec.size() << std::endl;
                exit(EXIT_FAILURE);
            }
        } else {
            if (use_loose) {
                wei_vec = GetMMweight(event);
            } else {
                // weight is one for data
                wei_vec.emplace_back(1.);
            }
        }

        if (!m_matcher.ProcessEvent(event)) {
            /// event cannot run chi2 reconstruction, skipping
            continue;
        }
        
        const double best_chi2 = m_matcher.GetBestChi2();

        if (best_chi2 > m_chi2_cut) {
            continue;
        }
        
        const std::pair<std::size_t, std::size_t> indices = m_matcher.GetIndices(event);
        const float average = 0.5*(event.jet_pt->at(indices.first) 
                            + event.jet_pt->at(indices.second));

        if (event.jet_pt->at(indices.first)  > m_leading_pt_max)                 continue;
        if (event.jet_pt->at(indices.first)  < m_leading_pt_min)                 continue;
        if (event.jet_pt->at(indices.second) > m_subleading_pt_max)              continue;
        if (event.jet_pt->at(indices.second) < m_subleading_pt_min)              continue;
        if (std::fabs(event.jet_eta->at(indices.first) ) > m_leading_eta_max)    continue;
        if (std::fabs(event.jet_eta->at(indices.first) ) < m_leading_eta_min)    continue;
        if (std::fabs(event.jet_eta->at(indices.second)) > m_subleading_eta_max) continue;
        if (std::fabs(event.jet_eta->at(indices.second)) < m_subleading_eta_min) continue;
        if (average < m_average_pt_min)                                          continue;
        if (average > m_average_pt_max)                                          continue;

        if (m_isTT && m_isNominal) {
            m_truth_plots->FillAllHistos(event,
                                         indices.first,
                                         indices.second,
                                         wei_vec.at(0));
        }


        if (event.jet_closest_reco_dR->empty())                                 continue;
        if (event.jet_closest_reco_dR->at(indices.first) < m_critical_reco_dR)  continue;
        if (event.jet_closest_reco_dR->at(indices.second) < m_critical_reco_dR) continue;

        m_control_plots->FillAllHistos(event,
                                       indices.first,
                                       indices.second,
                                       best_chi2,
                                       m_isMC,
                                       wei_vec);

        //if (m_isPP8 && m_isNominal) {
        //    m_folding_plots->FillAllHistos(event,
        //                                   folder,
        //                                   indices.first,
        //                                   indices.second,
        //                                   wei_vec.at(0));
        //}
    }

    delete tree;
    file->Close();
}

void HistoMaker::Finalise() {
    m_control_plots->Finalise();
    if (m_isTT && m_isNominal) {
        m_truth_plots->Finalise();
        if (m_isPP8) {
            m_folding_plots->Finalise();
        }
    }
}

void HistoMaker::WriteHistosToFile(const std::string& path, const std::string& name) {
    const std::string suffix = m_isNominal ? ".temp" : ""; 
    const std::string final_path = path+"/"+name+"/"+m_type+"_"+name+".root"+suffix;

    std::unique_ptr<TFile> file(new TFile(final_path.c_str(), "UPDATE"));
    if (!file) {
        std::cerr << "HistoMaker::WriteHistosToFile: Cannot open file at: " << final_path << std::endl;
        exit(EXIT_FAILURE);
    }
    
    /// Create the directory structure
    if (m_isNominal) {
        file->cd();
        gDirectory->mkdir("nominal");
        if (m_type == "Multijet") {
            gDirectory->mkdir("Alt");
        }
        const std::vector<std::string> eta_names = ::Common::GetBinsNames(m_eta_bins, false);
        for (const auto& ieta : eta_names) {
            file->cd("nominal");
            gDirectory->mkdir(ieta.c_str());
            const std::vector<std::string> pt_names = ::Common::GetBinsNames(m_pt_bins, true);
            for (const auto& ipt : pt_names) {
                file->cd(("nominal/"+ieta).c_str());
                gDirectory->mkdir(ipt.c_str());
            }
        }

    } else if (m_systematics == "syst") {
        if (!file->Get(m_current_syst.c_str())) {
            file->cd();
            gDirectory->mkdir(m_current_syst.c_str());
        }
    } else if (m_systematics == "sfsyst") {
        for (const auto& isf : m_sfsyst_list) {
            if (!file->Get(isf.c_str())) {
                file->cd();
                gDirectory->mkdir(isf.c_str());
            }
        }
    }

    m_control_plots->Write(m_current_syst, file.get());
    if (m_isTT && m_isNominal) {
        m_truth_plots->Write(file.get());
        if (m_isPP8) {
            m_folding_plots->Write(file.get());
        }
    }

    file->Close();
}
    
std::vector<float> HistoMaker::GetMMweight(const Event& event) const {
   
    std::vector<float> result;
    float wei(0);
    float wei_alt(0);
    xAOD::IParticleContainer MMleptons;
    MMleptons.emplace_back();
    if (m_lepton_type == Common::LEPTONTYPE::EL){
        MMleptons.back().charge = event.el_charge->at(0);
        MMleptons.back().pt   = event.el_pt->at(0);
        MMleptons.back().eta  = event.el_eta->at(0);
        MMleptons.back().phi  = event.el_phi->at(0);
        MMleptons.back().type = xAOD::Type::Electron;
        MMleptons.back().tight = event.el_isTight->at(0);
        m_IFFtools_el->addEvent(MMleptons);
        m_IFFtools_el->getEventWeight(wei, "1T", "=1F[T]");
        m_IFFtools_el_alt->addEvent(MMleptons);
        m_IFFtools_el_alt->getEventWeight(wei_alt, "1T", "=1F[T]");
        result.emplace_back(wei);
        result.emplace_back(wei_alt);
    } else {
        MMleptons.back().charge = event.mu_charge->at(0);
        MMleptons.back().pt   = event.mu_pt->at(0);
        MMleptons.back().eta  = event.mu_eta->at(0);
        MMleptons.back().phi  = event.mu_phi->at(0);
        MMleptons.back().type = xAOD::Type::Muon;
        MMleptons.back().tight = event.mu_isTight->at(0);
        m_IFFtools_mu->addEvent(MMleptons);
        m_IFFtools_mu->getEventWeight(wei, "1T", "=1F[T]");
        m_IFFtools_mu_alt->addEvent(MMleptons);
        m_IFFtools_mu_alt->getEventWeight(wei_alt, "1T", "=1F[T]");
        result.emplace_back(wei);
        result.emplace_back(wei_alt);
    }

    return result;
}
