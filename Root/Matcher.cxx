#include "WmassAnalysis/Matcher.h"

#include "Math/Vector4D.h"

#include <algorithm>
#include <iostream>

Matcher::Matcher() :
    m_mass_w(80.38),
    m_mass_t(172.5),
    m_width_w(25),
    m_width_t(35),
    m_chi2(99999),
    m_index1(999),
    m_index2(999) {
}

void Matcher::SetChi2Params(const Chi2Params& par) {
    m_mass_w = par.mass_w;
    m_mass_t = par.mass_t;
    m_width_w = par.width_w;
    m_width_t = par.width_t;
}

bool Matcher::ProcessEvent(const Event& event) {
    m_chi2 = 99999999.;
    m_index1 = 999;
    m_index2 = 999;
    m_bIndex = 999;

    /// Get b-jet and light jet indices
    std::vector<std::size_t> bIndices;
    std::vector<std::size_t> lIndices;

    for (std::size_t ijet = 0; ijet < event.jet_pt->size(); ++ijet) {
        if (event.jet_isbtagged_DL1d_60->at(ijet)) {
            bIndices.emplace_back(ijet);
        } else {
            if (lIndices.size() < 4) {
                lIndices.emplace_back(ijet);
            }
        }
    }

    if (lIndices.size() < 2) {
        return false;
    }
    
    if (bIndices.size() == 0) {
        return false;
    }

    std::vector<std::size_t> lIndicesOrig = lIndices;

    // original b jet index and light jet indices
    this->RunSinglePermutation(event, bIndices.at(0), lIndices.at(0), lIndices.at(1));

    // run light jet permutations
    while (std::next_permutation(lIndices.begin(), lIndices.end())) {
        // effectively the same
        if (m_index1 == lIndices.at(0) &&
            m_index2 == lIndices.at(1)) continue;
        if (m_index1 == lIndices.at(1) &&
            m_index2 == lIndices.at(0)) continue;
        this->RunSinglePermutation(event, bIndices.at(0), lIndices.at(0), lIndices.at(1));
    }
    // now run the permutations on b_jets
    while (std::next_permutation(bIndices.begin(), bIndices.end())) {
        // original light jets
        this->RunSinglePermutation(event, bIndices.at(0), lIndicesOrig.at(0), lIndicesOrig.at(1));
        // permutation of light jets
        while (std::next_permutation(lIndicesOrig.begin(), lIndicesOrig.end())) {
            // effectively the same
            if (m_bIndex == bIndices.at(0) &&
                m_index1 == lIndicesOrig.at(0) &&
                m_index2 == lIndicesOrig.at(1)) continue;
            if (m_bIndex == bIndices.at(0) &&
                m_index1 == lIndicesOrig.at(1) &&
                m_index2 == lIndicesOrig.at(0)) continue;
            this->RunSinglePermutation(event, bIndices.at(0), lIndicesOrig.at(0), lIndicesOrig.at(1));
        }
    }

    /// successful run
    return true;
}

const double& Matcher::GetBestChi2() const {
    return m_chi2;
}

std::pair<std::size_t, std::size_t> Matcher::GetIndices(const Event& event) const {
    if (event.jet_pt->at(m_index1) > event.jet_pt->at(m_index2)) {
        return std::make_pair(m_index1, m_index2);
    }
    return std::make_pair(m_index2, m_index1);
}

double Matcher::Chi2(const double& reco_mass_w, const double& reco_mass_t) const {
    const double a = (reco_mass_w - m_mass_w)*(reco_mass_w - m_mass_w)/(m_width_w*m_width_w);
    const double b = (reco_mass_t - m_mass_t)*(reco_mass_t - m_mass_t)/(m_width_t*m_width_t);

    return a + b;
}

void Matcher::RunSinglePermutation(const Event& event, const int bIndex, const int lIndex1, const int lIndex2) {
    ROOT::Math::PtEtaPhiEVector bHad(event.jet_pt->at(bIndex)/1e3,
                                     event.jet_eta->at(bIndex),
                                     event.jet_phi->at(bIndex),
                                     event.jet_e->at(bIndex)/1e3);
    ROOT::Math::PtEtaPhiEVector l1(event.jet_pt->at(lIndex1)/1e3,
                                   event.jet_eta->at(lIndex1),
                                   event.jet_phi->at(lIndex1),
                                   event.jet_e->at(lIndex1)/1e3);
    ROOT::Math::PtEtaPhiEVector l2(event.jet_pt->at(lIndex2)/1e3,
                                   event.jet_eta->at(lIndex2),
                                   event.jet_phi->at(lIndex2),
                                   event.jet_e->at(lIndex2)/1e3);
    const float mW = (l1+l2).M();
    const float mT = (l1+l2+bHad).M();
    const float currentChi2 = this->Chi2(mW, mT);
    if (currentChi2 < m_chi2) {
        m_chi2 = currentChi2;
        m_index1 = lIndex1;
        m_index2 = lIndex2;
        m_bIndex = bIndex;
    }
}
