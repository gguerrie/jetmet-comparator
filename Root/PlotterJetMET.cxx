#include "WmassAnalysis/PlotterJetMET.h"
#include "AtlasUtils/AtlasStyle.h"
#include "WmassAnalysis/Common.h"

#include "TCanvas.h"
#include "TColor.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TLatex.h"
#include "TLine.h"
#include "TLegend.h"
#include "TPad.h"
#include "TSystem.h"

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <fstream>

PlotterJetMET::PlotterJetMET(const std::string& directory20, const std::string& directory21) :
    m_directory("MC20vMC21"),
    m_directory20(directory20),
    m_directory21(directory21),
    m_data_name("SetMe"),
    m_data_file(nullptr),
    m_atlas_label("Internal"),
    m_lumi_label("139"),
    m_collection(""),
    m_nominal_ttbar_name(""),
    m_nominal_ttbar_index(999),
    m_syst_shape_only(false),
    m_run_syst(false) {

    SetAtlasStyle();
    FillStyleMap();
    SetColourMap();
    SetLabelMap();
}

void PlotterJetMET::SetTTbarNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "PlotterJetMET::SetTTbarNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_ttbar_names = names;
}

void PlotterJetMET::SetBackgroundNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "PlotterJetMET::SetBackgroundNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_background_names = names;
    m_background_names_with_impossible.emplace_back("ttbar_impossible");
    m_background_names_with_impossible.insert(m_background_names_with_impossible.end(), names.begin(), names.end());
}

void PlotterJetMET::SetSpecialNames(const std::vector<std::string>& names) {
    if (names.size() == 0) {
        std::cerr << "PlotterJetMET::SetSpecialNames: Passed name vector is empty!" << std::endl;
        exit(EXIT_FAILURE);
    }
    m_special_names = names;
}

void PlotterJetMET::SetDataName(const std::string& name) {
    m_data_name = name;
}

void PlotterJetMET::SetAtlasLabel(const std::string& label) {
    m_atlas_label = label;
}

void PlotterJetMET::SetLumiLabel(const std::string& label) {
    m_lumi_label = label;
}

void PlotterJetMET::SetEtaPtBins(const std::vector<float>& eta_bins, const std::vector<float>& pt_bins){
    m_eta_bins = eta_bins;
    m_pt_bins = pt_bins;

    m_eta_names = Common::GetBinsNames(m_eta_bins, false);
    m_pt_names  = Common::GetBinsNames(m_pt_bins, true);

    for (const auto& ieta : m_eta_names) {
        gSystem->mkdir(("../Plots/"+m_directory+"/"+ieta).c_str());
    }
}

void PlotterJetMET::SetCollection(const std::string& collection) {
    m_collection = collection;
}

void PlotterJetMET::SetNominalTTbarName(const std::string& name) {
    m_nominal_ttbar_name = name;

    auto itr = std::find(m_ttbar_names.begin(), m_ttbar_names.end(), name);
    if (itr == m_ttbar_names.end()) {
        std::cerr << "PlotterJetMET::SetNominalTTbarName: The nominal ttbar name is not in the list of ttbar names" << std::endl;
        exit(EXIT_FAILURE);
    }

    m_nominal_ttbar_index = std::distance(m_ttbar_names.begin(), itr);
}

void PlotterJetMET::OpenRootFiles() {

    std::cout << "Opening .root files.." << std::endl;
    /// TTbar files
    for (const auto& ittbar : m_ttbar_names) {
        std::unique_ptr<TFile> f(new TFile(("../OutputHistos/"+m_directory20+"/"+ittbar+"_"+m_directory20+".root").c_str(), "READ"));
        if (!f) {
            std::cerr << "PlotterJetMET::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/"+m_directory20+"/"+ittbar+"_"+m_directory20+".root" << std::endl;
            exit(EXIT_FAILURE);
        }

        std::unique_ptr<TFile> t(new TFile(("../OutputHistos/"+m_directory21+"/"+ittbar+"_"+m_directory21+".root").c_str(), "READ"));
        if (!t) {
            std::cerr << "PlotterJetMET::OpenRootFiles: Cannot open file: " <<  "../OutputHistos/"+m_directory21+"/"+ittbar+"_"+m_directory21+".root" << std::endl;
            exit(EXIT_FAILURE);
        }

        m_ttbar_files.emplace_back(std::move(f));

        m_ttbar_files.emplace_back(std::move(t));

    }

    /// Create directory
    gSystem->mkdir(("../Plots/"+m_directory).c_str());
    gSystem->mkdir(("../Plots/"+m_directory+"/Response").c_str());
    gSystem->mkdir(("../Plots/"+m_directory+"/FF").c_str());
}

void PlotterJetMET::PlotResponsePlots(const std::string& name, const std::string& axis, const bool& normalise) const {
    if (m_eta_names.size() == 0 || m_pt_names.size() == 0) {
        std::cerr << "PlotterJetMET::PlotResponsePlots: eta or pt binning not set!" << std::endl;
        exit(EXIT_FAILURE);
    }

    std::cout << "PlotterJetMET::PlotResponsePlots: Plotting " << name << ", this may take a while...\n";

    for (const auto& ieta : m_eta_names) {
        for (const auto& ipt : m_pt_names) {
            std::vector<std::unique_ptr<TH1D> > histos;
            for (const auto& ifile : m_ttbar_files) {
                std::string namefile = "nominal/"+ieta+"/"+ipt+"/"+name;
                //std::cout << "Opening file: " << namefile << std::endl;
                if (ifile->Get(("nominal/"+ieta+"/"+ipt+"/"+name).c_str()) == nullptr) {
                    std::cerr << "Cannot read histogram: " << "nominal/"+ieta+"/"+ipt+"/"+name << " ignoring.\n";
                    continue;
                }
                histos.emplace_back(static_cast<TH1D*>(ifile->Get(("nominal/"+ieta+"/"+ipt+"/"+name).c_str())));
            }
            
            /// normalise to unity
            if (normalise) {
                for (auto& ihist : histos) {
                    ihist->Scale(1./ihist->Integral());
                }
            }
            //std::cout << "Histos created and normalized" << std::endl;
            TCanvas canvas("","",800,600);
            canvas.cd();
            TPad pad1("pad1","pad1",0.0, 0.3, 1.0, 1.00);
            TPad pad2("pad2","pad2",0.0, 0.010, 1.0, 0.3);
            pad1.SetBottomMargin(0.001);
            pad1.SetBorderMode(0);
            pad2.SetBottomMargin(0.4);
            pad1.Draw();
            pad2.Draw();
            

            PlotUpperComparison(histos, &pad1, axis);
            //std::cout << "PlotUpperComparison.." << std::endl;

            /// Draw legend
            TLegend leg(0.60,0.7,0.75,0.9);
            std::vector<std::string> suffix = {m_directory20, m_directory21};
            for (std::size_t ihist = 0; ihist < histos.size(); ++ihist) {
                std::string histname = m_ttbar_names.at(0)+ "_" + suffix.at(ihist);
                //leg.AddEntry(histos.at(ihist).get(), (m_ttbar_names.at(ihist)).c_str(), "l");
                leg.AddEntry(histos.at(ihist).get(), histname.c_str(), "l");
            }
            leg.SetFillColor(0);
            leg.SetLineColor(0);
            leg.SetBorderSize(0);
            leg.SetTextFont(72);
            leg.SetTextSize(0.035);
            leg.Draw("same");

            TLatex l1;
            l1.SetTextAlign(9);
            l1.SetTextSize(0.05);
            l1.SetTextFont(42);
            l1.SetNDC();
            l1.DrawLatex(0.72, 0.5, Common::NiceRangeLabels(ieta, true).c_str());
            l1.DrawLatex(0.72, 0.43, Common::NiceRangeLabels(ipt, false).c_str());

            pad1.RedrawAxis();

            pad2.cd();
            //std::cout << "TLegend.." << std::endl;

            std::vector<std::unique_ptr<TH1D> > ratios;
            for (auto& ihist : histos) {
                ratios.emplace_back(static_cast<TH1D*>(ihist->Clone()));
            }

            PlotRatioComparison(ratios, &pad2, axis, 0.3, true);

            //std::cout << "PlotRatioComparison.." << std::endl;

            /// Draw line
            const float min = ratios.at(0)->GetXaxis()->GetBinLowEdge(1);
            const float max = ratios.at(0)->GetXaxis()->GetBinUpEdge(ratios.at(0)->GetNbinsX());
            TLine line(min, 1, max, 1);
            line.SetLineColor(kBlack);
            line.SetLineStyle(2);
            line.SetLineWidth(3);
            line.Draw("same");

            pad2.RedrawAxis();

            canvas.Print(("../Plots/"+m_directory+"/"+ieta+"/"+name+"_"+ipt+".png").c_str());
            canvas.Print(("../Plots/"+m_directory+"/"+ieta+"/"+name+"_"+ipt+".pdf").c_str());

        } // loop over pt
    } // loop over eta
}


void PlotterJetMET::CloseRootFiles() {
    for (auto& ifile : m_ttbar_files) {
        ifile->Close();
    }
}

void PlotterJetMET::PlotUpperComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                                  TPad* pad,
                                  const std::string& axis) const {

    if (histos.size() == 0) return;

    pad->cd();

    const float resize = MaxResize(histos, false);

    histos.at(0)->GetYaxis()->SetRangeUser(0.00001, resize);
    histos.at(0)->GetXaxis()->SetTitle(axis.c_str());

    for (std::size_t i = 0; i < histos.size(); ++i) {
        histos.at(i)->SetLineWidth(2);
        if (i < m_styles.size()) {
            histos.at(i)->SetLineColor(m_styles.at(i).first);
            histos.at(i)->SetLineStyle(m_styles.at(i).second);
        } else {
            std::cerr << "PlotterJetMET::PlotUpperComparison: No map for the plot style. Will use some random colours.\n";
            histos.at(i)->SetLineColor(i);
            histos.at(i)->SetLineStyle(i);
        }

        if (i == 0) {
            histos.at(0)->Draw("HIST");
        } else {
            histos.at(i)->Draw("HIST same");
        }
    }

    DrawLabels(pad, 0.2 , 0.85, false);
}

void PlotterJetMET::PlotRatioComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                                  TPad* pad,
                                  const std::string& axis,
                                  const float& range,
                                  const bool& force) const {

    pad->cd();

    histos.at(0)->GetXaxis()->SetLabelFont(42);
    histos.at(0)->GetXaxis()->SetLabelSize(0.15);
    histos.at(0)->GetXaxis()->SetLabelOffset(0.01);
    histos.at(0)->GetXaxis()->SetTitleFont(42);
    histos.at(0)->GetXaxis()->SetTitleSize(0.15);
    histos.at(0)->GetXaxis()->SetTitleOffset(1.2);
    histos.at(0)->GetXaxis()->SetTitle(axis.c_str());

    histos.at(0)->GetYaxis()->SetLabelFont(42);
    histos.at(0)->GetYaxis()->SetLabelSize(0.15);
    histos.at(0)->GetYaxis()->SetLabelOffset(0.03);
    histos.at(0)->GetYaxis()->SetTitleFont(42);
    histos.at(0)->GetYaxis()->SetTitleSize(0.15);
    histos.at(0)->GetYaxis()->SetTitleOffset(0.5);
    histos.at(0)->GetYaxis()->SetNdivisions(505);
    histos.at(0)->GetYaxis()->SetTitle("ratio");

    float max(-10);
    float min(100);
    for (std::size_t ihist = 0; ihist < histos.size(); ++ihist) {
        histos.at(ihist)->Divide(histos.at(0).get());
        if (histos.at(ihist)->GetMaximum() > max) {
            max = histos.at(ihist)->GetMaximum();
        }
        if (histos.at(ihist)->GetMinimum() < min) {
            min = histos.at(ihist)->GetMinimum();
        }
        if (ihist == 0) {
            histos.at(0)->Draw("HIST");
        } else {
            histos.at(ihist)->Draw("HIST same");
        }
    }

    if (force) {
        histos.at(0)->GetYaxis()->SetRangeUser(1-range, 1.+range);
    } else {
        histos.at(0)->GetYaxis()->SetRangeUser(0.8*min, 1.2*max);
    }
}

void PlotterJetMET::FillStyleMap() {
    m_styles.push_back(std::make_pair(kBlack, 1));
    m_styles.push_back(std::make_pair(kBlue, 1));
    m_styles.push_back(std::make_pair(kRed, 1));
    m_styles.push_back(std::make_pair(kGreen+2, 1));
    m_styles.push_back(std::make_pair(kMagenta, 1));
}

float PlotterJetMET::MaxResize(const std::vector<std::unique_ptr<TH1D> >& histos, const bool& is_log) const {
    float result(-9999);

    for (const auto& ihist : histos) {
        if (result < ihist->GetMaximum()) {
            result = ihist->GetMaximum();
        }
    }

    if (is_log) return result*1e6;

    return result*1.6;
}

void PlotterJetMET::DrawLabels(TPad *pad, const float& x, const float& y, const bool& add_lumi) const{
    pad->cd();

    TLatex l1;
    l1.SetTextAlign(9);
    l1.SetTextFont(72);
    l1.SetTextSize(0.06);
    l1.SetNDC();
    l1.DrawLatex(x, y, "ATLAS");

    TLatex l2;
    l2.SetTextAlign(9);
    l2.SetTextSize(0.06);
    l2.SetTextFont(42);
    l2.SetNDC();
    l2.DrawLatex(x+0.11, y, m_atlas_label.c_str());

    TLatex l3;
    l3.SetTextAlign(9);
    l3.SetTextSize(0.06);
    l3.SetTextFont(42);
    l3.SetNDC();
    if (add_lumi) {
        l3.DrawLatex(x, y-0.07, ("#sqrt{s} = 13 TeV, "+m_lumi_label+" fb^{-1}").c_str());
    } else {
        l3.DrawLatex(x, y-0.07, "#sqrt{s} = 13 TeV");
    }

    std::string collection;
    /// Add collection name
    if (m_collection == "topo")  {
        collection = "Anti-kt, R=0.4, EMTopo";
    } else if (m_collection == "pflow") {
        collection = "Anti-kt, R=0.4, EMPFlow";
    } else {
        std::cout << "PlotterJetMET::DrawLabels: Collection not set, setting to 'pflow'\n";
        collection = "Anti-kt, R=0.4, EMPFlow";
    }

    l2.DrawLatex(x, y-0.15, collection.c_str());
}

void PlotterJetMET::SetColourMap() {
    m_colour_map["ttbar_impossible"] = kSpring-7;
    m_colour_map["SingleTop_PP8_s_chan_FS"] = kBlue-1;
    m_colour_map["SingleTop_PP8_t_chan_FS"] = kBlue+1;
    m_colour_map["SingleTop_PP8_tW_chan_FS"] = kBlue;
    m_colour_map["Wjets_Sherpa_FS"] = 92;
    m_colour_map["Zjets_Sherpa_FS"] = 95;
    m_colour_map["Diboson_Sherpa_FS"] = 5;
    m_colour_map["ttV_aMCNLO_Pythia8_FS"] = kRed;
    m_colour_map["ttH_PP8_FS"] = kRed;
    m_colour_map["Multijet"] = 619;
}

void PlotterJetMET::TransformErrorHistoToTGraph(TGraphAsymmErrors* error, TH1D* up, TH1D* down) const {
    const int nbins = error->GetN();

    for (int ibin = 0; ibin < nbins; ++ibin) {
        error->SetPointEYhigh(ibin, up->GetBinContent(ibin+1));
        error->SetPointEYlow (ibin, -down->GetBinContent(ibin+1));
    }
}

void PlotterJetMET::SetLabelMap() {
    m_label_map["ttbar_PP8_FS"] = "t#bar{t} PP8";
    m_label_map["ttbar_impossible"] = "t#bar{t} not matched";
    m_label_map["ttbar_PP8_AFII"] = "t#bar{t} PP8 AFII";
    m_label_map["ttbar_PP8_dilep_FS"] = "t#bar{t} PP8";
    m_label_map["ttbar_PP8_dilep_AFII"] = "t#bar{t} PP8 AFII";
    m_label_map["Wjets_Sherpa_FS"] = "W+jets";
    m_label_map["Zjets_Sherpa_FS"] = "Z+jets";
    m_label_map["Diboson_Sherpa_FS"] = "Diboson";
    m_label_map["ttV_aMCNLO_Pythia8_FS"] = "t#bar{t}+V/H";
    m_label_map["ttH_PP8_FS"] = "t#bar{t}+V/H";
    m_label_map["SingleTop_PP8_s_chan_FS"] = "SingleTop s-chan";
    m_label_map["SingleTop_PP8_t_chan_FS"] = "SingleTop t-chan";
    m_label_map["SingleTop_PP8_tW_chan_FS"] = "SingleTop tW-chan";
    m_label_map["SingleTop_PP8_tW_chan_dilep_FS"] = "SingleTop tW-chan";
}
