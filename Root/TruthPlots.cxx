#include "WmassAnalysis/TruthPlots.h"
#include "WmassAnalysis/Common.h"
#include "WmassAnalysis/Event.h"

#include "TFile.h"
#include "TLorentzVector.h"

#include <iostream>

TruthPlots::TruthPlots() :
    m_critical_reco_dR(0.5),
    m_critical_truth_dR(0.5)
{
}

void TruthPlots::Init(const std::vector<float>& eta_bins,
                      const std::vector<float>& pt_bins) {

    m_eta_bins = eta_bins;
    m_pt_bins  = pt_bins;

    m_truth_W_m = std::unique_ptr<TH1D>(new TH1D("","",50, 40, 120));
    m_truth_W_pt = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 350));
    m_truth_W_eta = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 5));
    m_reco_truth_ratio_W_m = std::unique_ptr<TH1D>(new TH1D("","",50, 0.5, 2));
    m_truth_W_jets_dR = std::unique_ptr<TH1D>(new TH1D("","",50, 0.5, 3.5));
    m_truth_W_jets_vec_sum_pt = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 350));
    m_truth_W_jets_scalar_sum_pt = std::unique_ptr<TH1D>(new TH1D("","",50, 30, 350));
    m_truth_quark_W_m = std::unique_ptr<TH1D>(new TH1D("","",50, 60, 100));
    m_truth_quark_W_pt = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 350));
    m_truth_quark_W_eta = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 5));
    m_truth_parton_lepton_W_m  = std::unique_ptr<TH1D>(new TH1D("","",50, 60, 100));
    m_truth_parton_lepton_W_pt  = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 350));
    m_truth_parton_lepton_W_eta  = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 5));
    m_truth_identified_W_m = std::unique_ptr<TH1D>(new TH1D("","",50, 40, 120));
    m_truth_identified_W_pt = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 350));
    m_truth_identified_W_eta = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 5));
    m_truth_reco_eff = std::unique_ptr<TH1D>(new TH1D("","",7, 0.5, 7.5));
    
    m_jet_parton_ratio_pt = std::unique_ptr<TH1D>(new TH1D("","",50, 0.5, 1.5));
    m_jet_parton_ratio_e  = std::unique_ptr<TH1D>(new TH1D("","",50, 0.5, 1.5));
    m_jet_parton_difference_m  = std::unique_ptr<TH1D>(new TH1D("","",50, 0, 30));
    m_jet_parton_difference_eta  = std::unique_ptr<TH1D>(new TH1D("","",50, -0.3, 0.3));
    m_jet_parton_difference_phi  = std::unique_ptr<TH1D>(new TH1D("","",50, -0.3, 0.3));
    m_jet_parton_W_difference_pt  = std::unique_ptr<TH1D>(new TH1D("","",50, -50, 50));
    m_jet_parton_W_ratio_pt_50  = std::unique_ptr<TH1D>(new TH1D("","",50, 0.5, 1.5));
    m_jet_parton_W_ratio_m  = std::unique_ptr<TH1D>(new TH1D("","",50, 0.5, 1.5));
    
    for (std::size_t ieta = 0; ieta < (m_eta_bins.size()-1); ++ieta) {
        std::vector<TH1D> m_jet_response_temp;
        std::vector<TH1D> m_jet_response_fifth_jet_temp;
        std::vector<TH1D> m_jet_response_gluon_temp;
        std::vector<TH1D> m_jet_response_leading_gluon_temp;
        std::vector<TH1D> m_jet_response_non_leading_gluon_temp;
        std::vector<TH1D> m_jet_response_light_temp;
        std::vector<TH1D> m_jet_response_s_temp;
        std::vector<TH1D> m_jet_response_c_temp;
        std::vector<TH1D> m_jet_response_b_temp;
        std::vector<TH1D> m_jet_response_from_W_temp;
        std::vector<TH1D> m_jet_response_from_W_light_temp;
        std::vector<TH1D> m_jet_response_from_W_s_temp;
        std::vector<TH1D> m_jet_response_from_W_c_temp;

        /// pt has also an "overflow" bin!
        for (std::size_t ipt = 0; ipt < m_pt_bins.size(); ++ipt) {
            float max = 2.;
            float min = 0;
            if (m_pt_bins.at(ipt) > 100) {
                max = 1.5;
                min = 0.5;
            }
            if (m_pt_bins.at(ipt) > 300) {
               max = 1.5;
               min = 0.5;
            }

            m_jet_response_temp.emplace_back("","",50, min, max);
            m_jet_response_fifth_jet_temp.emplace_back("","",50, min, max);
            m_jet_response_gluon_temp.emplace_back("","",50, min, max);
            m_jet_response_leading_gluon_temp.emplace_back("","",50, min, max);
            m_jet_response_non_leading_gluon_temp.emplace_back("","",50, min, max);
            m_jet_response_light_temp.emplace_back("","",50, min, max);
            m_jet_response_s_temp.emplace_back("","",50, min, max);
            m_jet_response_c_temp.emplace_back("","",50, min, max);
            m_jet_response_b_temp.emplace_back("","",50, min, max);
            m_jet_response_from_W_temp.emplace_back("","",50, min, max);
            m_jet_response_from_W_light_temp.emplace_back("","",50, min, max);
            m_jet_response_from_W_s_temp.emplace_back("","",50, min, max);
            m_jet_response_from_W_c_temp.emplace_back("","",50, min, max);
        }

        m_jet_response.emplace_back(m_jet_response_temp);
        m_jet_response_fifth_jet.emplace_back(m_jet_response_fifth_jet_temp);
        m_jet_response_gluon.emplace_back(m_jet_response_gluon_temp);
        m_jet_response_leading_gluon.emplace_back(m_jet_response_leading_gluon_temp);
        m_jet_response_non_leading_gluon.emplace_back(m_jet_response_non_leading_gluon_temp);
        m_jet_response_light.emplace_back(m_jet_response_light_temp);
        m_jet_response_s.emplace_back(m_jet_response_s_temp);
        m_jet_response_c.emplace_back(m_jet_response_c_temp);
        m_jet_response_b.emplace_back(m_jet_response_b_temp);
        m_jet_response_from_W.emplace_back(m_jet_response_from_W_temp);
        m_jet_response_from_W_light.emplace_back(m_jet_response_from_W_light_temp);
        m_jet_response_from_W_s.emplace_back(m_jet_response_from_W_s_temp);
        m_jet_response_from_W_c.emplace_back(m_jet_response_from_W_c_temp);
    }
}
    
void TruthPlots::SetCriticalRecoDR(const float& dR) {
    m_critical_reco_dR = dR;
}

void TruthPlots::SetCriticalTruthDR(const float& dR) {
    m_critical_truth_dR = dR;
}

void TruthPlots::FillAllHistos(const Event& event,
                               const int& index1,
                               const int& index2,
                               const float& weight) {

    FillInclusiveHistos(event, index1, index2, weight);

    FillExclusiveHistos(event, weight);

    FillRecoEfficiencyHistos(event, index1, index2, weight);
    
    FillParticlePartonHistos(event, weight);
}

void TruthPlots::Finalise() {
}

void TruthPlots::Write(TFile* file) {

    file->cd("nominal");
    m_truth_W_m->Write("truth_W_m");
    m_truth_W_pt->Write("truth_W_pt");
    m_truth_W_eta->Write("truth_W_eta");
    m_reco_truth_ratio_W_m->Write("reco_truth_ratio_W_m");
    m_truth_W_jets_dR->Write("truth_W_jets_dR");
    m_truth_W_jets_vec_sum_pt->Write("truth_W_jets_vec_sum_pt");
    m_truth_W_jets_scalar_sum_pt->Write("truth_W_jets_scalar_sum_pt");
    m_truth_quark_W_m->Write("truth_W_quark_m");
    m_truth_quark_W_pt->Write("truth_W_quark_pt");
    m_truth_quark_W_eta->Write("truth_W_quark_eta");
    m_truth_parton_lepton_W_m->Write("truth_parton_lepton_W_m");
    m_truth_parton_lepton_W_pt->Write("truth_parton_lepton_W_pt");
    m_truth_parton_lepton_W_eta->Write("truth_parton_lepton_W_eta");
    m_truth_identified_W_m->Write("truth_W_identified_m");
    m_truth_identified_W_pt->Write("truth_W_identified_pt");
    m_truth_identified_W_eta->Write("truth_W_identified_eta");
    m_truth_reco_eff->Write("truth_reco_eff");

    m_jet_parton_ratio_pt->Write("jet_parton_ratio_pt");
    m_jet_parton_ratio_e->Write("jet_parton_ratio_e");
    m_jet_parton_difference_m->Write("jet_parton_difference_m");
    m_jet_parton_difference_eta->Write("jet_parton_difference_eta");
    m_jet_parton_difference_phi->Write("jet_parton_difference_phi");
    m_jet_parton_W_difference_pt->Write("jet_parton_W_difference_pt");
    m_jet_parton_W_ratio_pt_50->Write("jet_parton_W_ratio_pt_50");
    m_jet_parton_W_ratio_m->Write("jet_parton_W_ratio_m");
    
    const std::vector<std::string> eta_names = Common::GetBinsNames(m_eta_bins, false);
    const std::vector<std::string> pt_names  = Common::GetBinsNames(m_pt_bins, true);

    for (std::size_t ieta = 0; ieta < eta_names.size(); ++ieta) {
        for (std::size_t ipt = 0; ipt < pt_names.size(); ++ipt) {
            file->cd(("nominal/"+eta_names.at(ieta)+"/"+pt_names.at(ipt)).c_str());
            m_jet_response.at(ieta).at(ipt).Write("jet_response");
            m_jet_response_fifth_jet.at(ieta).at(ipt).Write("jet_response_fifth_jet");
            m_jet_response_gluon.at(ieta).at(ipt).Write("jet_response_gluon");
            m_jet_response_leading_gluon.at(ieta).at(ipt).Write("jet_response_leading_gluon");
            m_jet_response_non_leading_gluon.at(ieta).at(ipt).Write("jet_response_non_leading_gluon");
            m_jet_response_light.at(ieta).at(ipt).Write("jet_response_light");
            m_jet_response_s.at(ieta).at(ipt).Write("jet_response_s");
            m_jet_response_c.at(ieta).at(ipt).Write("jet_response_c");
            m_jet_response_b.at(ieta).at(ipt).Write("jet_response_b");
            m_jet_response_from_W.at(ieta).at(ipt).Write("jet_response_from_W");
            m_jet_response_from_W_light.at(ieta).at(ipt).Write("jet_response_from_W_light");
            m_jet_response_from_W_s.at(ieta).at(ipt).Write("jet_response_from_W_s");
            m_jet_response_from_W_c.at(ieta).at(ipt).Write("jet_response_from_W_c");
        }
    }
}


void TruthPlots::FillInclusiveHistos(const Event& event,
                                     const int& index1,
                                     const int& index2,
                                     const float& weight) {

    if (event.isDilepton) return;

    if ((event.index_W_1 >= 0) && (event.index_W_2 >= 0)) {
        /// select only correctly paired and isolated jets
        if (event.truth_jet_failed_to_pair_match->at(event.index_W_1)) return;
        if (event.truth_jet_closest_truth_dR->at(event.index_W_1) < m_critical_truth_dR) return;
        if (event.jet_closest_reco_dR->at(event.index_W_1) < m_critical_reco_dR) return;
        if (event.truth_jet_failed_to_pair_match->at(event.index_W_2)) return;
        if (event.truth_jet_closest_truth_dR->at(event.index_W_2) < m_critical_truth_dR) return;
        if (event.jet_closest_reco_dR->at(event.index_W_2) < m_critical_reco_dR) return;
        if (std::abs(event.jet_truthPartonLabel->at(event.index_W_1)) > 4) return;
        if (std::abs(event.jet_truthPartonLabel->at(event.index_W_2)) > 4) return;

        TLorentzVector j1, j2, reco_j1, reco_j2;
        j1.SetPtEtaPhiE(event.truth_jet_paired_pt->at(event.index_W_1)/1e3,
                        event.truth_jet_paired_eta->at(event.index_W_1),
                        event.truth_jet_paired_phi->at(event.index_W_1),
                        event.truth_jet_paired_e->at(event.index_W_1)/1e3);
        j2.SetPtEtaPhiE(event.truth_jet_paired_pt->at(event.index_W_2)/1e3,
                        event.truth_jet_paired_eta->at(event.index_W_2),
                        event.truth_jet_paired_phi->at(event.index_W_2),
                        event.truth_jet_paired_e->at(event.index_W_2)/1e3);
        reco_j1.SetPtEtaPhiE(event.jet_pt->at(event.index_W_1)/1e3,
                             event.jet_eta->at(event.index_W_1),
                             event.jet_phi->at(event.index_W_1),
                             event.jet_e->at(event.index_W_1)/1e3);
        reco_j2.SetPtEtaPhiE(event.jet_pt->at(event.index_W_2)/1e3,
                             event.jet_eta->at(event.index_W_2),
                             event.jet_phi->at(event.index_W_2),
                             event.jet_e->at(event.index_W_2)/1e3);

        m_truth_W_pt->Fill((j1+j2).Pt(), weight);
        m_truth_W_eta->Fill(std::fabs((j1+j2).Eta()), weight);
        m_truth_W_m->Fill((j1+j2).M(), weight);
        m_reco_truth_ratio_W_m->Fill((reco_j1+reco_j2).M()/(j1+j2).M(), weight);
        m_truth_W_jets_dR->Fill(j1.DeltaR(j2), weight);
        m_truth_W_jets_vec_sum_pt->Fill((j1+j2).Pt(), weight);
        m_truth_W_jets_scalar_sum_pt->Fill(j1.Pt() + j2.Pt(), weight);
    }

    /// Parton level
    TLorentzVector q1, q2;
    q1.SetPtEtaPhiM(event.W_quark1_pt/1e3,
                    event.W_quark1_eta,
                    event.W_quark1_phi,
                    event.W_quark1_m/1e3);
    q2.SetPtEtaPhiM(event.W_quark2_pt/1e3,
                    event.W_quark2_eta,
                    event.W_quark2_phi,
                    event.W_quark2_m/1e3);

    m_truth_quark_W_pt->Fill((q1+q2).Pt(), weight);
    m_truth_quark_W_eta->Fill(std::fabs((q1+q2).Eta()), weight);
    m_truth_quark_W_m->Fill((q1+q2).M(), weight);

    /// Truth jets from events identified at reco level
    TLorentzVector j1, j2;
    j1.SetPtEtaPhiE(event.truth_jet_paired_pt->at(index1)/1e3,
                    event.truth_jet_paired_eta->at(index1),
                    event.truth_jet_paired_phi->at(index1),
                    event.truth_jet_paired_e->at(index1)/1e3);
    j2.SetPtEtaPhiE(event.truth_jet_paired_pt->at(index2)/1e3,
                    event.truth_jet_paired_eta->at(index2),
                    event.truth_jet_paired_phi->at(index2),
                    event.truth_jet_paired_e->at(index2)/1e3);


    TLorentzVector lepton, nu;
    lepton.SetPtEtaPhiM(event.lepton_pt/1e3,
                        event.lepton_eta,
                        event.lepton_phi,
                        event.lepton_m/1e3);
    nu.SetPtEtaPhiM(event.neutrino_pt/1e3,
                    event.neutrino_eta,
                    event.neutrino_phi,
                    event.neutrino_m/1e3);

    const TLorentzVector lep_W = lepton + nu;
    m_truth_parton_lepton_W_m->Fill(lep_W.M(), weight);
    m_truth_parton_lepton_W_pt->Fill(lep_W.Pt(), weight);
    m_truth_parton_lepton_W_eta->Fill(std::fabs(lep_W.Eta()), weight);

    if (event.truth_jet_failed_to_pair_match->at(index1)) return;
    if (event.truth_jet_closest_truth_dR->at(index1) < m_critical_truth_dR) return;
    if (event.jet_closest_reco_dR->at(index1) < m_critical_reco_dR) return;
    if (event.truth_jet_failed_to_pair_match->at(index2)) return;
    if (event.truth_jet_closest_truth_dR->at(index2) < m_critical_truth_dR) return;
    if (event.jet_closest_reco_dR->at(index2) < m_critical_reco_dR) return;

    m_truth_identified_W_pt->Fill((j1+j2).Pt(), weight);
    m_truth_identified_W_eta->Fill(std::fabs((j1+j2).Eta()), weight);
    m_truth_identified_W_m->Fill((j1+j2).M(), weight);
}

void TruthPlots::FillExclusiveHistos(const Event& event,
                                     const float& weight) {

    if (event.isDilepton) return;

    std::size_t ngluons(0);

    for (int ijet = 0; ijet < (int)event.jet_pt->size(); ijet++) {

        /// select only correctly paired and isolated jets
        if (event.truth_jet_failed_to_pair_match->at(ijet)) continue;
        if (event.truth_jet_closest_truth_dR->at(ijet) < m_critical_truth_dR) continue;
        if (event.jet_closest_reco_dR->at(ijet) < m_critical_reco_dR) continue;

        const std::size_t eta_index = Common::IndexFromVec(event.jet_eta->at(ijet),
                                                           m_eta_bins,
                                                           true);
        const std::size_t pt_index  = Common::IndexFromVec(event.truth_jet_paired_pt->at(ijet)/1e3,
                                                           m_pt_bins,
                                                           false);

        if (((event.index_W_1 == ijet) && (event.index_W_1 >= 0)) ||
            ((event.index_W_2 == ijet) && (event.index_W_2 >= 0))) {
            if (std::abs(event.jet_truthPartonLabel->at(ijet)) < 5) {
                m_jet_response_from_W.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            }
        }
        m_jet_response.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
        if (ijet == 4) {
            m_jet_response_fifth_jet.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
        }
        if ((std::abs(event.jet_truthPartonLabel->at(ijet)) == 1) || (std::abs(event.jet_truthPartonLabel->at(ijet)) == 2)) { ///light jets
            m_jet_response_light.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            if (((event.index_W_1 == ijet) && (event.index_W_1 >= 0)) ||
                ((event.index_W_2 == ijet) && (event.index_W_2 >= 0))) {
                m_jet_response_from_W_light.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            }
        }
        if (std::abs(event.jet_truthPartonLabel->at(ijet)) == 3) { /// s
            m_jet_response_s.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            if (((event.index_W_1 == ijet) && (event.index_W_1 >= 0)) ||
                ((event.index_W_2 == ijet) && (event.index_W_2 >= 0))) {
                m_jet_response_from_W_s.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            }
        }
        if (std::abs(event.jet_truthPartonLabel->at(ijet)) == 4) { /// c
            m_jet_response_c.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            if (((event.index_W_1 == ijet) && (event.index_W_1 >= 0)) ||
                ((event.index_W_2 == ijet) && (event.index_W_2 >= 0))) {
                m_jet_response_from_W_c.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            }
        }
        if (std::abs(event.jet_truthPartonLabel->at(ijet)) == 5) { /// b
            m_jet_response_b.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
        }
        if (std::abs(event.jet_truthPartonLabel->at(ijet)) == 21) { /// gluon
            m_jet_response_gluon.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            if (ngluons == 0) {
                m_jet_response_leading_gluon.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            } else {
                m_jet_response_non_leading_gluon.at(eta_index).at(pt_index).Fill(event.truth_jet_paired_respPt->at(ijet), weight);
            }
            ++ngluons;
        }
    }
}

void TruthPlots::FillRecoEfficiencyHistos(const Event& event,
                                          const int& index1,
                                          const int& index2,
                                          const float& weight) {

    m_truth_reco_eff->Fill(1., weight); /// All events

    if (event.index_W_1 < 0 || event.index_W_2 < 0) {
        m_truth_reco_eff->Fill(2., weight); 
        return;
    }
    
    if (event.jet_closest_reco_dR->at(event.index_W_1) < m_critical_reco_dR ||
        event.truth_jet_failed_to_pair_match->at(event.index_W_1) ||
        event.truth_jet_closest_truth_dR->at(event.index_W_1) < m_critical_truth_dR ||
        event.jet_closest_reco_dR->at(event.index_W_2) < m_critical_reco_dR ||
        event.truth_jet_failed_to_pair_match->at(event.index_W_2) ||
        event.truth_jet_closest_truth_dR->at(event.index_W_2) < m_critical_truth_dR) {

        m_truth_reco_eff->Fill(3., weight); /// Cannot identify the correct jet
        return;
    }

    m_truth_reco_eff->Fill(4., weight); /// Can be paired

    if (index1 == event.index_W_1 || index1 == event.index_W_2) { /// first index is correct
        m_truth_reco_eff->Fill(5., weight);
    }
    if (index2 == event.index_W_1 || index2 == event.index_W_2) { /// second index is correct
        m_truth_reco_eff->Fill(6., weight);
    }
    if ((index1 == event.index_W_1 || index1 == event.index_W_2) &&
            (index2 == event.index_W_1 || index2 == event.index_W_2)) { /// both indies are correct
        m_truth_reco_eff->Fill(7., weight);
    }
}
    
void TruthPlots::FillParticlePartonHistos(const Event& event,
                                          const float& weight) {

    if (event.isDilepton) return;
    if (event.index_W_1 < 0) return;
    if (event.index_W_2 < 0) return;
    if (event.truth_jet_failed_to_pair_match->at(event.index_W_1)) return;
    if (event.truth_jet_closest_truth_dR->at(event.index_W_1) < m_critical_truth_dR) return;
    if (event.jet_closest_reco_dR->at(event.index_W_1) < m_critical_reco_dR) return;
    if (event.truth_jet_failed_to_pair_match->at(event.index_W_2)) return;
    if (event.truth_jet_closest_truth_dR->at(event.index_W_2) < m_critical_truth_dR) return;
    if (event.jet_closest_reco_dR->at(event.index_W_2) < m_critical_truth_dR) return;
    if (std::abs(event.jet_truthPartonLabel->at(event.index_W_1)) > 4) return;
    if (std::abs(event.jet_truthPartonLabel->at(event.index_W_2)) > 4) return;
    
    TLorentzVector q1, q2;
    q1.SetPtEtaPhiM(event.W_quark1_pt/1e3,
                    event.W_quark1_eta,
                    event.W_quark1_phi,
                    event.W_quark1_m/1e3);
    q2.SetPtEtaPhiM(event.W_quark2_pt/1e3,
                    event.W_quark2_eta,
                    event.W_quark2_phi,
                    event.W_quark2_m/1e3);
    TLorentzVector j1, j2;
    j1.SetPtEtaPhiE(event.truth_jet_paired_pt->at(event.index_W_1)/1e3,
                    event.truth_jet_paired_eta->at(event.index_W_1),
                    event.truth_jet_paired_phi->at(event.index_W_1),
                    event.truth_jet_paired_e->at(event.index_W_1)/1e3);
    j2.SetPtEtaPhiE(event.truth_jet_paired_pt->at(event.index_W_2)/1e3,
                    event.truth_jet_paired_eta->at(event.index_W_2),
                    event.truth_jet_paired_phi->at(event.index_W_2),
                    event.truth_jet_paired_e->at(event.index_W_2)/1e3);

    m_jet_parton_ratio_pt->Fill(j1.Pt()/q1.Pt(), weight);
    m_jet_parton_ratio_pt->Fill(j2.Pt()/q2.Pt(), weight);
    m_jet_parton_ratio_e->Fill(j1.E()/q1.E(), weight);
    m_jet_parton_ratio_e->Fill(j2.E()/q2.E(), weight);
    m_jet_parton_difference_m->Fill(j1.M() - q1.M(), weight);
    m_jet_parton_difference_m->Fill(j2.M() - q2.M(), weight);
    m_jet_parton_difference_eta->Fill(j1.Eta() - q1.Eta(), weight);
    m_jet_parton_difference_eta->Fill(j2.Eta() - q2.Eta(), weight);
    m_jet_parton_difference_phi->Fill(j1.Phi() - q1.Phi(), weight);
    m_jet_parton_difference_phi->Fill(j2.Phi() - q2.Phi(), weight);
    m_jet_parton_W_difference_pt->Fill((j1+j2).Pt() - (q1+q2).Pt(), weight);
    m_jet_parton_W_ratio_m->Fill((j1+j2).M()/(q1+q2).M(), weight);
    if ((q1+q2).Pt() > 50) {
        m_jet_parton_W_ratio_pt_50->Fill((j1+j2).Pt() / (q1+q2).Pt(), weight);
    }
}
