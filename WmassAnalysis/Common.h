#ifndef COMMON_H_
#define COMMON_H_

#include "WmassAnalysis/SystHistoManager.h"

#include <map>
#include <string>
#include <vector>

class Event;
class TH1D;
class TH2D;

namespace Common {
    enum LEPTONTYPE {
        EL = 0,
        MU = 1,
    };

    void AddOverflow(TH1D* hist);
    
    void AddOverflow(TH2D* hist);

    void NormaliseMatrix(TH2D* mat, bool byRow = true);

    std::size_t IndexFromVec(const float& value,
                             const std::vector<float>& binning,
                             const bool& is_abs);
   
    std::string GetNameFromVec(const float& value,
                               const std::vector<float>& binning,
                               const bool& is_abs);

    std::vector<std::string> GetBinsNames(const std::vector<float>& binning,
                                          const bool& is_pt);

    std::string RemoveSubstrings(const std::string& str, const std::vector<std::string>& remove);

    float GetMax(TH1D* h1, TH1D* h2);

    double GetMean(const std::vector<double>& v);
    
    double GetSigma(const std::vector<double>& v);

    double GetMeanGaus(const std::vector<double>& y, const double& min, const double& max, double& error);
    
    double GetSigmaGaus(const std::vector<double>& y, const double& min, const double& max, double& error);

    std::vector<std::string> GetSystUniqueNames(const std::vector<Systematic>& all);
    
    std::vector<Systematic> GetSystSameName(const std::vector<Systematic>& all,
                                            const std::string& name);

    bool SystematicsAreConsistent(const std::vector<Systematic>& systs);

    int GetSystematicIndex(const std::vector<Systematic>& syst, const std::string& name);

    float CalculateTruthWmass(const Event& event);

    void AddMapUncertainty(std::map<std::string, std::pair<double,double> >& map,
                           const double& up,
                           const double& down,
                           const std::string& name);

    double SumCategories(const std::map<std::string, std::pair<double,double> >& map,
                         const bool& isUp);

    std::string NiceRangeLabels(const std::string& in, const bool isEta);
}

#endif
