#ifndef CONTROLPLOTS_H_
#define CONTROLPLOTS_H_

#include "TH1D.h"

#include <string>
#include <vector>

class Event;
class TFile;

class ControlPlots {

public:
    ControlPlots();

    ~ControlPlots() = default;

    void Init(const std::vector<std::string>& sf_syst_names);

    void FillAllHistos(const Event& event,
                       const std::size_t& index1,
                       const std::size_t& index2,
                       const double& chi2,
                       const bool& isMC,
                       const std::vector<float>& weights);
    
    void Finalise();

    void Write(const std::string& name, TFile* file);

private:

    std::vector<std::string> m_sf_syst_names;
    std::vector<float> m_weights;
    bool m_isMC;

    std::vector<TH1D> m_jet1_pt;
    std::vector<TH1D> m_jet1_eta;
    std::vector<TH1D> m_jet1_phi;
    std::vector<TH1D> m_jet1_jvt;
    std::vector<TH1D> m_jet2_pt;
    std::vector<TH1D> m_jet2_eta;
    std::vector<TH1D> m_jet2_phi;
    std::vector<TH1D> m_jet2_jvt;
    std::vector<TH1D> m_jet3_pt;
    std::vector<TH1D> m_jet3_eta;
    std::vector<TH1D> m_jet3_phi;
    std::vector<TH1D> m_jet3_jvt;
    std::vector<TH1D> m_jet4_pt;
    std::vector<TH1D> m_jet4_eta;
    std::vector<TH1D> m_jet4_phi;
    std::vector<TH1D> m_jet4_jvt;
    std::vector<TH1D> m_jet5_pt;
    std::vector<TH1D> m_jet5_eta;
    std::vector<TH1D> m_jet5_phi;
    std::vector<TH1D> m_jet5_jvt;
    std::vector<TH1D> m_jet6_pt;
    std::vector<TH1D> m_jet6_eta;
    std::vector<TH1D> m_jet6_phi;
    std::vector<TH1D> m_jet6_jvt;
    std::vector<TH1D> m_jet_pt;
    std::vector<TH1D> m_jet_eta;
    std::vector<TH1D> m_jet_phi;
    std::vector<TH1D> m_jet_pt_20_50;
    std::vector<TH1D> m_jet_eta_20_50;
    std::vector<TH1D> m_jet_pt_20_30;
    std::vector<TH1D> m_jet_eta_20_30;
    std::vector<TH1D> m_jet_pt_50;
    std::vector<TH1D> m_jet_eta_50;
    std::vector<TH1D> m_jet_pt_crack;
    std::vector<TH1D> m_jet_eta_crack;
    std::vector<TH1D> m_jet_phi_crack;
    std::vector<TH1D> m_jet_jvt_crack;
    std::vector<TH1D> m_bjet1_pt;
    std::vector<TH1D> m_bjet1_eta;
    std::vector<TH1D> m_bjet1_phi;
    std::vector<TH1D> m_bjet2_pt;
    std::vector<TH1D> m_bjet2_eta;
    std::vector<TH1D> m_bjet2_phi;
    std::vector<TH1D> m_HT;
    std::vector<TH1D> m_jet_n;
    std::vector<TH1D> m_bjet_n;
    std::vector<TH1D> m_jet_el_dR;
    std::vector<TH1D> m_jet_mu_dR;

    std::vector<TH1D> m_el_pt;
    std::vector<TH1D> m_el_eta;
    std::vector<TH1D> m_el_phi;
    std::vector<TH1D> m_mu_pt;
    std::vector<TH1D> m_mu_eta;
    std::vector<TH1D> m_mu_phi;
    std::vector<TH1D> m_met;
    std::vector<TH1D> m_met_phi;
    std::vector<TH1D> m_average_mu;
    std::vector<TH1D> m_actual_mu;
    std::vector<TH1D> m_chi2;
    
    std::vector<TH1D> m_Wjet1_pt;
    std::vector<TH1D> m_Wjet1_eta;
    std::vector<TH1D> m_Wjet1_phi;
    std::vector<TH1D> m_Wjet2_pt;
    std::vector<TH1D> m_Wjet2_eta;
    std::vector<TH1D> m_Wjet2_phi;
    std::vector<TH1D> m_W_pt;
    std::vector<TH1D> m_W_eta;
    std::vector<TH1D> m_W_phi;
    std::vector<TH1D> m_W_m;
    std::vector<TH1D> m_W_m_peak;
    std::vector<TH1D> m_W_m_peak_shifted;
    std::vector<TH1D> m_W_m_peak_60_90;

    void FillJetPlots(const Event& event);
    void FillWbosonPlots(const Event& event, const std::size_t& index1, const std::size_t& index2);
    void FillOtherPlots(const Event& event, const double& chi2);
};

#endif
