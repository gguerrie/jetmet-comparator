#ifndef EXTRACTOR_H_
#define EXTRACTOR_H_

#include "WmassAnalysis/SystHistoManager.h"

#include "ForwardFolding/ForwardFolding/FFFitter.h"

#include "TFile.h"

#include <fstream>
#include <map>
#include <memory>
#include <string>
#include <vector>

class TH2D;

class Extractor {

public:
    explicit Extractor(const std::string& name);
    
    ~Extractor();
    
    void SetTtbarName(const std::string& name);
    
    void SetBackgroundNames(const std::vector<std::string>& names);
    
    void SetSpecialNames(const std::vector<std::string>& names);

    void SetParameterS(const int& steps,
                       const float& min,
                       const float& max);

    void SetParameterR(const int& steps,
                       const float& min,
                       const float& max);
    
    void SetAllSysts(const std::vector<Systematic>& systs);

    void SetFitData(const bool& flag);
    
    void SetUseSyst(const bool& flag);

    void SetNumberOfPE(const int& n);

    void SetNeighbourIndices(const int& n);

    void SetObsName(const std::string& name);

    void SetFixIndexS(const int& index);
    
    void SetFixIndexR(const int& index);

    void ExtractJES();

private:

    struct Param {
        int steps;
        float min;
        float max;
    };

    void OpenRootFiles();

    std::vector<std::vector<TH1D> > GetNominalTemplates(const FFFitter& fitter) const;

    std::unique_ptr<TH1D> GetDataHistogram(const bool& is_asimov) const;

    void CloseRootFiles();

    void AsimovFit(FFFitter& fitter, const std::vector<std::vector<TH1D> >& histos);

    void DataFit(FFFitter& fitter, const std::vector<std::vector<TH1D> >& histos);

    void Make2DPlot(TH2D* curve, const bool& is_asimov) const;

    void ProcessSingleSystematic(FFFitter& fitter,
                                 const std::vector<std::vector<TH1D> >& histos,
                                 const std::string& name,
                                 const double& mean_s,
                                 const double& mean_r);

    std::unique_ptr<TH1D> GetSystShiftedHist(const std::vector<Systematic>& systs,
                                             const bool& is_up,
                                             const bool& use_ref) const;

    std::string m_name;
    bool m_fit_data;
    bool m_use_syst;
    int m_PE_number;
    std::string m_ttbar_name;
    int m_neighbour_indices;
    std::string m_obs_name;
    int m_fix_s_index;
    int m_fix_r_index;
    std::vector<std::string> m_background_names;
    std::vector<std::string> m_special_names;

    Param m_param_s;
    Param m_param_r;

    std::unique_ptr<TFile> m_ttbar_file;
    std::unique_ptr<TFile> m_data_file;
    std::vector<std::unique_ptr<TFile> > m_background_files;
    std::vector<std::unique_ptr<TFile> > m_special_files;

    std::vector<Systematic> m_systematics;

    std::map<std::string, std::pair<double,double> > m_all_syst_map_s;
    std::map<std::string, std::pair<double,double> > m_all_syst_map_r;

    std::map<std::string, std::pair<double,double> > m_category_syst_map_s;
    std::map<std::string, std::pair<double,double> > m_category_syst_map_r;

    std::unique_ptr<std::ofstream> m_result_file_s;
    std::unique_ptr<std::ofstream> m_result_file_r;

};

#endif
