#ifndef FILEPROCESSOR_H_
#define FILEPROCESSOR_H_

#include "WmassAnalysis/Matcher.h"

#include <string>
#include <vector>

class FileProcessor {

public:
    FileProcessor(const std::string& name,
                  const std::string& dataset_type,
                  const std::string& systematics);

    ~FileProcessor() = default;

    void ReadConfig();

    void SetCriticalRecoDR(const float& dR);
    
    void SetCriticalTruthDR(const float& dR);

    void SetLeadingWjetCuts(const float& min, const float& max);
    
    void SetSubLeadingWjetCuts(const float& min, const float& max);
    
    void SetLeadingWjetEtaCuts(const float& min, const float& max);
    
    void SetSubLeadingWjetEtaCuts(const float& min, const float& max);
    
    void SetAverageWjetCuts(const float& min, const float& max);
    
    void SetNpvCuts(const int& min, const int& max);
    
    void SetMuCuts(const float& min, const float& max);
    
    void SetRunNumberCuts(const unsigned int& min, const unsigned int& max);

    void SetCreateHerwigCorrection(const bool& flag);

    void ProcessAllFiles(const Chi2Params& par, const double& chi2_cut);

private:
    std::string m_name;
    std::string m_dataset_type;
    std::string m_systematics;
    std::vector<std::string> m_process;
    std::vector<std::string> m_syst_list;
    std::vector<std::string> m_sfsyst_list;
    std::vector<std::string> m_file_path;
    std::vector<std::string> m_file_type;
    std::vector<std::string> m_af2;
    std::vector<std::string> m_campaign;
    std::vector<int> m_dsid;
    std::vector<float> m_eta_bins;
    std::vector<float> m_pt_bins;
    float m_leading_pt_min;
    float m_leading_pt_max;
    float m_subleading_pt_min;
    float m_subleading_pt_max;
    float m_leading_eta_min;
    float m_leading_eta_max;
    float m_subleading_eta_min;
    float m_subleading_eta_max;
    float m_average_pt_min;
    float m_average_pt_max;
    int m_npv_min;
    int m_npv_max;
    float m_mu_min;
    float m_mu_max;
    unsigned int m_runNumber_min;
    unsigned int m_runNumber_max;
    bool m_createHerwigCorrection;
    float m_critical_reco_dR;
    float m_critical_truth_dR;

    void ReadFileList(const std::string& path);

    std::vector<std::string> ReadSystList() const;

    void CreateOutputFiles(const std::string& path) const;

    void RenameOutputFiles(const std::string& path) const;

    void CreateHerwigCorrection() const;
};

#endif
