#ifndef FOLDINGPLOTS_H_
#define FOLDINGPLOTS_H_

#include "TH1D.h"

#include <memory>
#include <string>
#include <vector>

class Event;
class Folder;
class TFile;

class FoldingPlots {

typedef std::vector<std::vector<TH1D> > smearVector;

public:
    FoldingPlots();

    ~FoldingPlots() = default;

    void Init(const std::pair<int,int>& sizes);

    void SetCriticalTruthDR(const float& dR);

    void FillAllHistos(const Event& event,
                       const Folder& folder,
                       const int& index1,
                       const int& index2,
                       const float& weight);

    void Finalise();

    void Write(TFile* file);

private:

    float m_critical_truth_dR;
    
    int m_size_s;
    int m_size_r;

    std::unique_ptr<TH1D> m_jet1_impossible;
    std::unique_ptr<TH1D> m_jet2_impossible;
    std::unique_ptr<TH1D> m_w_m_impossible;
    std::unique_ptr<TH1D> m_w_m_peak_impossible;
    std::unique_ptr<TH1D> m_w_m_peak_shifted_impossible;
    std::unique_ptr<TH1D> m_w_m_peak_60_90_impossible;
    std::unique_ptr<TH1D> m_jet1_possible;
    std::unique_ptr<TH1D> m_jet2_possible;
    std::unique_ptr<TH1D> m_w_m_possible;
    std::unique_ptr<TH1D> m_w_m_peak_possible;
    std::unique_ptr<TH1D> m_w_m_peak_shifted_possible;
    std::unique_ptr<TH1D> m_w_m_peak_60_90_possible;

    smearVector m_jet1_smear;
    smearVector m_jet2_smear;
    smearVector m_w_m_smear;
    smearVector m_w_m_peak_smear;
    smearVector m_w_m_peak_shifted_smear;
    smearVector m_w_m_peak_60_90_smear;
};

#endif
