#ifndef HISTOMAKER_H_
#define HISTOMAKER_H_

#include "WmassAnalysis/ControlPlots.h"
#include "WmassAnalysis/Common.h"
#include "WmassAnalysis/FoldingPlots.h"
#include "WmassAnalysis/Matcher.h"
#include "WmassAnalysis/TruthPlots.h"

#include "ForwardFolding/ForwardFolding/Folder.h"

#include "FakeLeptons/FakeBkgTools/FakeBkgTools/AsymptMatrixTool.h"

#include <memory>
#include <string>
#include <vector>

class Event;
class WeightManager;

class HistoMaker {

public:
    explicit HistoMaker(const std::string& type,
                        const std::string& systematics,
                        const std::vector<float>& eta_bins,
                        const std::vector<float>& pt_bins,
                        const std::vector<std::string>& sfsyst_list,
                        const std::pair<int,int>& folding_sizes);

    void SetCurrentSyst(const std::string& syst){m_current_syst = syst;}
    void SetIsMC(const bool& isMC){m_isMC = isMC;}
    void SetChi2Params(const Chi2Params& par);
    void SetChi2Cut(const double& chi2_cut);

    void SetCriticalRecoDR(const float& dR);
    void SetCriticalTruthDR(const float& dR);
    void SetLeadingWjetCuts(const float& min, const float& max);
    void SetSubLeadingWjetCuts(const float& min, const float& max);
    void SetLeadingWjetEtaCuts(const float& min, const float& max);
    void SetSubLeadingWjetEtaCuts(const float& min, const float& max);
    void SetAverageWjetCuts(const float& min, const float& max);
    void SetNpvCuts(const int& min, const int& max);
    void SetMuCuts(const float& min, const float& max);
    void SetRunNumberCuts(const unsigned int& min, const unsigned int& max);
    
    void FillHistos(const WeightManager& wei_manager,
                    const Folder& folder,
                    const std::string& tree_name,
                    const std::string& path);
    void Finalise();
    void WriteHistosToFile(const std::string& path, const std::string& name);

private:

    float m_critical_reco_dR;
    float m_critical_truth_dR;
    std::string m_type;
    std::string m_systematics;
    std::string m_current_syst;
    bool m_isMC;
    bool m_isTT;
    bool m_isPP8;
    bool m_isNominal;
    std::string m_tree_name;
    double m_chi2_cut;
    float m_leading_pt_min;
    float m_leading_pt_max;
    float m_subleading_pt_min;
    float m_subleading_pt_max;
    float m_leading_eta_min;
    float m_leading_eta_max;
    float m_subleading_eta_min;
    float m_subleading_eta_max;
    float m_average_pt_min;
    float m_average_pt_max;
    int m_npv_min;
    int m_npv_max;
    float m_mu_min;
    float m_mu_max;
    unsigned int m_runNumber_min;
    unsigned int m_runNumber_max;
    bool m_applyCorrection;
    std::vector<std::string> m_sfsyst_list;
    std::vector<float> m_eta_bins;
    std::vector<float> m_pt_bins;
    Common::LEPTONTYPE m_lepton_type;

    Matcher m_matcher;

    std::unique_ptr<ControlPlots> m_control_plots;
    std::unique_ptr<FoldingPlots> m_folding_plots;
    std::unique_ptr<TruthPlots> m_truth_plots;
    
    std::unique_ptr<CP::AsymptMatrixTool> m_IFFtools_el;
    std::unique_ptr<CP::AsymptMatrixTool> m_IFFtools_el_alt;
    std::unique_ptr<CP::AsymptMatrixTool> m_IFFtools_mu;
    std::unique_ptr<CP::AsymptMatrixTool> m_IFFtools_mu_alt;

    std::vector<float> GetMMweight(const Event& event) const;
};

#endif
