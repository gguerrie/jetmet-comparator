#ifndef MATCHER_H_
#define MATCHER_H_

#include "WmassAnalysis/Event.h"

struct Chi2Params {
    double mass_w;
    double mass_t;
    double width_w;
    double width_t;
};

class Matcher {

public:
    Matcher();
    
    ~Matcher() = default;

    void SetChi2Params(const Chi2Params& par);

    bool ProcessEvent(const Event& event);

    const double& GetBestChi2() const;
    
    std::pair<std::size_t, std::size_t> GetIndices(const Event& event) const;

    void RunSinglePermutation(const Event& event, const int bIndex, const int lIndex1, const int lIndex2);

private:
    double m_mass_w;
    double m_mass_t;
    double m_width_w;
    double m_width_t;
    double m_chi2;
    std::size_t m_index1;
    std::size_t m_index2;
    std::size_t m_bIndex;

    double Chi2(const double& reco_mass_w, const double& reco_mass_t) const;
};

#endif 
