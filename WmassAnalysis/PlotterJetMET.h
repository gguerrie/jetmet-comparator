#ifndef PLOTTERJETMET_H_
#define PLOTTERJETMET_H_

#include "WmassAnalysis/SystHistoManager.h"

#include "TFile.h"

#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

class TGraphAsymmErrors;
class TH1D;
class TPad;

class PlotterJetMET {

public:
    explicit PlotterJetMET(const std::string& directory20, const std::string& directory21);

    ~PlotterJetMET() = default;

    void SetTTbarNames(const std::vector<std::string>& names);
    
    void SetBackgroundNames(const std::vector<std::string>& names);

    void SetSpecialNames(const std::vector<std::string>& names);

    void SetDataName(const std::string& name);

    void SetAtlasLabel(const std::string& label);
    
    void SetLumiLabel(const std::string& label);
    
    void SetEtaPtBins(const std::vector<float>& eta_bins, const std::vector<float>& pt_bins);
    
    void SetCollection(const std::string& collection);
    
    void SetNominalTTbarName(const std::string& name);

    const std::string& GetNominalTTbarName() const {return m_nominal_ttbar_name;}

    void SetSystShapeOnly(const bool& flag) {m_syst_shape_only = flag;}

    void SetAllSysts(const std::vector<Systematic>& systs) {m_systematics = systs;}

    void SetRunSyst(const bool& flag) {m_run_syst = flag;}

    void PlotResponsePlots(const std::string& name, const std::string& axis, const bool& normalise) const;

    void CalculateYields() const;

    void OpenRootFiles();

    void CloseRootFiles();

private:
    std::string m_directory;
    std::string m_directory20;
    std::string m_directory21;

    std::vector<std::string> m_ttbar_names;
    std::vector<std::string> m_background_names;
    std::vector<std::string> m_special_names;
    std::vector<std::string> m_background_names_with_impossible;
    std::string m_data_name;

    std::vector<std::unique_ptr<TFile> > m_ttbar_files;
    std::vector<std::unique_ptr<TFile> > m_background_files;
    std::vector<std::unique_ptr<TFile> > m_special_files;
    std::unique_ptr<TFile> m_data_file;

    std::vector<std::pair<int,int> > m_styles;

    std::string m_atlas_label;
    std::string m_lumi_label;
    std::string m_collection;
    std::string m_nominal_ttbar_name;
    std::size_t m_nominal_ttbar_index;
    bool m_syst_shape_only;
    bool m_run_syst;
    std::vector<float> m_eta_bins;
    std::vector<float> m_pt_bins;
    std::vector<std::string> m_eta_names;
    std::vector<std::string> m_pt_names;
    std::map<const std::string, int> m_colour_map;
    std::map<const std::string, std::string> m_label_map;
    std::vector<Systematic> m_systematics;

    void PlotUpperComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                             TPad* pad,
                             const std::string& axis) const;
    
    void PlotRatioComparison(const std::vector<std::unique_ptr<TH1D> >& histos,
                             TPad* pad,
                             const std::string& axis,
                             const float& range,
                             const bool& force) const;
    
    void FillStyleMap();

    float MaxResize(const std::vector<std::unique_ptr<TH1D> >& histos, const bool& is_log) const;

    void DrawLabels(TPad *pad, const float& x, const float& y, const bool& add_lumi) const;

    void SetColourMap();
    
    void SetLabelMap();

    void GetTotalUpDownUncertainty(TH1D* total_up, TH1D* total_down, const std::string& name) const;

    void AddSingleSyst(TH1D* total_up, TH1D* total_down, const std::string& name, const Systematic& systematic) const;

    std::unique_ptr<TH1D> GetHistoFromAll(const std::string& file_name, const std::string& histo_name) const;

    void AddHistosInSquares(TH1D* total_up, TH1D* total_down, TH1D* histo_up, TH1D* histo_down, TH1D* histo_nominal) const;

    void TransformErrorHistoToTGraph(TGraphAsymmErrors* error, TH1D* up, TH1D* down) const;
};

#endif
