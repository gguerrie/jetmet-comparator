#ifndef TRUTHPLOTS_H_
#define TRUTHPLOTS_H_

#include "TH1D.h"
#include <memory>
#include <string>
#include <vector>

class Event;
class TFile;

class TruthPlots {

public:
    TruthPlots();

    ~TruthPlots() = default;

    void Init(const std::vector<float>& eta_bins,
              const std::vector<float>& pt_bins);

    void SetCriticalRecoDR(const float& dR);
    
    void SetCriticalTruthDR(const float& dR);

    void FillAllHistos(const Event& event,
                       const int& index1,
                       const int& index2,
                       const float& weight);

    void Finalise();

    void Write(TFile *file);

private:

    float m_critical_reco_dR;
    float m_critical_truth_dR;

    std::vector<float> m_eta_bins;
    std::vector<float> m_pt_bins;

    std::unique_ptr<TH1D> m_truth_W_pt;
    std::unique_ptr<TH1D> m_truth_W_eta;
    std::unique_ptr<TH1D> m_truth_W_m;
    std::unique_ptr<TH1D> m_reco_truth_ratio_W_m;
    std::unique_ptr<TH1D> m_truth_W_jets_dR;
    std::unique_ptr<TH1D> m_truth_W_jets_vec_sum_pt;
    std::unique_ptr<TH1D> m_truth_W_jets_scalar_sum_pt;
    std::unique_ptr<TH1D> m_truth_quark_W_pt;
    std::unique_ptr<TH1D> m_truth_quark_W_eta;
    std::unique_ptr<TH1D> m_truth_quark_W_m;
    std::unique_ptr<TH1D> m_truth_parton_lepton_W_pt;
    std::unique_ptr<TH1D> m_truth_parton_lepton_W_eta;
    std::unique_ptr<TH1D> m_truth_parton_lepton_W_m;
    std::unique_ptr<TH1D> m_truth_identified_W_pt;
    std::unique_ptr<TH1D> m_truth_identified_W_eta;
    std::unique_ptr<TH1D> m_truth_identified_W_m;
    std::unique_ptr<TH1D> m_truth_reco_eff;

    std::vector<std::vector<TH1D> > m_jet_response;
    std::vector<std::vector<TH1D> > m_jet_response_fifth_jet;
    std::vector<std::vector<TH1D> > m_jet_response_gluon;
    std::vector<std::vector<TH1D> > m_jet_response_leading_gluon;
    std::vector<std::vector<TH1D> > m_jet_response_non_leading_gluon;
    std::vector<std::vector<TH1D> > m_jet_response_light;
    std::vector<std::vector<TH1D> > m_jet_response_s;
    std::vector<std::vector<TH1D> > m_jet_response_c;
    std::vector<std::vector<TH1D> > m_jet_response_b;
    std::vector<std::vector<TH1D> > m_jet_response_from_W;
    std::vector<std::vector<TH1D> > m_jet_response_from_W_light;
    std::vector<std::vector<TH1D> > m_jet_response_from_W_s;
    std::vector<std::vector<TH1D> > m_jet_response_from_W_c;
    
    std::unique_ptr<TH1D> m_jet_parton_ratio_pt;
    std::unique_ptr<TH1D> m_jet_parton_ratio_e;
    std::unique_ptr<TH1D> m_jet_parton_difference_m;
    std::unique_ptr<TH1D> m_jet_parton_difference_eta;
    std::unique_ptr<TH1D> m_jet_parton_difference_phi;
    std::unique_ptr<TH1D> m_jet_parton_W_difference_pt;
    std::unique_ptr<TH1D> m_jet_parton_W_ratio_pt_50;
    std::unique_ptr<TH1D> m_jet_parton_W_ratio_m;
    
    void FillInclusiveHistos(const Event& event,
                             const int& index1,
                             const int& index2,
                             const float& weight);
    
    void FillParticlePartonHistos(const Event& event,
                                  const float& weight);
    
    void FillExclusiveHistos(const Event& event,
                             const float& weight);
    
    void FillRecoEfficiencyHistos(const Event& event,
                                  const int& index1,
                                  const int& index2,
                                  const float& weight);
    
};

#endif
