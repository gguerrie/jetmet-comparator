#ifndef WEIGHTMANAGER_H_
#define WEIGHTMANAGER_H_

#include "WmassAnalysis/SfSyst.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

class Event;
class TFile;
class TH1D;

class WeightManager {

public:
    struct Weight {
        std::vector<float> sumW;
        std::vector<std::string> names;
        std::vector<std::size_t> indices;
    };
    
    WeightManager();

    ~WeightManager() = default;

    bool ReadFile(const std::string& path);
    
    void SetDsidAf2CampaignType(const int& dsid,
                                const std::string& af2,
                                const std::string& campaign,
                                const std::string& type);

    void SetXsecLumi(const float& xSec, const float& lumi);

    float GetNominalWeight(const Event& event) const;

    std::vector<float> GetSfSystWeights(const Event& event,
                                        const bool& applyCorrection,
                                        const float& truth,
                                        const std::string& name) const;
    
    void ProcessNominalNormalisation();

    void ProcessModellingNormalisation();

    std::vector<std::string> GetSfSystList(const std::string& systematics,
                                           const std::string& type) const;

    void ReadCorrectionFile();

    void Clear();

private:

    int m_dsid;
    std::string m_af2;
    std::string m_campaign;
    float m_xSec;
    float m_lumi;
    float m_nominal_normalisation;
    std::string m_type;
    std::vector<float> m_model_normalisation;

    std::vector<SFSYSTENUM> m_sf_enum;

    std::map<const std::string, std::string> m_weight_buggy;

    std::map<const std::string, Weight> m_weight_map;
    
    std::map<const std::string, std::vector<std::string> > m_model_sf_names;

    std::unique_ptr<TFile> m_correction;
    
    std::unique_ptr<TH1D> m_herwig_713;
    std::unique_ptr<TH1D> m_amcnlo_herwig;
    std::unique_ptr<TH1D> m_amcnlo_pythia;

    void InitialiseModelNames();
    
    Weight FindWeight() const;
    
    double GetNominalSumWeight() const;

    std::vector<std::size_t> GetModelIndices(const std::vector<std::string>& names) const;

    std::vector<std::size_t> GetModelIndicesFromBuggyPythia(const std::vector<std::string>& names) const;
    float GetCorrection(const float& nominal, const float& truth, const std::string& name) const;
};

#endif
