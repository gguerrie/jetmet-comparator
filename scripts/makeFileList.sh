#!/bin/bash

# THIS SCRIPT PRODUCES A LIST OF ROOT FILES WITH METADATA: SAMPLE TYPE, TOTAL SUM OF WEIGHTS FOR GIVEN SAMPLE

#SAMPLEDIR='/home/dado2/lhcdata_13TeV/Wmass/PFlow/'
SAMPLEDIR='../test/input_files/'
OUTPUTFILELIST="file_list.txt"
OUTPUTSUMW="sumW.txt"

if [[ ! -f $(basename $0) ]]; then
    echo "Run this script from the dir where it lives" >&2
    exit 1
fi

WRKDIR=$PWD

echo "Going to create filelist out of samples in directory:"
echo "'${SAMPLEDIR}'"
echo "Filelist name: '${OUTPUTFILELIST}'"

if [ -f $OUTPUT ]; then
    echo "Filelist with the specified name already exists! Overwrite ?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes ) truncate -s 0 $OUTPUTFILELIST; break;;
            No ) exit;;
        esac
    done
else
    touch $OUTPUT
fi

FILELISTPATH="${WRKDIR}/"${OUTPUTFILELIST}
SUMWPATH="${WRKDIR}/"${OUTPUTSUMW}

if [ -d ${SAMPLEDIR} ]; then
    cd $SAMPLEDIR
    find `pwd` -type f -name "*.root*" >> $FILELISTPATH
else
    echo "ERROR: Directory '${SAMPLEDIR}' doesn't exist! Aborting."
    exit 1
fi

cd $WRKDIR

# pre-process the files in the filelist and create final filelist with sample types and sumOfWeights
#echo "GETTING SAMPLES' METADATA (SUM OF WEIGHTS & SAMPLE TYPE)"
mv "$FILELISTPATH" "${FILELISTPATH}_temp"
./makeFileListMetadata.py "${FILELISTPATH}_temp" "$FILELISTPATH"
./makeSumWeights.py "${FILELISTPATH}_temp" "$SUMWPATH"
rm "${FILELISTPATH}_temp"
