#!/bin/bash

setupATLAS

lsetup "views LCG_102 x86_64-centos7-gcc11-opt"

if [ -d build ]
then
    echo ""
else
    mkdir build
fi

cd build
cmake ../
make -j4

cd ..
