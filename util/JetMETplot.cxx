#include "WmassAnalysis/PlotterJetMET.h"
#include "WmassAnalysis/SystHistoManager.h"

#include "TError.h"

#include <iostream>
#include <string>
#include <vector>

int main (int argc, char *argv[]) {
    if (argc != 2 && argc != 3) {
        std::cerr << "Expecting one or two arguments but " << argc-1 << " arguments provided" << std::endl;
        exit(EXIT_FAILURE);  
    }

    const std::string folder20 = argv[1];
    const std::string folder21 = argv[2];
    bool use_syst(false);
    std::string syst_param = "";

    gErrorIgnoreLevel = kError;

    PlotterJetMET plotter(folder20, folder21);

    static const std::vector<std::string> ttbar_names = {"ttbar_PP8_FS"
                                                         //"ttbar_PH7_1_3_AFII",
                                                         //"ttbar_PP8_hdamp_AFII",
                                                         //"ttbar_aMCNLO_Pythia8_AFII"
                                                        };
    
    static const std::vector<std::string> bkg_names = {"SingleTop_PP8_s_chan_FS",
                                                       "SingleTop_PP8_t_chan_FS",
                                                       "SingleTop_PP8_tW_chan_FS",
                                                       "Wjets_Sherpa_FS",
                                                       "Zjets_Sherpa_FS",
                                                       "Diboson_Sherpa_FS",
                                                       "ttV_aMCNLO_Pythia8_FS",
                                                       "ttH_PP8_FS",
                                                       "Multijet"};

    static const std::vector<float> eta_bins = {0.0,1.0,2.5};
    static const std::vector<float> pt_bins = {0.0,30.0,55.0,110.0,200.0,400.0,800.0,1500.0};
    
    static const std::vector<std::string> special_names = {"ttbar_PP8_AFII",
                                                           "SingleTop_PP8_s_chan_AFII",
                                                           "SingleTop_PP8_t_chan_AFII",
                                                           "SingleTop_PP8_tW_chan_AFII",
                                                           "ttbar_PP8_hdamp_AFII",
                                                           "ttbar_PP8_shower_decor_AFII",
                                                           "SingleTop_DS_PP8_tW_chan_FS",
                                                           "SingleTop_PH7_0_4_s_chan_AFII",
                                                           "SingleTop_PH7_0_4_t_chan_AFII",
                                                           "SingleTop_PH7_0_4_tW_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_s_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_t_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_tW_chan_AFII"
                                                           };

    plotter.SetTTbarNames(ttbar_names);
    plotter.SetDataName("Data");
    plotter.SetAtlasLabel("Internal");
    plotter.SetLumiLabel("139");
    plotter.SetCollection("pflow");
    plotter.SetNominalTTbarName("ttbar_PP8_FS");
    plotter.SetSystShapeOnly(false);
    
    plotter.OpenRootFiles();
    plotter.SetEtaPtBins(eta_bins, pt_bins);

    /// Set all systematics
    SystHistoManager syst_manager{};
    
    std::vector<std::string> all(bkg_names);
    all.emplace_back(plotter.GetNominalTTbarName());

    /// Set tree systematics
    for (const auto& ifile : all) {
        if (ifile == "Multijet") continue;
        for (int ib = 0; ib < 9; ++ib) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_B_"+std::to_string(ib)+"_up", "BTAG_B_"+std::to_string(ib)+"_down", "", "nominal", ifile, "", "");
        }
        for (int ic = 0; ic < 5; ++ic) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_C_"+std::to_string(ic)+"_up", "BTAG_C_"+std::to_string(ic)+"_down", "", "nominal", ifile, "", "");
        }
        for (int il = 0; il < 5; ++il) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Light_"+std::to_string(il)+"_up", "BTAG_Light_"+std::to_string(il)+"_down", "", "nominal", ifile, "", "");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "el_trigger_up", "el_trigger_down", "", "nominal", ifile, "","");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_reco_up", "el_reco_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_id_up", "el_id_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_isol_up", "el_isol_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_stat_up", "mu_trigger_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_syst_up", "mu_trigger_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_up", "mu_id_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_up", "mu_id_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_lowpt_up", "mu_id_stat_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_lowpt_up", "mu_id_syst_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_stat_up", "mu_isol_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_syst_up", "mu_isol_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_stat_up", "mu_ttva_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_syst_up", "mu_ttva_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "jvt_up", "jvt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_from_charm_up", "BTAG_extrapolation_from_charm_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_up", "BTAG_extrapolation_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "pileup_up", "pileup_down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_ALL__1up", "EG_RESOLUTION_ALL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_ALL__1up", "EG_SCALE_ALL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MET_SoftTrk_Scale__1up", "MET_SoftTrk_Scale__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_CB__1up", "MUON_CB__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_DATASTAT__1up", "MUON_SAGITTA_DATASTAT__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_RESBIAS__1up", "MUON_SAGITTA_RESBIAS__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SCALE__1up", "MUON_SCALE__1down", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPara", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPerp", "", "nominal", ifile, "", "");
        
        /// Luminosity
        syst_manager.AddNormSyst(ifile, 0.017, 0.017, "");

        /// MC stat
        syst_manager.AddMCstatSyst(ifile);
    }

    /// Add individual systematics
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_aMCNLO_Herwig7_1_3_AFII", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "muR10muF20", "muR10muF05", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "muR20muF10", "muR05muF10", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddOneSidedSyst("ttbar_PP8_hdamp_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_FS", "ttbar_Hdamp", "ttbar_Hdamp");

    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_s_chan_AFII","nominal","SingleTop_PP8_s_chan_AFII", "nominal", "SingleTop_PP8_s_chan_FS", "", "");
    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_t_chan_AFII","nominal","SingleTop_PP8_t_chan_AFII", "nominal", "SingleTop_PP8_t_chan_FS", "", "");
    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_tW_chan_AFII","nominal","SingleTop_PP8_tW_chan_AFII", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");
    syst_manager.AddOneSidedSyst("SingleTop_DS_PP8_tW_chan_FS","nominal","SingleTop_PP8_tW_chan_FS", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");
    
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    
    /// PDF
    for (int i = 90901; i <= 90930; ++i) {
        const std::string pdfname = "PDFset"+std::to_string(i);
        syst_manager.AddOneSidedSyst("ttbar_PP8_FS", pdfname, "ttbar_PP8_FS", "PDFset90900", "ttbar_PP8_FS", "", "");
    }

    syst_manager.AddNormSyst("ttbar_PP8_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_s_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_t_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_tW_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("Wjets_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Zjets_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Diboson_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("ttV_aMCNLO_Pythia8_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("ttH_PP8_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Multijet", 0.5, 0.5, "");
    
    /// Pass all systematics
    plotter.SetAllSysts(syst_manager.GetAllSystematics());
    
    /// 1D response plots (binned in eta and pt)
    plotter.PlotResponsePlots("jet_response", "reco all jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_fifth_jet", "reco fifth jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_gluon", "reco gluon jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_leading_gluon", "leading reco gluon jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_non_leading_gluon", "non-leading reco gluon jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_light", "reco u/d-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_s", "reco s-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_c", "reco c-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_b", "reco b-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W", "reco all jet from W p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W_light", "reco u/d-quark jet from W p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W_s", "reco s-quark jet from W p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W_c", "reco c-quark jet from W p_{T}/truth jet p_{T} [-]", true);

    plotter.CloseRootFiles();

    /// Successful run
    return 0;
}
