#include "WmassAnalysis/Comparator.h"

#include "TError.h"

#include <iostream>

void PrintUsage() {
    std::cout << "You need to pass 6 arguments in this order: \n";
    std::cout << "1. Folder name of the first histo\n";
    std::cout << "2. File name of the first histo\n";
    std::cout << "3. Histo name of the first histo\n";
    std::cout << "4. Folder name of the second histo\n";
    std::cout << "5. File name of the second histo\n";
    std::cout << "6. Histo name of the second histo\n";
}

int main (int argc, char *argv[]) {
    if (argc != 7) {
        PrintUsage();
        exit(EXIT_FAILURE);
    }

    const std::string folder1 = argv[1];
    const std::string file1   = argv[2];
    const std::string name1   = argv[3];
    const std::string folder2 = argv[4];
    const std::string file2   = argv[5];
    const std::string name2   = argv[6];

    gErrorIgnoreLevel = kError;
    
    Comparator comparator{};

    comparator.SetFirstHisto(folder1, file1, name1);
    comparator.SetSecondHisto(folder2, file2, name2);
    comparator.SetNormalise(false);

    comparator.OpenRootFiles();
    
    comparator.GetHistoList();
    
    comparator.PlotAllHistos();
    
    comparator.CloseRootFiles();
}
