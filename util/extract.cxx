#include "WmassAnalysis/Extractor.h"
#include "WmassAnalysis/SystHistoManager.h"

#include "TError.h"

#include <iostream>
#include <string>

int main(int argc, char** argv) {
    
    if (argc < 2 || argc > 3) {
        std::cerr << "Expected one or two parameters but " << argc -1 << " parameters provided" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    gErrorIgnoreLevel = kError;

    const std::string name(argv[1]);
    bool run_syst(false);
    if (argc == 3) {
        const std::string s = argv[2];
        if (s == "syst") run_syst = true;
    }

    static const std::vector<std::string> bkg_names = {"SingleTop_PP8_s_chan_FS",
                                                       "SingleTop_PP8_t_chan_FS",
                                                       "SingleTop_PP8_tW_chan_FS",
                                                       "Wjets_Sherpa_FS",
                                                       "Zjets_Sherpa_FS",
                                                       "Diboson_Sherpa_FS",
                                                       "ttV_aMCNLO_Pythia8_FS",
                                                       "ttH_PP8_FS",
                                                       "Multijet"};

    static const std::vector<std::string> special_names = {"ttbar_PP8_AFII",
                                                           "SingleTop_PP8_s_chan_AFII",
                                                           "SingleTop_PP8_t_chan_AFII",
                                                           "SingleTop_PP8_tW_chan_AFII",
                                                           "ttbar_PP8_hdamp_AFII",
                                                           "ttbar_PP8_shower_decor_AFII",
                                                           "ttbar_PH7_1_3_AFII",
                                                           "ttbar_PP8_mass_173_AFII",
                                                           "ttbar_PP8_hdamp_AFII",
                                                           "ttbar_aMCNLO_Herwig7_1_3_AFII",
                                                           "SingleTop_DS_PP8_tW_chan_FS",
                                                           "SingleTop_PH7_0_4_s_chan_AFII",
                                                           "SingleTop_PH7_0_4_t_chan_AFII",
                                                           "SingleTop_PH7_0_4_tW_chan_AFII",
                                                           };
    
    SystHistoManager syst_manager{};
    std::vector<std::string> all(bkg_names);
    all.emplace_back("ttbar_PP8_FS");
    /// Set tree systematics
    for (const auto& ifile : all) {
        if (ifile == "Multijet") continue;
        for (int ib = 0; ib < 9; ++ib) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_B_"+std::to_string(ib)+"_up", "BTAG_B_"+std::to_string(ib)+"_down", "", "nominal", ifile, "Btag_B_"+std::to_string(ib), "BTag");
        }
        for (int ic = 0; ic < 5; ++ic) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_C_"+std::to_string(ic)+"_up", "BTAG_C_"+std::to_string(ic)+"_down", "", "nominal", ifile, "Btag_C_"+std::to_string(ic), "BTag");
        }
        for (int il = 0; il < 5; ++il) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Light_"+std::to_string(il)+"_up", "BTAG_Light_"+std::to_string(il)+"_down", "", "nominal", ifile, "Btag_L_"+std::to_string(il), "BTag");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "el_trigger_up", "el_trigger_down", "", "nominal", ifile, "El_trig","Electron");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_reco_up", "el_reco_down", "", "nominal", ifile, "El_reco", "Electron");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_id_up", "el_id_down", "", "nominal", ifile, "El_id", "Electron");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_isol_up", "el_isol_down", "", "nominal", ifile, "El_Iso", "Electron");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_stat_up", "mu_trigger_stat_down", "", "nominal", ifile, "Mu_trig_stat", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_syst_up", "mu_trigger_syst_down", "", "nominal", ifile, "Mu_trig_syst", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_up", "mu_id_stat_down", "", "nominal", ifile, "Mu_id_stat", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_up", "mu_id_syst_down", "", "nominal", ifile, "Mu_id_syst", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_lowpt_up", "mu_id_stat_lowpt_down", "", "nominal", ifile, "Mu_id_stat_lowpt", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_lowpt_up", "mu_id_syst_lowpt_down", "", "nominal", ifile, "Mu_id_syst_lowpt", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_stat_up", "mu_isol_stat_down", "", "nominal", ifile, "Mu_isol_stat", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_syst_up", "mu_isol_syst_down", "", "nominal", ifile, "Mu_isol_syst", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_stat_up", "mu_ttva_stat_down", "", "nominal", ifile, "Mu_ttva_stat", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_syst_up", "mu_ttva_syst_down", "", "nominal", ifile, "Mu_ttva_syst", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "jvt_up", "jvt_down", "", "nominal", ifile, "Jvt", "JVT");
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_from_charm_up", "BTAG_extrapolation_from_charm_down", "", "nominal", ifile, "Btag_c_pt_extrapolation", "BTag");
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_up", "BTAG_extrapolation_down", "", "nominal", ifile, "Btag_pt_extrapolation", "BTag");
        syst_manager.AddTwoSidedSyst(ifile, "", "pileup_up", "pileup_down", "", "nominal", ifile, "Pileup", "Pileup");
        
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_ALL__1up", "EG_RESOLUTION_ALL__1down", "", "nominal", ifile, "EG_RESOLUTION_ALL", "Electron");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_ALL__1up", "EG_SCALE_ALL__1down", "", "nominal", ifile, "EG_SCALE_ALL", "Electron");
        syst_manager.AddTwoSidedSyst(ifile, "", "MET_SoftTrk_Scale__1up", "MET_SoftTrk_Scale__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_CB__1up", "MUON_CB__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_DATASTAT__1up", "MUON_SAGITTA_DATASTAT__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_RESBIAS__1up", "MUON_SAGITTA_RESBIAS__1down", "", "nominal", ifile, "MUON_SAGITTA_RESBIAS", "Muon");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SCALE__1up", "MUON_SCALE__1down", "", "nominal", ifile, "MUON_SCALE", "Muon");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPara", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPerp", "", "nominal", ifile, "", "");

        /// Luminosity
        syst_manager.AddNormSyst(ifile, 0.017, 0.017, "Luminosity");
    }

    /// Add individual systematics
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_aMCNLO_Herwig7_1_3_AFII", "nominal", "ttbar_PP8_FS", "ttbar_ME", "ttbar_ME");
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_FS", "ttbar_PS", "ttbar_PS");
    syst_manager.AddOneSidedSyst("ttbar_PP8_hdamp_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_FS", "ttbar_Hdamp", "ttbar_Hdamp");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "muR10muF20", "muR10muF05", "", "nominal", "ttbar_PP8_FS", "ttbar_muR", "ttbar_scales");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "muR20muF10", "muR05muF10", "", "nominal", "ttbar_PP8_FS", "ttbar_muF", "ttbar_scales");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_FS", "ttbar_Var3c", "ttbar_scales");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_FS", "ttbar_FSR", "ttbar_scales");
    
    /// Top mass
    syst_manager.AddOneSidedSyst("ttbar_PP8_mass_173_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_FS", "ttbar_mass", "ttbar_mass");

    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_s_chan_AFII","nominal","SingleTop_PP8_s_chan_AFII", "nominal", "SingleTop_PP8_s_chan_FS", "SingleTop_PS_s", "SingleTop_PS");
    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_t_chan_AFII","nominal","SingleTop_PP8_t_chan_AFII", "nominal", "SingleTop_PP8_t_chan_FS", "SingleTop_PS_t", "SingleTop_PS");
    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_tW_chan_AFII","nominal","SingleTop_PP8_tW_chan_AFII", "nominal", "SingleTop_PP8_tW_chan_FS", "SingleTop_PS_tW", "SingleTop_PS");
    syst_manager.AddOneSidedSyst("SingleTop_DS_PP8_tW_chan_FS","nominal","SingleTop_PP8_tW_chan_FS", "nominal", "SingleTop_PP8_tW_chan_FS", "SingleTop_DS_DR", "SingleTop_DS_DR");
    
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "SingleTop_PP8_tW_chan_FS", "SingleTop_muF_tW", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "SingleTop_PP8_tW_chan_FS", "SingleTop_muR_tW", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "SingleTop_PP8_tW_chan_FS", "SingleTop_Var3c_tW", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "SingleTop_PP8_tW_chan_FS", "SingleTop_FSR_tW", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "SingleTop_PP8_s_chan_FS", "SingleTop_muF_s", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "SingleTop_PP8_s_chan_FS", "SingleTop_muR_s", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "SingleTop_PP8_s_chan_FS", "SingleTop_Var3c_s", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "SingleTop_PP8_s_chan_FS", "SingleTop_FSR_s", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "SingleTop_PP8_t_chan_FS", "SingleTop_muF_t", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "SingleTop_PP8_t_chan_FS", "SingleTop_muR_t", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "SingleTop_PP8_t_chan_FS", "SingleTop_Var3c_t", "SingleTop_scales");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "SingleTop_PP8_t_chan_FS", "SingleTop_FSR_t", "SingleTop_scales");
    
    /// PDF
    for (int i = 90901; i <= 90930; ++i) {
        const std::string pdfname = "PDFset"+std::to_string(i);
        syst_manager.AddOneSidedSyst("ttbar_PP8_FS", pdfname, "ttbar_PP8_FS", "PDFset90900", "ttbar_PP8_FS", pdfname, "ttbar_PDF");
    }
    syst_manager.AddNormSyst("ttbar_PP8_FS", 0.05, 0.05, "ttbar_normalisation");
    syst_manager.AddNormSyst("SingleTop_PP8_s_chan_FS", 0.05, 0.05, "SingleTop_normalisation_s");
    syst_manager.AddNormSyst("SingleTop_PP8_t_chan_FS", 0.05, 0.05, "SingleTop_normalisation_t");
    syst_manager.AddNormSyst("SingleTop_PP8_tW_chan_FS", 0.05, 0.05, "SingleTop_normalisation_tW");
    syst_manager.AddNormSyst("Wjets_Sherpa_FS", 0.5, 0.5, "Wjets_normalisation");
    syst_manager.AddNormSyst("Zjets_Sherpa_FS", 0.5, 0.5, "Zjets_normalisation");
    syst_manager.AddNormSyst("Diboson_Sherpa_FS", 0.5, 0.5, "Diboson_normalisation");
    syst_manager.AddNormSyst("ttV_aMCNLO_Pythia8_FS", 0.5, 0.5, "ttV_normalisation");
    syst_manager.AddNormSyst("ttH_PP8_FS", 0.5, 0.5, "ttH_normalisation");
    syst_manager.AddNormSyst("Multijet", 0.5, 0.5, "Multijet_normalisation");

    Extractor extractor(name);

    extractor.SetParameterS(41,0.95,1.05);
    extractor.SetParameterR(41,0.80,1.20);
    extractor.SetTtbarName("ttbar_PP8_FS");
    extractor.SetFitData(true);
    extractor.SetUseSyst(run_syst);
    extractor.SetNumberOfPE(1000);
    extractor.SetBackgroundNames(bkg_names);
    extractor.SetSpecialNames(special_names);
    extractor.SetAllSysts(syst_manager.GetAllSystematics());
    extractor.SetNeighbourIndices(1);
    extractor.SetObsName("W_m_peak_60_90");
    extractor.SetFixIndexS(-1);
    //extractor.SetFixIndexR(20);
    extractor.SetFixIndexR(-1);

    extractor.ExtractJES();

    return 0;
}
