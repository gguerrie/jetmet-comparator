#include "WmassAnalysis/Plotter.h"
#include "WmassAnalysis/SystHistoManager.h"

#include "TError.h"

#include <iostream>
#include <string>
#include <vector>

int main (int argc, char *argv[]) {
    if (argc != 2 && argc != 3) {
        std::cerr << "Expecting one or two arguments but " << argc-1 << " arguments provided" << std::endl;
        exit(EXIT_FAILURE);  
    }

    const std::string folder = argv[1];
    bool use_syst(false);
    std::string syst_param = "";

    if (argc == 3) {
        syst_param = argv[2];
        if (syst_param == "syst") {
            use_syst = true;
        } else {
            std::cerr << "Third argument can only be 'syst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    gErrorIgnoreLevel = kError;

    Plotter plotter(folder);

    static const std::vector<std::string> ttbar_names = {"ttbar_PP8_FS",
                                                         //"ttbar_aMCNLO_Herwig7_1_3_AFII",
                                                         //"ttbar_PH7_1_3_AFII",
                                                         //"ttbar_PP8_hdamp_AFII",
                                                         //"ttbar_aMCNLO_Pythia8_AFII"
                                                        };
    
    static const std::vector<std::string> bkg_names = {"SingleTop_PP8_s_chan_FS",
                                                       "SingleTop_PP8_t_chan_FS",
                                                       "SingleTop_PP8_tW_chan_FS",
                                                       "Wjets_Sherpa_FS",
                                                       "Zjets_Sherpa_FS",
                                                       "Diboson_Sherpa_FS",
                                                       "ttV_aMCNLO_Pythia8_FS",
                                                       "ttH_PP8_FS",
                                                       "Multijet"};

    static const std::vector<float> eta_bins = {0.0,1.0,2.5};
    static const std::vector<float> pt_bins = {0.0,30.0,55.0,110.0,200.0,400.0,800.0,1500.0};
    
    static const std::vector<std::string> special_names = {"ttbar_PP8_AFII",
                                                           "SingleTop_PP8_s_chan_AFII",
                                                           "SingleTop_PP8_t_chan_AFII",
                                                           "SingleTop_PP8_tW_chan_AFII",
                                                           "ttbar_PP8_hdamp_AFII",
                                                           "ttbar_PP8_shower_decor_AFII",
                                                           "SingleTop_DS_PP8_tW_chan_FS",
                                                           "SingleTop_PH7_0_4_s_chan_AFII",
                                                           "SingleTop_PH7_0_4_t_chan_AFII",
                                                           "SingleTop_PH7_0_4_tW_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_s_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_t_chan_AFII",
                                                           "SingleTop_aMCNLO_Pythia8_tW_chan_AFII"
                                                           };

    plotter.SetTTbarNames(ttbar_names);
    plotter.SetBackgroundNames(bkg_names);
    plotter.SetSpecialNames(special_names);
    plotter.SetDataName("Data");
    plotter.SetAtlasLabel("Internal");
    plotter.SetLumiLabel("139");
    plotter.SetCollection("pflow");
    plotter.SetNominalTTbarName("ttbar_PP8_FS");
    plotter.SetSystShapeOnly(false);
    plotter.SetRunSyst(use_syst);
    
    plotter.OpenRootFiles();
    plotter.SetEtaPtBins(eta_bins, pt_bins);

    /// Set all systematics
    SystHistoManager syst_manager{};
    
    std::vector<std::string> all(bkg_names);
    all.emplace_back(plotter.GetNominalTTbarName());

    /// Set tree systematics
    for (const auto& ifile : all) {
        if (ifile == "Multijet") continue;
        for (int ib = 0; ib < 9; ++ib) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_B_"+std::to_string(ib)+"_up", "BTAG_B_"+std::to_string(ib)+"_down", "", "nominal", ifile, "", "");
        }
        for (int ic = 0; ic < 5; ++ic) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_C_"+std::to_string(ic)+"_up", "BTAG_C_"+std::to_string(ic)+"_down", "", "nominal", ifile, "", "");
        }
        for (int il = 0; il < 5; ++il) {
            syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_Light_"+std::to_string(il)+"_up", "BTAG_Light_"+std::to_string(il)+"_down", "", "nominal", ifile, "", "");
        }
        syst_manager.AddTwoSidedSyst(ifile, "", "el_trigger_up", "el_trigger_down", "", "nominal", ifile, "","");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_reco_up", "el_reco_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_id_up", "el_id_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "el_isol_up", "el_isol_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_stat_up", "mu_trigger_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_trigger_syst_up", "mu_trigger_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_up", "mu_id_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_up", "mu_id_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_stat_lowpt_up", "mu_id_stat_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_id_syst_lowpt_up", "mu_id_syst_lowpt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_stat_up", "mu_isol_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_isol_syst_up", "mu_isol_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_stat_up", "mu_ttva_stat_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "mu_ttva_syst_up", "mu_ttva_syst_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "jvt_up", "jvt_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_from_charm_up", "BTAG_extrapolation_from_charm_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "BTAG_extrapolation_up", "BTAG_extrapolation_down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "pileup_up", "pileup_down", "", "nominal", ifile, "", "");

        syst_manager.AddTwoSidedSyst(ifile, "", "EG_RESOLUTION_ALL__1up", "EG_RESOLUTION_ALL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "EG_SCALE_ALL__1up", "EG_SCALE_ALL__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MET_SoftTrk_Scale__1up", "MET_SoftTrk_Scale__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_CB__1up", "MUON_CB__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_DATASTAT__1up", "MUON_SAGITTA_DATASTAT__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SAGITTA_RESBIAS__1up", "MUON_SAGITTA_RESBIAS__1down", "", "nominal", ifile, "", "");
        syst_manager.AddTwoSidedSyst(ifile, "", "MUON_SCALE__1up", "MUON_SCALE__1down", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPara", "", "nominal", ifile, "", "");
        syst_manager.AddOneSidedSyst(ifile, "MET_SoftTrk_ResoPerp", "", "nominal", ifile, "", "");
        
        /// Luminosity
        syst_manager.AddNormSyst(ifile, 0.017, 0.017, "");

        /// MC stat
        syst_manager.AddMCstatSyst(ifile);
    }

    /// Add individual systematics
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_aMCNLO_Herwig7_1_3_AFII", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddOneSidedSyst("ttbar_PH7_1_3_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "muR10muF20", "muR10muF05", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "muR20muF10", "muR05muF10", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddTwoSidedSyst("ttbar_PP8_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_FS", "", "");
    syst_manager.AddOneSidedSyst("ttbar_PP8_hdamp_AFII","nominal","ttbar_PP8_AFII", "nominal", "ttbar_PP8_FS", "ttbar_Hdamp", "ttbar_Hdamp");

    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_s_chan_AFII","nominal","SingleTop_PP8_s_chan_AFII", "nominal", "SingleTop_PP8_s_chan_FS", "", "");
    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_t_chan_AFII","nominal","SingleTop_PP8_t_chan_AFII", "nominal", "SingleTop_PP8_t_chan_FS", "", "");
    syst_manager.AddOneSidedSyst("SingleTop_PH7_0_4_tW_chan_AFII","nominal","SingleTop_PP8_tW_chan_AFII", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");
    syst_manager.AddOneSidedSyst("SingleTop_DS_PP8_tW_chan_FS","nominal","SingleTop_PP8_tW_chan_FS", "nominal", "SingleTop_PP8_tW_chan_FS", "", "");
    
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_tW_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_tW_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_s_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_s_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "muR100muF200", "muR100muF050", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "muR200muF100", "muR050muF100", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "Var3cUp", "Var3cDown", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    syst_manager.AddTwoSidedSyst("SingleTop_PP8_t_chan_FS", "", "isrmuRfac10_fsrmuRfac20", "isrmuRfac10_fsrmuRfac05", "", "nominal", "ttbar_PP8_t_chan_FS", "", "");
    
    /// PDF
    for (int i = 90901; i <= 90930; ++i) {
        const std::string pdfname = "PDFset"+std::to_string(i);
        syst_manager.AddOneSidedSyst("ttbar_PP8_FS", pdfname, "ttbar_PP8_FS", "PDFset90900", "ttbar_PP8_FS", "", "");
    }

    syst_manager.AddNormSyst("ttbar_PP8_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_s_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_t_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("SingleTop_PP8_tW_chan_FS", 0.05, 0.05, "");
    syst_manager.AddNormSyst("Wjets_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Zjets_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Diboson_Sherpa_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("ttV_aMCNLO_Pythia8_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("ttH_PP8_FS", 0.5, 0.5, "");
    syst_manager.AddNormSyst("Multijet", 0.5, 0.5, "");
    
    /// Pass all systematics
    plotter.SetAllSysts(syst_manager.GetAllSystematics());

    /// 1D truth plots
    //plotter.PlotTruthPlots("truth_W_pt", "particle truth W p_{T} [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_eta", "particle truth W |#eta| [-]", true);
    //plotter.PlotTruthPlots("truth_W_m", "particle truth W mass [GeV]", true);
    //plotter.PlotTruthPlots("reco_truth_ratio_W_m", "reco W mass /particle truth W mass [-]", true);
    //plotter.PlotTruthPlots("truth_W_jets_dR", "particle truth #Delta R jets from W [-]", true);
    //plotter.PlotTruthPlots("truth_W_jets_vec_sum_pt", "particle truth vector sum p_{T} jets from W [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_jets_scalar_sum_pt", "particle truth scalar sum p_{T} jets from W [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_quark_pt", "parton truth W p_{T} [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_quark_eta", "parton truth W |#eta| [-]", true);
    //plotter.PlotTruthPlots("truth_W_quark_m", "parton truth W mass [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_identified_pt", "reco matched particle truth W p_{T} [GeV]", true);
    //plotter.PlotTruthPlots("truth_W_identified_eta", "reco matched particle truth W |#eta| [-]", true);
    //plotter.PlotTruthPlots("truth_W_identified_m", "reco matched particle truth W mass [GeV]", true);
    //
    //plotter.PlotTruthPlots("jet_parton_ratio_pt", "particle truth jet p_{T}/parton p_{T} [-]", true);
    //plotter.PlotTruthPlots("jet_parton_ratio_e", "particle truth jet E/parton E [-]", true);
    //plotter.PlotTruthPlots("jet_parton_difference_m", "particle truth jet mass - parton mass [GeV]", true);
    //plotter.PlotTruthPlots("jet_parton_difference_eta", "particle truth jet #eta - parton #eta [-]", true);
    //plotter.PlotTruthPlots("jet_parton_difference_phi", "particle truth jet #phi - parton #phi [-]", true);
    //plotter.PlotTruthPlots("jet_parton_W_difference_pt", "(j1 + j2 p_{T}) - (parton1 + parton2 p_{T}) [GeV]", true);
    //plotter.PlotTruthPlots("jet_parton_W_ratio_pt_50", "(j1 + j2 p_{T}) / (parton1 + parton2 p_{T}), (parton1 + parton2 p_{T} > 50) [-]", true);
    //plotter.PlotTruthPlots("jet_parton_W_ratio_m", "(j1 + j2 mass) / (parton1 + parton2 mass), [-]", true);
    //plotter.PlotTruthPlots("truth_parton_lepton_W_m", "parton leptonic W mass [GeV]", true);
    //plotter.PlotTruthPlots("truth_parton_lepton_W_pt", "parton leptonic W p_{T}  [GeV]", true);
    //plotter.PlotTruthPlots("truth_parton_lepton_W_eta", "parton leptonic W |#eta| [-]", true);
    
    /// 1D Data/MC plots
    plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("jet1_pt","Leading jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("jet1_eta","Leading jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet1_phi","Leading jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet1_jvt","Leading jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet2_pt","Second jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("jet2_pt","Second jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("jet2_eta","Second jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet2_phi","Second jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet2_jvt","Second jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet3_pt","Third jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("jet3_pt","Third jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("jet3_eta","Third jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet3_phi","Third jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet3_jvt","Third jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet4_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("jet4_pt","Fourth jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("jet4_eta","Fourth jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet4_phi","Fourth jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet4_jvt","Fourth jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet5_pt","Fifth jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("jet5_pt","Fifth jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("jet5_eta","Fifth jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet5_phi","Fifth jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet5_jvt","Fifth jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet6_pt","Sixth jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("jet6_pt","Sixth jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("jet6_eta","Sixth jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet6_phi","Sixth jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet6_jvt","Sixth jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("bjet1_pt","Leading b-jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("bjet1_pt","Leading b-jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("bjet1_eta","Leading b-jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("bjet1_phi","Leading b-jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("bjet2_pt","Second b-jet p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("bjet2_pt","Second b-jet p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("bjet2_eta","Second b-jet |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("bjet2_phi","Second b-jet #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet_pt","All jet p_{T} [GeV]", "Jets", "GeV", false);
    plotter.PlotDataMCPlots("jet_pt","All jet p_{T} [GeV]", "Jets", "GeV", true);
    plotter.PlotDataMCPlots("jet_eta","All jet |#eta| [-]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_phi","All jet #phi [-]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_pt_20_50","Jet p_{T}, p_{T} < 50 GeV [GeV]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_eta_20_50","Jet |#eta|, p_{T} < 50 GeV [-]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_pt_20_30","Jet p_{T}, p_{T} < 30 GeV [GeV]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_eta_20_30","Jet |#eta|, p_{T} < 30 GeV [-]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_pt_50","Jet p_{T}, p_{T} > 50 GeV [GeV]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_pt_50","Jet p_{T}, p_{T} > 50 GeV [GeV]", "Jets", "", true);
    plotter.PlotDataMCPlots("jet_eta_50","Jet |#eta|, p_{T} > 50 GeV [-]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_pt_crack","Crack jet p_{T} [GeV]", "Jets", "GeV", false);
    plotter.PlotDataMCPlots("jet_pt_crack","Crack jet p_{T} [GeV]", "Jets", "GeV", true);
    plotter.PlotDataMCPlots("jet_eta_crack","Crack jet |#eta| [-]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_phi_crack","Crack jet #phi [-]", "Jets", "", false);
    plotter.PlotDataMCPlots("jet_jvt_crack","Crack jet JVT [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet_jvt_crack","Crack jet JVT [-]", "Events", "", true);
    plotter.PlotDataMCPlots("HT","H_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("HT","H_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("jet_n","Jet multiplicity [-]", "Events", "", false);
    plotter.PlotDataMCPlots("bjet_n","b-jet multiplicity [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet_el_dR","#Delta R (jet, el) [-]", "Events", "", false);
    plotter.PlotDataMCPlots("jet_mu_dR","#Delta R (jet, mu) [-]", "Events", "", false);
    
    plotter.PlotDataMCPlots("el_pt","Electron p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("el_pt","Electron p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("el_eta","Electron |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("el_phi","Electron #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("mu_pt","Muon p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("mu_pt","Muon p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("mu_eta","Muon |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("mu_phi","Muon #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("met","E_{T}^{miss} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("met_phi","E_{T}^{miss} #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("average_mu","average #mu [-]", "Events", "", false);
    plotter.PlotDataMCPlots("actual_mu","actual #mu [-]", "Events", "", false);
    plotter.PlotDataMCPlots("chi2","Best #chi^{2} [-]", "Events", "", false);
    plotter.PlotDataMCPlots("chi2","Best #chi^{2} [-]", "Events", "", true);
    
    plotter.PlotDataMCPlots("Wjet1_pt","Leading jet from W p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("Wjet1_pt","Leading jet from W p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("Wjet1_eta","Leading jet from W |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("Wjet1_phi","Leading jet from W #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("Wjet2_pt","Second jet from W p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("Wjet2_pt","Second jet from W p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("Wjet2_eta","Second jet from W |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("Wjet2_phi","Second jet from W #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("W_pt","W p_{T} [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("W_pt","W p_{T} [GeV]", "Events", "GeV", true);
    plotter.PlotDataMCPlots("W_eta","W |#eta| [-]", "Events", "", false);
    plotter.PlotDataMCPlots("W_phi","W #phi [-]", "Events", "", false);
    plotter.PlotDataMCPlots("W_m","Reconstructed W mass [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("W_m_peak","Reconstructed W mass [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("W_m_peak_shifted","Reconstructed W mass [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlots("W_m_peak_60_90","Reconstructed W mass [GeV]", "Events", "GeV", false);

    /// 1D Data/MC plots with the split
    plotter.PlotDataMCPlotsPossibleSplit("Wjet1_pt","Leading jet p_{T} from W [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlotsPossibleSplit("Wjet2_pt","Second jet p_{T} from W [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlotsPossibleSplit("W_m","Reconstructed W mass [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlotsPossibleSplit("W_m_peak","Reconstructed W mass [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlotsPossibleSplit("W_m_peak_shifted","Reconstructed W mass [GeV]", "Events", "GeV", false);
    plotter.PlotDataMCPlotsPossibleSplit("W_m_peak_60_90","Reconstructed W mass [GeV]", "Events", "GeV", false);

    plotter.PlotImpossible("W_m", "Reconstructed W mass [GeV]", "GeV");
    plotter.PlotImpossible("W_m_peak", "Reconstructed W mass [GeV]", "GeV");
    plotter.PlotImpossible("W_m_peak_shifted", "Reconstructed W mass [GeV]", "GeV");
    plotter.PlotImpossible("W_m_peak_60_90", "Reconstructed W mass [GeV]", "GeV");

    //plotter.PlotFFvariations("W_m", "Reconstructed W mass [GeV]", "Events", "GeV", true);
    //plotter.PlotFFvariations("W_m_peak", "Reconstructed W mass [GeV]", "Events", "GeV", true);
    //plotter.PlotFFvariations("W_m_peak_shifted", "Reconstructed W mass [GeV]", "Events", "GeV", true);
    //plotter.PlotFFvariations("W_m_peak_60_90", "Reconstructed W mass [GeV]", "Events", "GeV", true);
    //plotter.PlotFFvariations("Wjet1_pt", "Leading jet p_{T} from W [GeV]", "Events", "GeV", true);
    //plotter.PlotFFvariations("Wjet2_pt", "Second jet p_{T} from W [GeV]", "Events", "GeV", true);
    //plotter.PlotFFvariations("W_m", "Reconstructed W mass [GeV]", "Events", "GeV", false);
    //plotter.PlotFFvariations("W_m_peak", "Reconstructed W mass [GeV]", "Events", "GeV", false);
    //plotter.PlotFFvariations("W_m_peak_shifted", "Reconstructed W mass [GeV]", "Events", "GeV", false);
    //plotter.PlotFFvariations("W_m_peak_60_90", "Reconstructed W mass [GeV]", "Events", "GeV", false);
    //plotter.PlotFFvariations("Wjet1_pt", "Leading jet p_{T} from W [GeV]", "Events", "GeV", false);
    //plotter.PlotFFvariations("Wjet2_pt", "Second jet p_{T} from W [GeV]", "Events", "GeV", false);

    /// 1D response plots (binned in eta and pt)
    plotter.PlotResponsePlots("jet_response", "reco all jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_fifth_jet", "reco fifth jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_gluon", "reco gluon jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_leading_gluon", "leading reco gluon jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_non_leading_gluon", "non-leading reco gluon jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_light", "reco u/d-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_s", "reco s-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_c", "reco c-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_b", "reco b-quark jet p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W", "reco all jet from W p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W_light", "reco u/d-quark jet from W p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W_s", "reco s-quark jet from W p_{T}/truth jet p_{T} [-]", true);
    plotter.PlotResponsePlots("jet_response_from_W_c", "reco c-quark jet from W p_{T}/truth jet p_{T} [-]", true);

    /// Make html tables with the reco efficiencies
    plotter.PlotRecoEfficiencies();

    /// Make html table with yields
    plotter.CalculateYields();

    plotter.CloseRootFiles();

    /// Successful run
    return 0;
}
