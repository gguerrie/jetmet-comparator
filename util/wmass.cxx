#include "WmassAnalysis/FileProcessor.h"
#include "WmassAnalysis/Matcher.h"

#include "TError.h"
#include "TH1.h"

#include <iostream>

int main(int argc, char *argv[]) {

    TH1::SetDefaultSumw2();
    TH1::AddDirectory(kFALSE);

    if (argc == 1 || argc > 4) {
        std::cerr << "1, 2 or 3 parameters allowed, but " << argc-1 << " parameters provided \n";
        exit(EXIT_FAILURE);
    }

    gErrorIgnoreLevel = kError;

    std::string dataset_type("full");
    std::string systematics("nominal");

    std::string name =  argv[1];
    if (argc > 2) dataset_type = argv[2];
    if (argc > 3) {
        systematics = argv[3];
        if (systematics != "nominal" && systematics != "syst" && systematics != "sfsyst") {
            std::cerr << "Last parameter has to be 'nominal', 'syst' or 'sfsyst'" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    Chi2Params par;
    par.mass_w = 80.38;
    par.mass_t = 172.5;
    par.width_w = 12;
    par.width_t = 15;

    static const double chi2_cut = 1000000;

    FileProcessor processor(name, dataset_type, systematics);

    processor.SetCriticalRecoDR(0.5);
    processor.SetCriticalTruthDR(0.5);
    processor.SetLeadingWjetCuts(0, 50000e3);
    processor.SetSubLeadingWjetCuts(0, 5000e3);
    processor.SetLeadingWjetEtaCuts(-1, 9999);
    processor.SetSubLeadingWjetEtaCuts(-1, 9999);
    processor.SetAverageWjetCuts(-1, 50000e3);
    processor.SetNpvCuts(-1, 1000);
    processor.SetMuCuts(-1, 9999);
    processor.SetRunNumberCuts(0, 999999);
    processor.SetCreateHerwigCorrection(true);
    processor.ReadConfig();
    
    processor.ProcessAllFiles(par, chi2_cut);

    std::cout << "\nmain: Everything went fine\n";

    return 0;
}
